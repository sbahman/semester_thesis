function params = importParamsTable(filename, startRow, endRow)
%IMPORTFILE Import numeric data from a text file as a matrix.
%   BASE64CANDPARAMS = IMPORTFILE(FILENAME) Reads data from text file
%   FILENAME for the default selection.
%
%   BASE64CANDPARAMS = IMPORTFILE(FILENAME, STARTROW, ENDROW) Reads data
%   from rows STARTROW through ENDROW of text file FILENAME.
%
% Example:
%   base64candparams = importfile('base64cand_params.csv', 2, 2);
%
%    See also TEXTSCAN.

% Auto-generated by MATLAB on 2016/07/29 12:07:22

%% Initialize variables.
delimiter = ',';
if nargin<=2
    startRow = 2;
    endRow = inf;
end

%% Format string for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column3: double (%f)
%	column4: double (%f)
%   column5: double (%f)
%	column6: double (%f)
%   column7: double (%f)
%	column8: text (%s)
%   column9: text (%s)
%	column10: double (%f)
%   column11: double (%f)
%	column12: double (%f)
%   column13: double (%f)
%	column14: double (%f)
%   column15: text (%s)
%	column16: double (%f)
%   column17: text (%s)
%	column18: double (%f)
%   column19: text (%s)
%	column20: double (%f)
%   column21: double (%f)
%	column22: double (%f)
%   column23: double (%f)
%	column24: text (%s)
%   column25: text (%s)
%	column26: text (%s)
%   column27: double (%f)
%	column28: double (%f)
%   column29: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%f%f%f%f%f%s%s%f%f%f%f%f%s%f%s%f%s%f%f%f%f%s%s%s%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Create output variable
params = table(dataArray{1:end-1}, 'VariableNames', {'edim','entdim','cidim','lindim','memsize','nhop','ncand','distance','time','init_std','init_hid','sdt','anneal','annealep','annealbest','maxgradnorm','critavg','batchsize','optim','noise','unkmen','logIncrement','epochs','initrand','sgdSeq','dict','dt','stateEpoch','stateChunk'});

