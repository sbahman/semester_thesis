function compareStats( mdl, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addRequired(p, 'mdl');
addRequired(p, 'dataset', @ischar);
addRequired(p, 'measure', @ischar);
addRequired(p, 'figureID', @isnumeric);
addParameter(p, 'max', false)
addParameter(p, 'smooth', 5)
addParameter(p, 'name', false, @ischar)
addParameter(p, 'yLimits', [0.6 0.9])
parse(p, mdl, varargin{:});

persistent cfcolor;
if isempty(cfcolor)
    cfcolor = containers.Map('KeyType', 'uint32', 'ValueType', 'double');
end

mdl = p.Results.mdl;
dataset = p.Results.dataset;
measure = p.Results.measure;
id = p.Results.figureID;
maxbool = p.Results.max;
smspan = p.Results.smooth;
plotname = p.Results.name;
yLimits = p.Results.yLimits;

if ~plotname
    plotname = mdl.name;
end

Y = mdl.logs.(dataset).(measure);
if smspan ~= 0
    Y = smooth(Y,smspan);
end

X = (1:length(Y))./mdl.epochL;
%l = line([epoch epoch], [0 1], 'Color', [0 0 0], 'LineStyle', '-.');
%an = ;
%l.Annotation.LegendInformation.IconDisplayStyle = 'off';



figure(id)
hold on
colorOrder = get(gca, 'ColorOrder');
colorID = 0;
if isKey(cfcolor, id)
    colorID = cfcolor(id)+1;
    cfcolor(id) = colorID;
else
    colorID = 1;
    cfcolor(id) = colorID;
end
color = colorOrder(colorID,:);


plot(X,Y, 'DisplayName', plotname, 'Color', color)
if maxbool
    maxname = ['max(', plotname, ') = ', num2str(max(Y),3)];
    line(get(gca,'Xlim'), [max(Y) max(Y)], 'DisplayName', maxname, 'LineStyle', '--', 'Color', color)
end
%an;

if strcmp(measure, 'f1')
    ylabel(['f1 score on ' dataset])
elseif strcmp(measure, 'p')
    ylabel(['precision on ' dataset])
elseif strcmp(measure, 'r')
    ylabel(['recall on ' dataset])
end
xlabel('#Epochs')
ylim(yLimits)
    
hold off

legend('Location', 'southeast');
%li = 

end

