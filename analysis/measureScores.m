function [ t ] = measureScores( mdl, dataset, measure )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

t = table;
t.([dataset '_' measure '_max'])   = max(mdl.logs.(dataset).(measure));
t.([dataset '_' measure])   = mdl.logs.(dataset).(measure)(end);

end

