function [ t ] = getTableEntry( mdl, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
p = inputParser;
addRequired(p, 'mdl');
addParameter(p, 'epoch', 657, @isnumeric)
parse(p, mdl, varargin{:});

mdl = p.Results.mdl;
epoch = p.Results.epoch;

nepoch = length(mdl.logs.aidaB.f1)/epoch;

t = struct2table(mdl.params);
t.name = mdl.name;
t.epoch = nepoch;
t.id = mdl.index;

finames = fieldnames(mdl.logs);

for f = 1:length(finames)
    t = [t,scores(mdl, finames{f})];
end

end

