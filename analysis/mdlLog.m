classdef mdlLog
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        logs = struct([])
        params = struct([])
        index = 0
        name
        epochL = 657
    end
    
    properties (Access=private)
        fname
    end
    
    properties (Constant, Access=private)
        idxc = indexcounter();
        logextension = '_log.csv'
        paramextension = '_params.csv'
        logfolder = 'logs/'
    end
    
    
    methods (Access=public)
        function obj = mdlLog(fname, varargin)
            p = inputParser;
            addRequired(p, 'fname', @ischar);
            addOptional(p, 'index', false);
            addParameter(p, 'name', false);
            parse(p, fname, varargin{:});
            
            obj.fname = p.Results.fname;
            if ~p.Results.index
                obj.index = obj.idxc.getNext();
            else
                obj.index = obj.idxc.addIndex(p.Results.index);
            end
            
            if ~p.Results.name
                obj.name = p.Results.fname;
            else
                obj.name = p.Results.name;
            end
            obj.logs = loadSTL(strcat(obj.logfolder, obj.fname, obj.logextension));
            obj.params = loadSTP(strcat(obj.logfolder, obj.fname, obj.paramextension));
            obj.params.length = length(obj.logs.aidaB.f1);
        end
    end
end

