clear all
close all
clc

%% define save locations and other vars

lat = true;        

latexfpath = '../doc/thesis/figures/';
latexfformat = '-depsc';

pwpointpath = '../doc/presentation/figures/';
pwpfformat = '-dpng';

if lat
    figpath = latexfpath;
    figformat = latexfformat;
else
    figpath = pwpointpath;
    figformat = pwpfformat;
end


%% import all the data
gloveNoNoise =  mdlLog('dot2glove', 'name', 'gloveNoNoise');
gloveNoNoiseA =  mdlLog('dot2gloveNoNoiseA', 'name', 'gloveNoNoiseA');
gloveBase = mdlLog('noise', 'name', 'gloveBase');
gloveBaseA = mdlLog('noiseAnneal', 'name', 'gloveBaseA');
%gloveBaseABatch = mdlLog('noiseAnnealBatchsize', 'name', 'gloveBaseABatch');
w2vbase = mdlLog('dot2w2v', 'name', 'w2vbase');
w2vbaseA = mdlLog('dot2w2vA', 'name', 'w2vbaseA');
gloD80 = mdlLog('glove80', 'name', 'gloD80');
gloD80A = mdlLog('glove80Anneal', 'name', 'gloD80A');
gloD80M150 = mdlLog('glodim80mem150', 'name', 'gloD80M150');
gloD80M150A = mdlLog('glodim80mem150Anneal', 'name', 'gloD80M150A');
gloD80M150A99 = mdlLog('glodim80mem150Anneal099', 'name', 'gloD80M150A99');
initrand = mdlLog('initrand');
initrandA = mdlLog('initrandA');
mem150 = mdlLog('memsize150', 'name', 'mem150');
mem150A = mdlLog('memsize150Anneal', 'name', 'mem150A');
mem150ABest = mdlLog('memsize150AnnealBest', 'name', 'mem150ABest');
nhop1 = mdlLog('nhop1');
nhop1A = mdlLog('nhop1A');
nhop2 = mdlLog('nhop2');
nhop2A = mdlLog('nhop2A');
nhop6 = mdlLog('nhop6');
nhop6A = mdlLog('nhop6A');
base64cand = mdlLog('base64cand', 'name', 'cand64');
base64candA = mdlLog('base64A', 'name', 'cand64A');

adagrad01 = mdlLog('adagrad01');
adagrad001 = mdlLog('adagrad001');
critavg = mdlLog('critavg');
critavgA = mdlLog('critavgA');
seq = mdlLog('seq');
seqA = mdlLog('seqA');

timesep = mdlLog('timesep');
timesepA = mdlLog('timesepA');
unkmen = mdlLog('unkmen');
unkmenA = mdlLog('unkmenA');
unkmen01 = mdlLog('unkmen01');
unkmen01A = mdlLog('unkmen01A');

w2vfulltmp = mdlLog('w2vfullTMP2', 'name' , 'w2vfulltmp');


% %% fix wonkey epochs
% le1028 = 657;
% le1024 = 770;
% 
% 
% %len1028 = length(gloveNoNoise.logs.aidaB.f1);
% gloD80.epoch = (1:le1028)./le1028;
% gloD80.params.batchsize = 1028;
% 
% critavg.epoch = (1:le1028)./le1028;
% critavg.params.batchsize = 1028;
% 
% seq.epoch = (1:le1028)./le1028;
% seq.params.batchsize = 1028;
% 
% gloveBaseA.epoch = fixepoch(gloveBaseA);
% gloD80A.epoch = fixepoch(gloD80A);
% gloD80M150A.epoch = fixepoch(gloD80M150A);
% gloD80M150A99.epoch = fixepoch(gloD80M150A99);
% mem150A.epoch = fixepoch(mem150A);
% mem150ABest.epoch = fixepoch(mem150ABest);


%% add to array for operations on all mdls

mdlarr = {gloveNoNoise, gloveNoNoiseA, gloveBase, gloveBaseA, w2vbase, ...
    w2vbaseA, gloD80, gloD80A, gloD80M150, gloD80M150A, gloD80M150A99, ...
    initrand, initrandA, mem150, mem150A, mem150ABest, nhop1, nhop1A, nhop2, nhop2A, ...
    nhop6, nhop6A, base64cand, base64candA, adagrad01, adagrad001, ...
    critavg, critavgA, seq, seqA, timesep, timesepA, unkmen, unkmenA, ...
    unkmen01, unkmen01A, w2vfulltmp};


% glove20size = size(gloveBase.logs.aidaA.f1,1);
% glove64size = size(base64cand.logs.aidaA.f1,1);
% w2v20size = size(w2vbase.logs.aidaA.f1,1);


%% plot stuff

name = 'Noise';
figureID = 1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(gloveNoNoiseA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Memory Size';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(mem150A, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Initialization Type w2v';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(w2vbaseA, 'aidaB', 'f1', figureID)
compareStats(initrandA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);

name = 'Initialization Type';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(initrandA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Embedding dimensions';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(gloD80A, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Memory Size vs Embedding dimension';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(mem150A, 'aidaB', 'f1', figureID)
compareStats(gloD80A, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Number of hops';
figureID = figureID +1;
smoothwindow = 9;
compareStats(nhop1A, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
compareStats(nhop2A, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
compareStats(gloveBaseA, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
compareStats(nhop6A, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);



name = 'Candidates';
figureID = figureID +1;
smoothwindow = 5;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
compareStats(base64candA, 'aidaB', 'f1', figureID, 'smooth', smoothwindow)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);



name = 'Annealing rate';
figureID = figureID +1;
compareStats(gloD80M150A, 'aidaB', 'f1', figureID)
compareStats(gloD80M150A99, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Annealing Best';
figureID = figureID +1;
compareStats(mem150A, 'aidaB', 'f1', figureID)
compareStats(mem150ABest, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'Optimization';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(adagrad001, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);

name = 'Optimization01';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(adagrad01, 'aidaB', 'f1', figureID)
compareStats(adagrad001, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'criterion averaging';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(critavgA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'sequential passing of data';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(seqA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


name = 'separate time embeddings';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(timesepA, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);

name = 'Unknown mention token';
figureID = figureID +1;
compareStats(gloveBaseA, 'aidaB', 'f1', figureID)
compareStats(unkmenA, 'aidaB', 'f1', figureID)
compareStats(unkmen01A, 'aidaB', 'f1', figureID)
title(name)
print(strrep([figpath name], ' ', '_'), figformat);


% figureID = figureID +1;
% compareStats(gloveBaseA, 'aidaB', 'f1', figureID, 'yLimits', 'auto')
% compareStats(w2vbase, 'aidaB', 'f1', figureID, 'yLimits', 'auto')
% compareStats(w2vfulltmp, 'aidaB', 'f1', figureID, 'yLimits', 'auto')
% title('W2vFull')

%% make parameter table

varnames = {};
tc = {};
for i = 1:length(mdlarr)
    t_tmp = getTableEntry(mdlarr{i});
    varnames = t_tmp.Properties.VariableNames;
    tc = [tc; table2cell(t_tmp)];
end

params = cell2table(tc, 'VariableNames', varnames);
params.Properties.RowNames = params.name;
params.name = [];

params = moveToFront(params, 'id');

%% print params

pout = params;

mdlout = {'gloveNoNoiseA', 'gloveBaseA', 'w2vbaseA', 'gloD80A', ...
    'initrandA', 'mem150A', 'nhop1A', 'nhop2A', 'nhop6A', 'cand64A', ...
    'adagrad01', 'adagrad001', 'critavgA', 'seqA', 'timesepA', 'unkmenA', 'unkmen01A'};

%pcols = {'id', 'edim', 'lindim', 'memsize', 'nhop', 'ncand', 'time', 'sdt', 


pout = pout(mdlout,:);

delcols = {'entdim', 'cidim', 'memsize', 'ncand', 'distance', 'init_std', ...
    'init_hid', 'sdt', 'anneal', 'annealep', 'annealbest', 'maxgradnorm', ...
    'critavg', 'batchsize', 'optim', 'logIncrement', 'epochs', 'initrand', ...
    'dict', 'dt', 'stateEpoch', 'stateChunk', 'length'};

pout(:, delcols) = [];


writetable(pout, 'models.csv', 'WriteRowNames', true, 'Delimiter', 'semi', 'QuoteStrings', true);


