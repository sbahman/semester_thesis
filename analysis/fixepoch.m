function [ epoch ] = fixepoch( mdl, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addRequired(p, 'mdl');
addOptional(p, 'lenep1', 657, @isnum);
addOptional(p, 'lenep2', 770, @isnum);

parse(p, mdl, varargin{:});

mdl = p.Results.mdl;
lenep1 = p.Results.lenep1;
lenep2 = p.Results.lenep2;

off = 1 - lenep1/lenep2;

epoch = [(1:lenep1)./lenep1, ((lenep1+1 : length(mdl.logs.aidaB.f1))./lenep2)+off];

end

