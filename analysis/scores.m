function [ t ] = scores( mdl, dataset )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


t1 = measureScores(mdl, dataset, 'f1');
t2 = measureScores(mdl, dataset, 'p');
t3 = measureScores(mdl, dataset, 'r');
t = [t1,t2,t3];

end

