classdef indexcounter < handle
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    properties(Access=private)
        inds = []
    end
    
    methods (Access=private)
        function append(obj, idx)
            obj.inds(end+1) = idx;
            obj.inds = sort(obj.inds);
        end
    end
    methods (Access=public)
        function valid = validateIndex(obj, idx)
            valid = ~any(abs(obj.inds - idx)==0) && idx > 0;
        end
        function idx = addIndex(obj, idx)
            assert(obj.validateIndex(idx), 'invalid index')
            obj.append(idx);
        end
        function next = getNext(obj)
            next = 0;
            for i = 1:numel(obj.inds)
                if ~any(abs(obj.inds - i)==0)
                    next = i;
                    obj.append(i);
                    break;
                end
            end
            if next == 0
                next = numel(obj.inds) + 1;
                obj.append(next);
            end
        end  
    end
    
end

