function [ logst ] = loadSTL( filename )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

logst = ([]);

M = importAsMatrix(filename);

fieldnames = {'wiki'; 'aidaA'; 'aidaB'; 'msnbc'; 'ace'; 'aquaint'};

for i = 1:numel(fieldnames)
    stIndex = 1 + (i-1)*3;
    enIndex = i*3;
    name = char(fieldnames(i));
    logst.(name) = populateDataset(M(:,stIndex:enIndex));
end

end


function [scores] = populateDataset(M)
% M is a matrix cointaining f1, p and r for the dataset in that collumn
% order
    scores = ([]);
    scores.f1   = M(:,1);
    scores.p    = M(:,2);
    scores.r    = M(:,3);
end

    
