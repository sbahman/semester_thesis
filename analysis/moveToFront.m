function [ t ] = moveToFront( tab, variableName )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

varnames = tab.Properties.VariableNames;
others = ~strcmp(variableName,varnames);
varnames = [variableName varnames(others)];
t = tab(:,varnames);


end

