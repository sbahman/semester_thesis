clear all
close all
clc

%% define save locations and other vars
printFig = false;
lat = true;        

latexfpath = '../doc/thesis/figures/';
latexfformat = '-depsc';

pwpointpath = '../doc/presentation/figures/';
pwpfformat = '-dpng';

if lat
    figpath = latexfpath;
    figformat = latexfformat;
else
    figpath = pwpointpath;
    figformat = pwpfformat;
end

appendname = '';
%% get data

P = importP('soft/nhop1.csv');
names = importNames('soft/tissuenames.csv');
tmp = load('stopwords.mat');
stp = tmp.stopwords;

nhops = size(P,1);

%thr = [0.3 0.3 0.3];
%qthr = [0.5 0.5 0.5];
%ethr = [0.006 0.006 0.006];
ethr = 0.01 * [1 1 1];

qu = zeros(size(P));
idx = {};
for i = 1:nhops
    %idx(i) = {find(P(i,:) > thr(i)*max(P(i,:)))};
    %idx(i) = {find(P(i,:) > quantile(P(i,:),qthr(i)))};
    idx(i) = {find(P(i,:) > ethr(i))};
    qu(i,idx{i}) = 1;
end

for i = 1:nhops
    disp('-------------')
    disp(i)
    disp('Important')
    disp('mean')
    disp(mean(P(i,idx{i})))
    disp('variance')
    disp(var(P(i,idx{i})))
    Pn = P(i,:);
    Pn(idx{i}) = [];
    disp('unimportant')
    disp('mean')
    disp(mean(Pn))
    disp('variance')
    disp(var(Pn))
end    

C = bi2de(qu');



%% plots

%stop words

%stopwords
ctmp = C;
ctmp(stp) = [];

name = 'stop_words';
figure(1)
subplot(2,1,1)
histogram(C(stp));
xlim([-0.5 7.5])
title('Stop word weighting')

subplot(2,1,2)
histogram(ctmp);
xlim([-0.5 7.5])
title('Non stop word weighting')
if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end

%important word
num = {'first', 'second', 'third'};

name = 'word_importance';
figure('units','normalized','position',[.1 .1 .6 .7])
for i = 1:nhops
    subplot(2*nhops,2,(2*i-1))
    histogram(C(idx{i}));
    xlim([-0.5 7.5])
    title(['Important words ' num{i} ' hop'])
    
    ctmp = C;
    ctmp(idx{i}) = [];
    subplot(2*nhops,2,2*i)
    histogram(ctmp);
    xlim([-0.5 7.5])
    title(['Unimportant words ' num{i} ' hop'])
end
if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end

name = 'word_importance_hist';
figure('units','normalized','position',[.1 .1 .22 .11])
histogram(C);
xlim([-0.5 7.5])
title('Word importance histogram')
if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end



name = 'word_weights';
figure('units','normalized')%,'position',[.1 .1 .3 .1])
for i = 1:nhops
    subplot(nhops,1,i)
    bar(P(i,:))
    xlim([-inf inf])
    title(['Importance weighting ' num{i} ' hop'])
end
if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end

colors = [0 0 0.8;
          0.8 0 0;
          0 0.8 0];

name = 'word_weights_group';
figure(5)
b = bar(P', 0.9);
xlim([-inf inf])
title(['Importance weighting'])
for i = 1:nhops
    b(i).FaceColor = colors(i, :);
end

if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end


name = 'word_weights_group_lim';
figure(6)
b = bar(P', 0.9);
xlim([-inf inf])
ylim([0 0.05])
title(['Importance weighting ylim'])

for i = 1:nhops
    b(i).FaceColor = colors(i, :);
end
if printFig
    print([strrep([figpath name], ' ', '_') appendname], figformat);
end
