
After introducing our model and showing how to train it in the previous
section, we now explain the inference process used for prediction.

% !TEX root = paper.tex

\subsection{Candidate Selection}  \label{ssec:candidates}

At test time, for each mention to be disambiguated, we first select a set of potential candidates by considering the top $R$ ranked entities based on the local mention-entity probability dictionary $\hat{p}(e|m)$. We found $R=64$ to be a good compromise between efficiency and accuracy loss. Second, we want to keep the average number of candidates per mention as small as possible in order to reduce the running time which is quadratic in this number (see the next section for details). Consequently, we further limit the number of candidates per mention by keeping only the top 10 entity candidates re-ranked by the local mention-context-entity compatibility defined as
\begin{align}
\log p(e_i|m_i, c_i) = \log p(e_i | m_i) + \zeta \sum_{w_j \in c_i} \log p(w_j | e_i) + \!\text{const} \,.
\label{eq:local-all} 
\end{align}
These pruning heuristics result in a significantly improved running time at an insignificant accuracy loss.

If the given mention is not found in our map $\hat{p}(e|m)$, we try to replace it by the closest name in this dictionary. Such a name is picked only if the Jaccard distance between the set of letter trigrams of these two strings is smaller than a threshold that we empirically picked as 0.5. Otherwise, the mention is not linked at all.








\subsection{Belief Propagation}

Collectively disambiguating all mentions in a text involves iterating through an exponential number of possible entity resolutions. Exact inference in general graphical models is NP-hard, therefore approximations are employed. We propose solving the inference problem through the \textit{loopy belief propagation} (LBP)~\cite{loopy} technique, using the max-product algorithm that approximates the MAP solution in a run-time polynomial in $n$, the number of input mentions. For the sake of brevity, we only present the algorithm for the maximum entropy model described by Eq.~\eqref{eq:cond-exp-family}; A similar approach was used for the enhanced PBoH model given by Eq.~\eqref{eq:c2}.

Our proposed graphical model is a fully connected graph where each node corresponds to an entity random variable. Unary potentials $\exp(\rho_{m,e})$ model the entity-mention compatibility, while pairwise potentials $\exp(\lambda_{\{e,e'\}})$ express enti\-ty-entity correlations. For the posterior in Eq.~\eqref{eq:cond-exp-family}, one can derive the update equation of the logarithmic message that is sent in round $t + 1$ from entity random variable $E_i$ to the outcome $e_j$ of the entity random variable $E_j$ :
\begin{align}
m_{E_i \rightarrow E_j}^{t+1}&(e_j) =  \\
& \max_{e_i} \left( \rho_{e_i, m_i} \!+\! \lambda_{\{e_i, e_j\}} \!+ \! \sum \limits_{1 \leq k \leq n; k \neq j} m_{E_k \rightarrow E_i}^t(e_i) \right) \nonumber
\end{align}
Note that, for simplicity, we skip the factor graph framework and send messages directly between each pair of entity variables. This is equivalent to the original BP framework.

We chose to update messages synchronously: in each round $t$, each two entity nodes $E_i$ and $E_j$ exchange messages. This is done until convergence or until an allowed maximum number of iterations (15 in our experiments) is reached. The convergence criterion is:
\begin{align}
\max_{1 \leq i, j \leq n; e_j \in \mathcal{E}} \vert m_{E_i \rightarrow E_j}^{t+1}(e_j) - m_{E_i \rightarrow E_j}^t(e_j) \vert \le \epsilon
\end{align}
where $\epsilon = 10^{-5}$. This setting was sufficient in most of the cases to reach convergence. 

In the end, the final entity assignment is determined by:
\begin{align}
e_i^* = \arg\max_{e_i} \left( \rho_{e_i, m_i} + \sum \limits_{1 \leq k \leq n} m_{E_k \rightarrow E_i}^t(e_i) \right)
\end{align}

The complexity of the belief propagation algorithm is, in our case, $O(n^2 \cdot r^2)$, with $n$ being the number of mentions in a document and $r$ being the average number of candidate entities per mention (10 in our case). More details regarding the run-time and convergence of the loopy BP algorithm can be found in Section \ref{sec:experiments}.





