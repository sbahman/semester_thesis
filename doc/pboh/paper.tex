\documentclass{sig-alternate-2013}


\pdfpagewidth 8.5in
\pdfpageheight 11.0in
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{changepage}
\usepackage{float}
\usepackage{hyperref}
\usepackage{breakurl}
\usepackage{todonotes}
\usepackage{fancyvrb}
\usepackage{dsfont}
\usepackage{amssymb}
\usepackage{verbatim}
\usepackage{tikz,pgfplots}
\usepackage{indentfirst}
\usepackage{breqn}
\usepackage{pbox}
\usepackage{soul}
\usepackage{rotating}
\usepackage{slashbox}
\usepackage{footmisc}

\usetikzlibrary{bayesnet}

\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}


\renewcommand{\Re}{{\mathbb R}}
\newcommand{\x}{{\mathbf x}}
\newcommand{\y}{{\mathbf y}}
\newcommand{\m}{{\mathbf m}}
\newcommand{\cc}{{\mathbf c}}
\newcommand{\E}{{\mathds E}}
\newcommand{\e}{{\textbf{e}}}
\renewcommand{\L}{{\mathcal{L}}}
\newcommand*{\backin}{\rotatebox[origin=c]{-180}{$\in$}}%

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\newcommand\ci{\perp\!\!\!\perp}


%\permission{Copyright is held by the International World Wide Web Conference Committee (IW3C2). IW3C2 reserves the right to provide a hyperlink to the author's site if the Material is used in electronic media.}
%\conferenceinfo{WWW 2016,}{April 11--15, 2016, Montr\'eal, Qu\'ebec, Canada.} 
%\copyrightetc{ACM \the\acmcopyr}
%\crdata{978-1-4503-4143-1/16/04. \\
%\url{http://dx.doi.org/10.1145/2872427.2882988}}

%\clubpenalty=10000 
%\widowpenalty = 10000


\begin{document}
\title{Probabilistic Bag-Of-Hyperlinks Model for Entity  Linking }

\numberofauthors{5}
\author{
\alignauthor
Octavian-Eugen Ganea\\
       \affaddr{Dept. of Computer Science}\\
       \affaddr{ETH Zurich, Switzerland}\\
       \email{ganeao@inf.ethz.ch}
       \alignauthor
Marina Ganea\titlenote{Currently at Google Inc.}\\
       \affaddr{Dept. of Computer Science}\\
       \affaddr{ETH Zurich, Switzerland}\\
       \email{marinah@google.com}
       \alignauthor
Aurelien Lucchi\\
       \affaddr{Dept. of Computer Science}\\
       \affaddr{ETH Zurich, Switzerland}\\
       \email{alucchi@inf.ethz.ch}
\and 
Carsten Eickhoff\\
       \affaddr{Dept. of Computer Science}\\
       \affaddr{ETH Zurich, Switzerland}\\
       \email{ecarsten@inf.ethz.ch}         
       \alignauthor
Thomas Hofmann\\
       \affaddr{Dept. of Computer Science}\\
       \affaddr{ETH Zurich, Switzerland}\\
       \email{thomaho@inf.ethz.ch}
}


\maketitle
\begin{abstract}
Many fundamental problems in natural language processing rely on determining what entities appear in a given text. Commonly referenced as \textit{entity linking}, this step is a fundamental component of many NLP tasks such as text understanding, automatic summarization, semantic search or machine translation. Name ambiguity, word polysemy, context dependencies and a heavy-tailed distribution of entities contribute to the complexity of this problem.

We here propose a probabilistic approach that makes use of an effective graphical model to perform collective \textit{entity disambiguation}. Input mentions (i.e.,~linkable token spans) are disambiguated jointly across an entire document by combining a document-level prior of entity co-occurrences with local information captured from mentions and their surrounding context. The model is based on simple sufficient statistics extracted from data, thus relying on few parameters to be learned.

Our method does not require extensive feature engineering, nor an expensive training procedure. We use loopy belief propagation to perform approximate inference. The low complexity of our model makes this step sufficiently fast for real-time usage. We demonstrate the accuracy of our approach on a wide range of benchmark datasets, showing that it matches, and in many cases outperforms, existing state-of-the-art methods.


\end{abstract}

%\begin{CCSXML}
%<ccs2012>
%<concept>
%<concept_id>10010147.10010178.10010179.10003352</concept_id>
%<concept_desc>Computing methodologies~Information extraction</concept_desc>
%<concept_significance>500</concept_significance>
%</concept>
%<concept>
%<concept_id>10010147.10010178.10010179.10010186</concept_id>
%<concept_desc>Computing methodologies~Language resources</concept_desc>
%<concept_significance>500</concept_significance>
%</concept>
%</ccs2012>
%\end{CCSXML}

%\ccsdesc[500]{Computing methodologies~Information extraction}
%\ccsdesc[500]{Computing methodologies~Language resources}
%\printccsdesc
\keywords{Entity linking; Entity disambiguation; Wikification; Probabilistic graphical models; Approximate inference; Loopy belief propagation}

\section{Introduction}\label{sec:intro}
\input{introduction}

\section{Related Work}\label{sec:related}
\input{relatedWork}

\section{Probabilistic Model}\label{sec:model}
\input{model}

\section{Inference}\label{sec:inference}
\input{inference}

\section{Experiments }\label{sec:experiments}
\input{experiments}

\section{Conclusion}\label{sec:conclusion}
\input{conclusion}


\small
\bibliographystyle{abbrv}
\bibliography{references}
\normalsize

\end{document}
