% !TEX root = paper.tex

In this section, we formally define the \textit{entity linking} task that we address in this work and describe our modeling approach in detail. 

\subsection{Problem Definition and Formulation}\label{ssec:probl-def}

Let $\mathcal{E}$ be a knowledge base (KB) of entities, $\mathcal{V}$ a finite dictionary of phrases or names and $\mathcal{C}$ a context representation. Formally, we seek a mapping $F: ( {\mathcal V}, {\mathcal C})^n \to \mathcal{E}^n$, that takes as input a sequence of linkable \textit{mentions} $\m=(m_1,\dots,m_n)$ along with their contexts $\cc=(c_1,\dots,c_n)$ and produces a joint entity assignment $\e=(e_1,\dots,e_n)$. Here $n$ refers to the number of linkable spans in a document. Our problem is also known as \textit{entity disambiguation} or \textit{link generation} in the literature. \footnote{Note that we do not address the issues of \textit{mention detection} or \textit{nil identification} in this work. Rather, our input is a document along with a fixed set of linkable mentions corresponding to existing KB entities.}

We can construct  such a mapping $F$ in a probabilistic approach, by learning a conditional probability model $p(\e | \m, \cc)$ from data and then employing (approximate) probabilistic inference in order to find the maximum a posteriori (MAP) assignment, hence: 
\begin{align}
F(\m,\cc)  := \argmax_{\e \in \mathcal{E}^n} p(\e | \m, \cc) \, .
\label{eq:posterior}
\end{align}
%
In the sequel, we describe how to estimate such a model from a corpus of entity-linked documents. Finally, we show in Section \ref{sec:inference} how to apply belief propagation (max-product) for approximate inference in this model.

\subsection{Maximum Entropy Models}

Assume a corpus of entity-linked documents is available. Specifically, we used the set of Wikipedia pages together with their respective Wiki hyperlinks. These hyperlinks are considered ground truth annotations, the mention being the linked span of text and the truth entity being the Wikipedia page it refers to. One can extract two kinds of basic statistics from such a corpus: First, counts of how often each entity was referred to by a specific name. Second, pairwise co-occurrence counts for entities in documents. Our fundamental conjecture is that most of the relevant information needed for entity disambiguation is contained in these counts, that they are \textit{sufficient statistics}. We thus request that our probability model reproduces these counts in expectation. As this alone typically yields an ill-defined problem, we follow the \textit{maximum entropy principle} of Jaynes~\cite{jaynes1982rationale}: Among the feasible set of distributions we favor the one with maximal entropy. 

Formally, let ${\mathcal D}$ be an entity-linked document collection. Ignoring mention contexts for now, we extract for each document $d \in {\mathcal D}$ a sequence of mentions $\m^{(d)}$ and their corresponding target entities $\e^{(d)}$, both of length $n^{(d)}$. Assuming exchangeability of random variables within these sequences, we reduce each $(\e,\m)$ to statistics (or \textit{features}) about mention-entity and entity-entity co-occurrence as follows:
\begin{align}
\phi_{e, m}(\e,\m) & := \sum_{i=1}^n \mathds{1}[e_i = e] \! \cdot \! \mathds{1}[m_i = m], \; \forall (e,m) \! \in \! \mathcal{E} \! \times \!\mathcal{V} 
\\
\psi_{\{e,e'\}}(\e) & := \sum_{i < j} \mathds{1}[\{ e_i, e_j\} =  \{e,e'\}], \;  \forall e, e' \in \mathcal{E}\,,
\end{align}
where $\mathds{1}[\cdot]$ is the indicator function. Note that we use the subscript notation $\{e,e'\}$ for $\psi$ to take into account the symmetry in $e, e'$ as well the fact that one may have $e=e'$. 

The document collection provides us with empirical estimates for the expectation of these statistics under an i.i.d.~sampling model for documents, namely the averages
\begin{align}
\phi_{e,m}({\cal D}) & := \frac 1{| \cal D|} \sum_{d \in {\cal D}} \phi_{e,m}(\e^{(d)},\m^{(d)})\,,\\
\psi_{\{e,e'\}}({\cal D}) & := \frac 1{| \cal D|} \sum_{d \in {\cal D}} \psi_{\{e,e'\}}(\e^{(d)}) \,.
\end{align}  

Note that in entity disambiguation, the mention sequence $\m$ is always considered given, while we seek to predict the corresponding entity sequence $\e$. It is thus not necessary to try to model the joint distribution $p(\e,\m)$, but sufficient to construct a conditional model $p(\e|\m)$. Following Berger et al.~\cite{berger1996maximum} this can be accomplished by taking the empirical distribution $p(\m|{\cal D})$ of mention sequences and combining it with a conditional model via $p(\e,\m) = p(\e|\m) \cdot p(\m |{ \cal D})$. We then require that:
\begin{align}
\label{eq:constraints}
\E_p[ \phi_{e,m} ] = \phi_{e,m}({\cal D}) \quad \text{and} \quad
\E_p[ \psi_{\{e,e'\}} ] = \psi_{\{e,e'\}}({\cal D}),
\end{align}
which yields $|\mathcal{E}| \cdot | \mathcal{V}| + \binom{| \mathcal{E} |}{2} + |\mathcal{E}|$ moment constraints on $p(\e|\m)$.
%Note that here
%\begin{align}
%\E_p[X] = \sum_{\m} p(\m|{\cal D}) \sum_{\e} p(\e|\m) X(\e,\m) \,. 
%\end{align}

The maximum entropy distributions, fulfilling constraints as stated in Eq.~\eqref{eq:constraints} form a conditional exponential family for which $\phi(\cdot,\m)$ and $\psi(\cdot,\cdot)$ are sufficient statistics. We thus know that there are canonical parameters $\rho_{e,m}$ and $\lambda_{\{e,e'\}}$ (formally corresponding to Lagrange multipliers) such that the maximum entropy distribution can be written as 
\begin{align}
\label{eq:cond-exp-family}
p(\e|\m; \rho, \lambda)= \frac 1{Z(\m)} \exp \left[ \langle \rho, \phi(\e,\m) \rangle + \langle \lambda, \psi(\e) \rangle \right]
\end{align}
where $Z(\m)$ is the partition function 
\begin{align}
Z(\m) := \sum_{\e  \in \mathcal{E}^n} \exp \left[ \langle \rho, \phi(\e,\m) \rangle + \langle \lambda, \psi(\e) \rangle \right]\,.
\end{align}
Here we interpret $(e,m)$ and $\{e,e'\}$ as multi-indices and suggestively define the shorthands
\begin{align}
\langle \rho, \phi \rangle := \sum_{e,m} \rho_{e,m} \phi_{e,m}, 
\;\; \langle \lambda, \psi \rangle := \sum_{\{ e,e'\}} \lambda_{\{e,e'\}} \psi_{\{e,e'\}} \,.
\end{align}
Note that we can switch between the statistics view and the raw data view by observing that 
\begin{align}
\langle \rho, \phi(\e,\m) \rangle = \sum_{i=1}^n \rho_{e_i,m_i}, \quad 
\langle \lambda, \psi(\e) \rangle = \sum_{i<j} \lambda_{\{e_i,e_j\}} \,.
\label{eq:unrolled}
\end{align}
While the maximum entropy principle applied to our fundamental conjecture restricts the form of our model to a finite-dimensional exponential family, we need to investigate ways of finding the optimal or -- as we will see -- an approximately optimal distribution in this family. To that extent, we first re-interpret the obtained model as a factor graph model. 

\subsection{Markov Network and Factor Graph} 

Complementary to the maximum entropy estimation perspective, we want to present a view on our model in terms of probabilistic graphical models and factor graphs. Inspecting Eq.~\eqref{eq:cond-exp-family} and interpreting $\phi$ and $\psi$ as potential functions, we can recover a Markov network that makes conditional independence assumptions of the following type: an entity link $e_i$ and a mention $m_j$ with $i \neq j$ are independent, given $m_i$ and $\e_{-i}$, where $\e_{-i}$ denotes the set of entity variables in the document excluding $e_i$. This means that a mention $m_j$ only influences a variable $e_i$ through the intermediate variable $e_j$. However, the functional form in Eq.~\eqref{eq:cond-exp-family} goes beyond these conditional independences in that it limits the order of interaction among the variables. A variable $e_i$ interacts with neighbors in its Markov blanket through pairwise potentials. In terms of a \textit{factor graph} decomposition, $p(\e|\m)$ decomposes into functions of two arguments only, modeling pairwise interactions between entities on one hand, and between entities and their corresponding mentions on the other hand.

%
\begin{figure}
\centering{}
\begin{tikzpicture}
  % Define nodes
  \node[latent] (y1) {$E_1$};
  \node[latent, right=1.5cm of y1] (y2) {$E_2$};
  \node[latent, above=1.5cm of y2] (y3) {$E_3$};
  \node[latent, left=1.5cm of y3] (y4) {$E_4$};
  
  \node[obs, left =1 of y1] (x1) {$m_1$};
  \node[obs, right=1 of y2] (x2) {$m_2$};
  \node[obs, right=1 of y3] (x3) {$m_3$};
  \node[obs, left=1 of y4] (x4) {$m_4$};

%  \factor[right=0.4 of x1] {x1-y1} {above:$\rho_{e_1,m_1}$} {} {} ;
  \factor[right=0.4 of x1] {x1-y1} {} {} {} ;
  \factor[left=0.4 of x2] {x2-y2} {} {} {} ;
  \factor[left=0.4 of x3] {x3-y3} {} {} {} ;
%  \factor[right=0.4 of x4] {x4-y4} {below:$\rho_{e_4,m_4}$} {} {} ;
  \factor[right=0.4 of x4] {x4-y4} {} {} {} ;

  \edge[-] {x1}  {y1};
  \edge[-] {x2}  {y2};
  \edge[-] {x3}  {y3};
  \edge[-] {x4}  {y4};

% \factor[right=0.7 of y1] {y1-y2} {below:$\lambda_{\{e_1,e_2\}}$} {}{};
\factor[right=0.7 of y1] {y1-y2} {} {}{};
\factor[right=0.7 of y4] {y3-y4} {} {}{};
%   \factor[below=0.3 of y4, xshift=0.75cm] {y4-y2} {above, xshift=0.25cm:$\lambda_{\{e_2,e_4\}}$} {} {} ;
 \factor[below=0.3 of y4, xshift=0.75cm] {y4-y2} {} {} {} ;
 \factor[above=0.3 of y1, xshift=0.75cm] {y1-y3} {} {} {} ;
%  \factor[below=0.6 of y3] {y2-y3} {right:$\lambda_{\{e_2,e_3\}}$} {}{};
\factor[below=0.6 of y3] {y2-y3} {} {}{};
\factor[above=0.6 of y1] {y2-y3} {} {}{};
 
  \edge[-] {y1}  {y2};
  \edge[-] {y1}  {y3};
  \edge[-] {y1}  {y4};
  \edge[-] {y2}  {y3};
  \edge[-] {y2}  {y4};
  \edge[-] {y3}  {y4};
\end{tikzpicture}
\caption{Proposed factor graph for a document with four mentions. Each mention node $m_i$ is paired with its corresponding entity node $E_i$, while all entity nodes are connected through entity-entity pair factors.}
\label{example-crf-el}
\end{figure}
%
We  emphasize the factor model view by rewriting \eqref{eq:cond-exp-family} as
\begin{align}
p(\e | \m; \rho, \lambda) \propto \prod_{i} \exp\left[ \rho_{e_i,m_i} \right] \cdot \prod_{i<j} \exp \left[ \lambda_{\{e_i,e_j\}} \right]
\label{eq:mrf}
\end{align}
where we think of $\rho$ and $\lambda$ as functions 
\begin{align}
& \rho: \mathcal{E} \times \mathcal{V} \to \Re, \quad (e,m) \mapsto \rho_{e,m} \nonumber \\
& \lambda: \mathcal{E} \cup \mathcal{E}^2 \to \Re, \quad \{e,e'\} \mapsto \lambda_{\{e,e'\}} \nonumber 
\end{align}
An example of a factor graph ($n=4$) is shown in Figure \ref{example-crf-el}. We will investigate in the sequel how the factor graph structure can be further exploited. 


\subsection{(Pseudo--)Likelihood Maximization}\label{ssec:pseudolikelihood}

While the maximum entropy approach directly motivates the exponential form of Eq.~\eqref{eq:cond-exp-family} and is amenable to a plausible factor graph interpretation, it does not by itself suggest an efficient parameter fitting algorithm. As is known by convex duality, the optimal parameters can be obtained by maximizing the conditional likelihood of the model under the data,
\begin{align}
\L(\rho,\lambda; \mathcal{D}) = \sum_{d} \log\ p(\e^{(d)} | \m^{(d)}; \rho, \lambda)
\label{eq:likelihood}
\end{align}
%
However, specialized algorithms for maximum entropy estimation such as generalized iterative scaling \cite{darroch1972generalized} are known to be slow, whereas gradient-based methods require the computation of gradients of $\L$, which involves evaluating expectations with regard to the model, since     
\begin{align}
\nabla_\rho \log Z(\m) = \E_p \phi(\e,\m), \;\;\nabla_\lambda \log Z(\m) = \E_p \psi(\e) \,.
\end{align} 
The exact inference problem of computing these model expectations, however, is not generally tractable due to the pairwise couplings through the $\psi$-statistics.\\

As an alternative to maximizing the  likelihood in Eq.~\eqref{eq:likelihood}, we have investigated an approximation known as, pseudo-likelihood maximization \cite{mccallum2009,murphy2006}. Its main benefits are low computational complexity, simplicity and practical success. Switching to the Markov network view, the pseudo-likelihood estimator predicts each variable conditioned on the value of all variables in its Markov blanket. The latter consists of the  minimal set of variables that renders a variable conditionally independent of everything else. In our case the Markov blanket consists of all variables that share a factor with a given variable. Consequently, the Markov blanket of $e_i$ is ${\cal N}(e_i) := (m_i,\e_{-i})$. The posterior is then approximated in the pseudo-likelihood approach as:
\begin{align}
\tilde p(\e | \m; \rho, \lambda) := \prod_{i=1}^n p(e_i | {\cal N}(e_i); \rho, \lambda )\,,
\end{align}
which results in the tractable log-likelihood function
\begin{align} 
\label{eq:pseudo}
\tilde \L(\rho,\lambda; \mathcal{D}) :=
\sum_{d \in \mathcal{D}} \sum_{i=1}^{n^{(d)}} \log p(e^{(d)}_i | {\cal N}(e^{(d)}_i); \rho, \lambda )\,.
\end{align} 

Introducing additional $L_2$-norm penalties $\gamma (\| \lambda\|^2_2 + \| \rho\|_2^2)$ to further regularize $\tilde \L$, we have utilized parallel stochastic gradient descent (SGD)~\cite{hogwild} with sparse updates to learn parameters $\rho, \lambda$. From a practical perspective, we only keep for each token span $m$ parameters $\rho_{e,m}$ for the most frequently observed entities $e$. Moreover, we only use  $\lambda_{\{e,e'\}}$ for entity pairs $(e,e')$ that co-occurred together a sufficient number of times in the collection $\mathcal{D}$.\footnote{For the Wikipedia collection, even after these pruning steps, we ended up with more than 50 million parameters in total.} As we will discuss in more detail in Section \ref{sec:experiments}, our experimental findings suggest this brute-force learning approach to be somewhat ineffective, which has motivated us to develop simpler, yet more effective plug-in estimators as described below. 

\subsection{Bethe Approximation} \label{ssec:unnormalized}

The major computational difficulty with our model lies in the pairwise couplings between entities and the fact that these couplings are dense: The Markov dependency graph between different entity links in a document is always a complete graph. Let us consider what would happen, if the dependency structure were loop-free, i.e.,~it would form a tree. Then we could rewrite the prior probability in terms of marginal distributions in the so-called \textit{Bethe form}. Encoding the tree structure in a symmetric relation ${\cal T}$, we would get  
\begin{align}
p(\e) = \frac{ \prod_{ \{i,j\} \in {\cal T} }  p(e_i,e_j) }{
   \prod_{i = 1}^n p(e_i)^{d_i - 1} } , \quad d_i := |\{ j: \{i,j\} \in {\cal T}\}|
   \,.
\label{eq:bethe-exact}
\end{align}
The Bethe approximation \cite{bethe1} pursues the idea of using the above representation as an unnormalized approximation for $p(\e)$, even when the Markov network has cycles. How does this relate to the exponential form in Eq.~\eqref{eq:cond-exp-family}? By simple pattern matching, we see that if we choose 
\begin{align}
\lambda_{\{e,e'\}} &= \log\left(\frac{p(e,e')}{p(e) \,p(e')}\right),\quad  \forall e, e' \in \mathcal{E}
\label{eq:lambda-bethe}
\end{align}
we can apply Eq.~\eqref{eq:bethe-exact} to get an approximate distribution
\begin{align}
\label{eqn:lambda-matching}
\begin{split}
\bar p(\e) & \propto \frac{ \prod_{i<j}  p(e_i,e_j) }{\prod_{i=1}^n p(e_i)^{n-2} } =  \prod_{i=1}^n p(e_i) \prod_{i<j} \frac{p(e_i,e_j)}{p(e_i)\, p(e_j)} \\
&=  \exp\left[ \sum_{i} \log p(e_i) + \sum_{i<j}  \lambda_{\{e_i,e_j\}} \right] \,,
% \exp \left[ \langle \lambda, \psi(\e) \rangle \right]
\end{split}
\end{align}
where we see the same exponential form in $\lambda$ appearing as in Eq.~\eqref{eq:unrolled}. We complete this argument by observing that with 
\begin{align}
\rho_{e,m} = \log p(e) + \log p(m|e) 
\label{eq:rho}
\end{align}
we obtain a representation of a joint distribution that exactly matches the form in Eq.~\eqref{eq:cond-exp-family}.\\

What have we gained so far? We started from the desire of constructing a model that would agree with the observed data on the co-occurrence probabilities of token spans and their linked entities as well as on the co-link probability of entity pairs within a document. This has led to the conditional exponential family in Eq.~\eqref{eq:cond-exp-family}. We have then proposed pseudo-likelihood maximization as a way to arrive at a tractable learning algorithm to try to fit the massive amount of parameters $\rho$ and $\lambda$. Alternatively, we have now seen that a Bethe approximation of the joint prior $p(\e)$ yields a conditional distribution $p(\e|\m)$ that (i) is a member of the same exponential family, (ii) has explicit formulas for how to choose the parameters from pairwise marginals, and (iii) would be exact in the case of a dependency tree. We claim that the benefits of computational simplicity together with the correctness guarantee for non-dense dependency networks outweighs the approximation loss, relative to the model with the best generalization performance within the conditional exponential family. In order to close the suboptimality gap further, we suggest some important refinements below. 


\subsection{Parameter Calibration} \label{ssec:scaling-len} 


With the previous suggestion, one issue comes into play: The total contribution coming from the pairwise interactions between entities will scale with $\binom n 2$, while the entity--mention compatibility contributions will scale with $n$, the total number of mentions. This is a direct observation of the number of terms contributing to the sums in \eqref{eq:unrolled}. However, for practical reasons, it is somewhat implausible that, as $n$ grows, the prior $p(\e)$ should dominate and the contribution of the likelihood term should vanish. The model is not well-calibrated with regard to  $n$. 

We propose to correct for this effect by adding a normalization factor to the $\lambda$-parameters by replacing \eqref{eq:lambda-bethe} with:
\begin{align}
\lambda^n_{e,e'} = \frac{2}{n-1} \log\left(\frac{p(e,e')}{p(e)\cdot p(e')}\right), \quad  \forall e,e' \in \mathcal{E}
\label{eq:bethe-normalized-params}
\end{align}
where now these parameters scale inversely with $n$, the number of entity links in a document, making the corresponding sum in Eq.~\eqref{eq:cond-exp-family} scale with $n$. With this simple change, a substantial accuracy improvement was observed empirically, the details of which are reported in our experiments.\\

The re-calibration in Eq.~\eqref{eq:bethe-normalized-params} can also be justified by the following combinatorial argument: 
%
For a given set ${\mathbf Y}$ of random variables, define an ${\mathbf Y}$-cycle as a graph containing as nodes all variables in ${\mathbf Y}$, each with degree exactly 2, connected in a single cycle. Let $\Xi$ be the set enumerating all possible ${\mathbf Y}$-cycles. Then, $|\Xi| = (n-1)!$, where $n$ is the size of ${\mathbf Y}$.

In our case, if the entity variables $\e$ per document would have formed a cycle of length $n$ instead of a complete subgraph, the Bethe approximation would have been written as:
\begin{align}
\bar p_\pi(\e) \propto \frac{\prod_{(i,j) \in E(\pi)} p(e_i,e_j)}{\prod_i p(e_i) }, \quad \forall \pi \in \Xi
\end{align}
where $E(\pi)$ is the set of edges of the $\e$-cycle $\pi$. However, as we do not desire to further constrain our graph with additional independence assumptions, we propose to approximate the joint prior $p(\e)$ by the average of the Bethe approximation of all possible $\pi$, that is 
\begin{align}
\log \bar p(\e) \approx \frac{1}{|\Xi|} \sum_{\pi \in \Xi} \log \bar p_\pi(\e) \,.
\end{align}
%
Since each pair $(e_i,e_j)$ would appear in exactly $2(n-2)!$ $\e$-cycles, one can derive the final approximation:
\begin{align}
\bar p(\e) \approx \frac{\prod_{i < j} p(e_i,e_j)^{\frac{2}{n-1}}}{\prod_i p(e_i) }\,.
\label{eq:average-bethe}
\end{align}
Distributing marginal probabilities over the parameters starting from Eq.~\eqref{eq:average-bethe} and applying a similar argument as in Eq.~\eqref{eqn:lambda-matching} results in the assignment given by Eq.~\eqref{eq:bethe-normalized-params}. While the above line of argument is not a strict mathematical derivation, we believe this to shed further light on the empirically observed effectiveness of the parameter re-scaling. 

\subsection{Integrating Context}  \label{ssec:smoothed}

The model that we have discussed so far does not consider the local context of a mention. This is a powerful source of information that a competitive entity linking system should utilize. For example, words like ``computer'', ``company'' or ``device''  are more likely to appear near references of the entity \texttt{Apple\_Inc.} than of the entity \texttt{Apple\_fruit}.  We demonstrate in this section how this integration can be easily done in a principled way on top of the current probabilistic model. This showcases the extensibility of our approach. Enhancing our model with additional knowledge such as entity categories or word co-reference can also be done in a rigorous way, so we hope that this provides a template for future extensions. 

As stated in Section~\ref{ssec:probl-def}, for each mention $m_i$ in a document, we maintain a context representation $c_i$ consisting of the bag of words surrounding the mention within a window of length $K$\footnote{Throughout our experiments, we used a context window of size $K = 100$, intuitively chosen and without extensive validation.}. Hence, $c_i$ can be viewed as an additional random variable with an observed outcome.  At this stage, we make additional reasonable independence assumptions that increase  tractability of our model. First, we assume that,  knowing the identity of the linked entity $e_i$, the mention token span $m_i$ is just the surface form of the entity, so it brings no additional information for the generative process describing the surrounding context $c_i$. Formally, this means that $m_i$ and $c_i$ are conditionally independent given $e_i$. Consequently, we obtain a factorial expression for the joint model
\begin{align}
p(\e, \m, \cc) = p(\e) p(\m, \cc| \e) = p(\e) \prod_{i=1}^n p(m_i|e_i) p(c_i|e_i)
\label{eq:context-mention}
\end{align}
This is a simple extension of the previous factor graph that includes context variables. Second, we assume conditional independence of the words in $c_i$ given an entity $e_i$ which let us factorize the context probabilities as 
\begin{align}
p(c_i | e_i) = \prod_{w_j \in c_i} p(w_j | e_i)\,.
\label{eq:context-factor}
\end{align}
Note that this assumption is commonly made in models using bag-of-word representations or na\"{\i}ve Bayes classifiers.
%

While this completes the argument from a joint model point of view, we need to consider one more aspect for the conditional distribution $p(\e|\m,\cc)$ that we are interested in. If we cannot afford (computationally as well as with regard to training data size) a full-blown discriminative learning approach, then how do we balance the relative influence of the context $c_i$ and the mention token span $m_i$ on $e_i$? For instance, the effect of $c_i$ will depend on the chosen window size $K$, which is not realistic.  

To address this issue, we resort to a hybrid approach, where, in the spirit of the Bethe approximation, we continue to express our model in terms of simple marginal distributions that can be easily estimated independently from data, yet that allow for a small number of parameters (in our case ``small'' equals $2$) to be chosen to optimize the conditional log-likelihood $p(\e|\m,\cc)$. We thus introduce weights $\zeta$ and $\tau$ that control the importance of the context factors and, respectively, of the entity-entity interaction factors. Putting equations~\eqref{eq:rho},~\eqref{eq:bethe-normalized-params},~\eqref{eq:context-mention} and~\eqref{eq:context-factor} together, we arrive at the final model that will be subsequently referred to as the \textit{PBoH} model (\textbf{P}robabilistic \textbf{B}ag \textbf{o}f \textbf{H}yperlinks):

\begin{align}
\log p(\e|\m, \cc) = & \sum_{i=1}^n \left( \log p(e_i|m_i )+ \zeta  \sum_{w_j \in c_i} \log p(w_j | e_i) \right) \nonumber \\
&  + \frac{2 \tau}{n-1}  \sum_{i < j} \log\left(\frac{p(e_i,e_j)}{p(e_i) \, p(e_j)}\right) \!+ \!\text{const} \,.
\label{eq:c2}
\end{align}
Here we used the identity $p(m|e) p(e) = p(e|m) p(m)$ and absorbed all $\log p(m)$ terms in the constant.  We use grid-search on a validation set for the remaining problem of optimizing over the parameters $\zeta,\tau$. Details are provided in section \ref{sec:experiments}. 


\subsection{Smoothing Empirical Probabilities}

In order to estimate the probabilities involved in Eq.~\eqref{eq:c2}, we rely on an entity annotated corpus of text documents, e.g.,~Wikipedia Web pages together with their hyperlinks which we view as ground truth annotations. From this corpus, we derive empirical probabilities for a name-to-entity dictionary $\hat p(m|e)$ based on counting how many times an entity appeared referenced by a given name\footnote{In our implementation we summed the mention-entity counts from Wikipedia hyperlinks with the Crosswikis counts~\cite{spitkovsky2012lrec}}. We also compute the pairwise probabilities $\hat{p}(e,e')$ obtained by counting the pairwise co-occurrence of entities $e$ and $e'$ within the same document. Similarly, we obtained empirical values for the marginals $\hat{p}(e) = \sum_{e'} \hat{p}(e,e')$ and for the context word-entity statistics $\hat{p}(w|e)$.

In the absence of huge amounts of data, estimating such probabilities from counts is subject to sparsity. For instance, in our statistics, there are 8 times more distinct pairs of entities that co-occur in at most 3 Wikipedia documents compared to the total number of distinct pairs of entities that appear together in at least 4 documents. Thus, it is expected that the heavy tail of infrequent pairs of entities will have a strong impact on the accuracy of our system. 

Traditionally, various smoothing techniques are employed to address sparsity issues arising commonly in areas such as natural language processing. Out of the wealth of methods, we decided to use the absolute discounting smoothing technique~\cite{zhai2004study} that involves interpolation of higher and lower order (backoff) models. In our case, whenever insufficient data is available for a pair of entities $(e,e')$, we assume the two entities are drawn from independent distributions. Thus, if we denote by $N(e,e')$ the total number of corpus documents that link both $e$ and $e'$, and by $N_{ep}$ the total number of pairs of entities referenced in each document, then the final formula for the smoothed entity pairwise probabilities is:
\begin{align}
\widetilde{p}(e,e') = \frac{\max(N(e,e') - \delta, 0)}{N_{ep}} + (1 - \mu_e) \hat{p}(e) \hat{p}(e')
\end{align}
where $\delta \in [0,1]$ is a fixed discount and $\mu_e$ is a constant that assures that $\sum_{e} \sum_{e'} \widetilde{p}(e,e') = 1$. $\delta$ was set by performing a coarse grid search on a validation set. The best $\delta$ value was found to be 0.5.

The word-entity empirical probabilities $\hat{p}(w|e)$ were computed based on the Wikipedia corpus by counting the frequency with which word $w$ appears in the context windows of size K around the hyperlinks pointing to $e$. In order to avoid memory explosion, we only considered the entity-words pairs for which these counts are at least 3. 
%
These empirical estimates are also sparse, so we used absolute discounting smoothing for their correction by backing off to the unbiased estimates $\hat{p}(w)$. The latter can be much more accurately estimated from any text corpus. Finally, we obtain:
\begin{align}
\widetilde{p}(w | e) = \frac{\max(N(w,e) - \xi, 0)}{N_{wp}} + (1 - \mu_w) \hat{p}(w)\,.
\end{align}
Again $\xi \in [0,1]$ was optimized by grid search to be 0.5.