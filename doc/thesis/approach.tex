\section{Approach}
Our model is an extension of the End-to-End Memory Network architecture introduced in \cite{Sukhbaatar2015} by Sukhbaatar \etal. The input to the model consists of a discrete set of context words $\cont[1], .. , \cont[k]$ and a mention \men. We then retrieve the set of \ncand{} candidate entities $\ecand[1], .. ,\ecand[n]$ which were most frequently associated with the given mention in Wikipedia, as is also done in the PBoH method \cite{Ganea2015} by Ganea \etal The model then re-weights those candidates and produces the most likely one as the predicted entity \epred{}. Both the context words \cont{} and the mention \men{} are embedded using the $\qdim \times \qsize$ matrix \mcdi{}, whereas the candidate entities \ecand{} are embedded using a separate embedding $\rdim \times \rsize$ matrix \edi{}. \qsize{} denotes the size of the mention and context word dictionary, and \rsize{} the size of the entity dictionary. Mentions as well as entities may be phrases comprised of multiple words, so that the model has to learn an embedding for each distinct phrase.

\subsection{Single Layer}
We will first describe our model for the single layer case which implements a single memory hop operation. We will then expand it by stacking to give multiple hops in memory.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{El-memN2Nsingle}
	\caption{Single layer version of the model, (figure adapted from \cite{Sukhbaatar2015}).}
	\label{fig:sh}
\end{figure}

\paragraph{Input memory representation:}
	Each of the context words is represented by a learned embedding vector \cont{}. These are converted to memory vectors \mem{} by the $\idim \times \qdim$ matrix \matA{} and bias \biaA{}.
	\begin{equation}\label{eq:memvec}
		\mem = \matA \cdot \cont + \biaA
	\end{equation}
	The mention representation \men{} is taken from the same embedding dictionary as the context words; however, it is transformed with the $\idim \times \qdim$ matrix \matB{} and bias \biaB{} to obtain the internal state \state{}.
	\begin{equation}\label{eq:state}
		\state{} = \matB \cdot \men + \biaB
	\end{equation}
	Now we can compute the probabilistic importance \wght{} of each context word by computing the softmax of the scalar product of the state \state{} and the context memory vectors \mem{}.
	\begin{equation}
		\wght = Softmax(\state{}^T \cdot \mem)
	\end{equation}
	Thanks to the softmax, the function from input to output is smooth and we can easily compute gradients and backpropagate through it.

\paragraph{Output memory representation:}
	The context words \cont{} are also transformed by the $\idim \times \qdim$ matrix \matC{} and bias \biaC{} to obtain the corresponding output vectors \cnto{}. These are then combined in a weighted sum to give the response vector of the memory \resp{}.
	\begin{equation}\label{eq:outvec}
		\cnto = \matC \cdot \cont + \biaC
	\end{equation}
	\begin{equation}
		\resp{} = \sum_{i=1}^{k}\wght \cdot \cnto
	\end{equation}
	
\paragraph{Prediction}
	We transform the internal state \state{} with the $\idim \times \idim$ matrix \matD{} and add the result to the response vector \resp{}.
	\begin{equation}
		\state{2} = \resp{} + \matD \cdot \state{1}
	\end{equation}
	The result is further transformed to produce a vector in the entity space.
	\begin{equation}
		\eout = \matF \cdot \state{2} + \biaF
	\end{equation}
	Entities are embedded by a $\rdim \times \rsize$ matrix \edi{}, and the predicted vector \eout{} should ideally be as close as possible to the vector representing the ground truth entity. Therefore, the set of \ncand{} entities $\caset =  \ecand[1], .. , \ecand[\ncand]$ that were most frequently referred to by the given mention in Wikipedia are retrieved, as is also done in the PBoH method \cite{Ganea2015}. The system then selects the most likely candidate \epred{}, based on a distance operation, as the final prediction.
	\begin{equation}\label{eq:pred}
		\epred = \argmin_{\ecand \in \caset}(distance(\eout, \ecand))
	\end{equation}
	We found the dot product produced the best results when compared with euclidean and cosine distance. The probability $\hat{p}(\ecand | mention)$ is not used by this method other than to select the top $n$ candidates. \\
	
	During training, all embedding and transformation matrices \mcdi{}, \edi{}, \matA{}, \matB{}, \matC{}, \matD{} and \matF{} as well as their corresponding biases \biaA{}, \biaB{}, \biaC{} and \biaF{} are jointly learned by minimizing the cross-entropy loss between the predicted candidate \epred{} and the true candidate \etrue{}.
	\begin{equation}
		cost = -\log \bigg( \frac{exp(distance(\epred, \etrue))}{\sum_{i} exp(distance(\epred, \ecand))} \bigg)
	\end{equation}

\subsection{Multiple Layers}

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{El-memN2Nmulti}
	\caption{Multi hop version of the model, we constrain $A^{(1)} = A^{(2)} = A^{(3)}$ and $C^{(1)} = C^{(2)} = C^{(3)}$, (figure adapted from \cite{Sukhbaatar2015}).}
	\label{fig:mh}
\end{figure}

	This model can easily be expanded to multiple layers, which are commonly referred to as hops in memory. To that end, equation \ref{eq:state} is adapted such that for the $j+1$-th hop in memory, the input to the memory is the output of the memory from the previous hop:
	\begin{equation}
		\state{j + 1} = \resp{j} + \matD \cdot \state{j}
	\end{equation}
	With our implementation, each layer uses the same embedding matrices \matA{} and \matC{} to produce the memory vectors \mem{} and \cnto{}.\\
	Using multiple hops, the model can pay attention to different context words in each hop, which we show in section \ref{sec:res}.
	
\subsection{Position embedding}
	The position of a context word relative to the mention also carries significance. In general, a context word  which is far away from the mention is less likely to be helpful in disambiguating said mention than a context word that appears right next to the mention. We therefore evaluate two positional encoding schemes where equations \ref{eq:memvec} and \ref{eq:outvec} are modified.
	\paragraph{Joint positional Encoding}  A vector \timc{} specific to each position $i$ is added to the corresponding context embedding vector \cont{}. 
	\begin{equation}
		\mem = \matA \cdot (\cont + \timc) + \biaA
	\end{equation}
	\begin{equation}
	\cnto = \matC \cdot (\cont + \timc) + \biaC
	\end{equation}
	\paragraph{Separate positional Encoding} This method is similar to the first. The difference is that separate positional vectors \timc[\mem] and \timc[\cnto] are learned for \mem{} and \cnto{} respectively.
	\begin{equation}
		\mem = \timc[\mem] + \matA \cdot \cont + \biaA
	\end{equation}
	\begin{equation}
		\cnto = \timc[\cnto] + \matC \cdot \cont + \biaC
	\end{equation}
	The position vectors \timc{} or \timc[\mem] and \timc[\cnto] are jointly learned with the other parameters. In practise we have found both positional encoding schemes to perform equally well with the separate positional encoding scheme having a slight edge. An interesting option that we could not test due to time constraints would have been to concatenate position embedding vectors \timc{} with the memory vectors \mem{} to form a larger word embedding.
	
	
	