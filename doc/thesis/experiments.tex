 \section{Experiments}\label{sec:exp}
		 \paragraph{Evaluation metrics}
		 We quantify the quality of an entity-linking system by measuring common metrics such as precision, recall and F1 scores.\\
		 For entity linking, these are defined as follows:\\
		 \begin{itemize}
		 \item $TP$ An output is deemed to be a true positive if $\epred = \etrue$, i.e. the system has produced the correct output.\\
		 \item $FP$ A false positive occurs if the system outputs an incorrect entity $\epred \neq \etrue$.\\
		 \item $FN$ False negatives are deemed any examples which could not be extracted. This occurs when there are no candidate entities for the given mention.
		 \item Precision: $P = \frac{\#TP}{\#TP + \#FP}$
		 \item Recall: $R = \frac{\#TP}{\#TP + \#FN}$
		 \item F1: $F_1 = \frac{2 \cdot P \cdot R}{P + R}$
		 \end{itemize}
		 
		 \begin{wrapfigure}[16]{r}{0.5\textwidth}
		 	\vspace{-10pt}
		 	\centering
		 	\includegraphics[width=\linewidth]{Optimization}
		 	\caption{Comparison of model learned with annealing to adagrad with $\eta=0.01$ (see results shown in table \ref{tab:mdl})}
		 	\label{fig:opt}
		 \end{wrapfigure}
		 
		 \paragraph{Training}
		 To train the model, we used the Wikipedia dump from February 2014, where we consider hyperlinks as ground truth entity annotations and the corresponding anchor text as the mention. We used the parameters proposed by \cite{Sukhbaatar2015}, and validated them on our models. Our models were trained with a learning rate of $\eta = 0.01$. After the first epoch, we applied learning rate annealing, whereby the learning rate is decreased every time the performance on the validation set (AIDA\_A) did not decrease $\eta = \eta \cdot 0.9$ if $AIDA\_A.f1(k) >= AIDA\_A.f1(k-1)$. Training ceases as soon as the learning rate drops below $10e-5$. This typically leads to around 1.2 passes over the data. We have also experimented with the use of Adagrad (figure \ref{fig:opt}), which increased the performance of the model, but at the cost of increased training time. The $l_2$ norm of the gradient is constrained to be lower than 50, and all training uses a batch size of 1024. This large batch size was necessary to reduce the training time.
		 
		 A memory padding token is trained to fill the memory whenever the number of context words is smaller than the memory. The same is done with a candidate padding token when the retrieved number of candidates is smaller than the model expects.
		 
		 \input{tables/allDatasetStats}
		 
		 
		 \begin{wrapfigure}[16]{R}{0.5\textwidth}
		 	%\vspace{-20pt}
		 	\centering
		 	\includegraphics[width=\linewidth]{Initialization_Type}
		 	\caption{GloVe embedding initialization vs random initialization (see results shown in table \ref{tab:mdl})}
		 	\label{fig:init}
		 \end{wrapfigure}
		 
		 \paragraph{Initialization}
		 The internal parameters are initialized using $\mathcal{N}(0, 0.05)$. Pre-trained 50-dimensional glove6B vectors (size 300'000) are used to initialize context and mention embeddings. This initialization gives a clear performance improvement, as seen in figure \ref{fig:init}. Mention phrases are initialized by taking the average glove6B embeddings of the words in the phrase. The number of mention phrases added to the dictionary in this manner is about 5.5 Million leading to a final dictionary size (\qsize) of 6 Million. It is vital that the mention phrases that are initialized in this way are trained sufficiently. However, given that the number of examples in the training set is 70 Million (see table \ref{tab:dset}) and the fact that model is trained by only one pass over the data, it is likely that many of these phrases are encountered only once during training. The entity candidates are initialized with pre-trained GloVe entity vectors (size 3'000'000), which are specifically trained to be suitable for representing entities. For unknown entity phrase embeddings we apply the same procedure as for the mention, which adds about 1 million entity embedding phrases to the dictionary. The entity dictionary size (\rsize) has a final size of 4 million. In the test data, unknown mentions are replaced with an unknown mention token. Unknown context words are always replaced by an unknown context token.
		 
		 %\pagebreak
		\begin{figure}[h]
			\vspace{-0.2cm}
			%\RawFloats
			\centering
			\begin{minipage}{.48\textwidth}
				\centering
				\includegraphics[width=\linewidth]{Memory_Size_vs_Embedding_dimension}
				\caption{Both increasing the context memory size from 100 to 150 and increasing the embedding dimensions from 50 to 80 (whereby the 30 extra dimensions are 	initialized randomly) clearly improve the performance (see results shown in table \ref{tab:mdl})}
				\label{fig:msed}
			\end{minipage}
			\hfill
			\begin{minipage}{.48\textwidth}
				\centering
				\includegraphics[width=\linewidth]{Noise}
				\caption{Performance comparison when injecting 10\% random noise vs no noise (see results shown in table \ref{tab:mdl})}
				\label{fig:noise}
				\vspace{+35pt}
			\end{minipage}%
			\vspace{-0.3cm}
		\end{figure}

		 
		 \paragraph{Noise}
		 Similar to \cite{Sukhbaatar2015}, we inject random noise into the context by randomly replacing words (10\%) with other words from the dictionary. This effected a modest but consistent improvement in performance. Experiments were also done with randomly replacing some of the mentions (1 - 10\%) by the unknown mention token, in order to place more emphasis on extracting as much information from the context as possible. The effects were negative when replacing 10\%, and at lower values (1\%) the effects were positive, but on a small scale. More experiments would have to be done in order to determine whether this is statistically significant.
		 

		 \begin{figure}[H]
		 	%\RawFloats
		 	\centering
		 	\begin{minipage}{.48\textwidth}
		 		\centering
		 		\includegraphics[width=\linewidth]{Number_of_hops}
		 		\caption{Performance when increasing number of hops (see results shown in table \ref{tab:mdl})}
		 		\label{fig:hops}
		 		\vspace{+10pt}
		 	\end{minipage}%
		 	\hfill
		 	\begin{minipage}{.48\textwidth}
		 		\centering
		 		\includegraphics[width=\linewidth]{Candidates}
		 		\caption{Increasing the number of candidates from 20 to 64 reduces the performance (see results shown in table \ref{tab:mdl})}
		 		\label{fig:cand}
		 	\end{minipage}
		 \end{figure}
		 \paragraph{Parameters}
		 As seen in figure \ref{fig:hops}, the performance increases during the annealing stage of training when increasing the number of hops from 1 to 2 to 3, but tends to drop off above that, as is seen for 6 hops. This was quite unexpected, since previous research indicated that increasing the number of hops should increase the performance \cite{Sukhbaatar2015}, \cite{Bordes2015}, \cite{Kumar2015}, \cite{Weston2015}. Increasing the embedding dimensions from 50 to 80, even when initialized as [50 GloVe + 30 random], clearly improves the performance, as does increasing the memory size. Both of these measures, especially increasing the embedding dimensions, significantly increased the computation time, so that it was not possible to test higher dimensions such as 100dimensional GloVe vectors.\\
		 For a good balance between efficiency and accuracy loss we used 20 candidates. Moreover, figure \ref{fig:cand} shows how the performance dropped off when using too many candidates. From table \ref{tab:dset} we can infer that the reason for this is that the correct entity is in the top 20 candidates in 92.8\% of the times for aidaA, and increasing this to 64 candidates only adds another 3\% to reach 95.9\%. Even in the hypothetical case where Precision=1, the resulting f1 score would increase by an even smaller amount (1.5\%), as is seen in table \ref{tab:sco} when comparing the theoretical maxima at 20 and 64 candidates. The added problem complexity, however, leads the precision to decrease and thus to a decreased f1 score. This issue could possibly be solved by providing the information about the prior likelihood of each candidate $\hat{p}(\ecand | mention)$ to the system in some manner. One possibility would be via the weighted summation of the softmax output produced by the network and the statistical prior probability by adapting equation \ref{eq:pred} to:
		 \begin{equation}
			 \epred = \argmin_{\ecand \in \caset}(\gamma \cdot P_{mem}(\eout, \ecand)) + (1 - \gamma) \cdot \hat{p}(\ecand | mention))
		 \end{equation}
		 where $\gamma$ is a learned parameter and $P_{mem}$ is the original output of the network, interpreted as a probability.
		 \begin{equation}
			 P_{mem}(\eout, \ecand)) = Softmax(distance(\eout, \ecand))
		 \end{equation}
		 \input{tables/models}
		 
		 \pagebreak
		 
		 
		 

		 
