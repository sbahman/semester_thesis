tds = require 'tds'

print("reading glove from file")

local vocab_size = 400000
local word_vecs_size = 50
local nSpecial = 5  -- number od special tokens

local word2id = tds.Hash()
local id2word = tds.Hash()
local M = torch.zeros(vocab_size + nSpecial, word_vecs_size)

-- insert special tokens

word2id['<pad>'] = #id2word+1
id2word[#id2word+1] = '<pad>'

word2id['<unk>'] = #id2word+1
id2word[#id2word+1] = '<unk>'

word2id['<unkmen>'] = #id2word+1
id2word[#id2word+1] = '<unkmen>'

word2id['<eos>'] = #id2word+1
id2word[#id2word+1] = '<eos>'

word2id['<eol>'] = #id2word+1
id2word[#id2word+1] = '<eol>'


function split(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={} ; i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    t[i] = str
    i = i + 1
  end
  return t
end

M[{{1,nSpecial},{}}] = torch.randn(nSpecial, word_vecs_size)

--Reading Contents
local id = nSpecial
for line in io.lines(glopt.txtfilename) do
  id = id + 1
  local parts = split(line, " ")
  local word = parts[1]
  word2id[word] = id
  id2word[id] = word

  if id % 100000 == 0 then
    print('  Processed ' .. id .. ' lines')
  end
  for i=2, #parts do
    M[id][i-1] = tonumber(parts[i])
  end
end


print('adding lowercase words to glove')
for k,v in pairs(word2id) do    -- add all the lowercase versions of words to vocab
  if not word2id[k:lower()] then
    word2id[k:lower()] = v
    word2id[k] = nil
  end
end

local Oword2id = tds.hash()         -- original vocab used to get comp words
for k,v in pairs(word2id) do
  Oword2id[k] = v
end

glove = {}
glove.word2id = word2id
glove.id2word = id2word
glove.Oword2id = Oword2id
glove.comp = tds.hash()
glove.redir = tds.hash()
glove.vdim = M:size(2)

print('Writing glove vocab t7 File for future usage.')
torch.save(glopt.vocabfile,glove, binary, false)

print('Writing glove vectors t7 File for future usage.')
torch.save(glopt.mfile, M, binary, false)


return glove
