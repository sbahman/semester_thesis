--------- USAGE -------

--assuming model is in directory ./models/

--ls models/*.mdl | th csvlog.lua [options]

-----------------------

local csvigo = require 'csvigo'
require('paths')
paths.dofile('utilities.lua')


-- default directory to put logs if andir parameter is enabled
local analysisdir = '../analysis/logs/'

  local paramNames =  {
  'edim',
  'entdim',
  'cidim',
  'lindim',
  'memsize',
  'nhop',
  'ncand',
  'distance',
  'time',
  'init_std',
  'init_hid',
  'sdt',
  'anneal',
  'annealep',
  'annealbest',
  'maxgradnorm',
  'critavg',
  'batchsize',
  'optim',
  'noise',
  'unkmen',
  'logIncrement',
  'epochs',
  'initrand',
  'sgdSeq',
  'dict',
  'dt',
  'stateEpoch',
  'stateChunk',
}

  local order = {'wiki_f1', 'wiki_p', 'wiki_r', 'aidaA_f1', 'aidaA_p', 'aidaA_r', 'aidaB_f1', 'aidaB_p', 'aidaB_r', 'msnbc_f1', 'msnbc_p', 'msnbc_r', 'ace_f1', 'ace_p', 'ace_r', 'aquaint_f1', 'aquaint_p', 'aquaint_r'}


local function addtoLog(orig, f1, p, r)
  for i = 1, #orig do
    f1[#f1+1] = orig[i][1]
    p[#p+1] = orig[i][2]
    r[#r+1] = orig[i][3]
  end
end


local function getParams(ldparams, paramNames)
  local params = {}
  for k,v in ipairs(paramNames) do
    params[v] = {}
    if type(ldparams[v]) == "boolean" then
      if ldparams[v] then
        params[v][1] = "true"
      else
        params[v][1] = "false"
      end
    elseif type(ldparams[v]) == "string" then
      if ldparams[v] == '' then
        params[v][1] = "none"
      else
        params[v][1] = ldparams[v]
      end
    else
      params[v][1] = ldparams[v]
    end
  end
  return params
end

local function getLog(ldlog)
  local log = {}

  log.wiki_f1 = {}
  log.wiki_p = {}
  log.wiki_r = {}
  addtoLog(ldlog.wiki, log.wiki_f1, log.wiki_p, log.wiki_r)

  log.aidaA_f1 = {}
  log.aidaA_p = {}
  log.aidaA_r = {}
  addtoLog(ldlog.aidaA, log.aidaA_f1, log.aidaA_p, log.aidaA_r)

  log.aidaB_f1 = {}
  log.aidaB_p = {}
  log.aidaB_r = {}
  addtoLog(ldlog.aidaB, log.aidaB_f1, log.aidaB_p, log.aidaB_r)

  log.msnbc_f1 = {}
  log.msnbc_p = {}
  log.msnbc_r = {}
  addtoLog(ldlog.msnbc, log.msnbc_f1, log.msnbc_p, log.msnbc_r)

  log.ace_f1 = {}
  log.ace_p = {}
  log.ace_r = {}
  addtoLog(ldlog.ace, log.ace_f1, log.ace_p, log.ace_r)

  log.aquaint_f1 = {}
  log.aquaint_p = {}
  log.aquaint_r = {}
  addtoLog(ldlog.aquaint, log.aquaint_f1, log.aquaint_p, log.aquaint_r)
  
  return log
end




local function makecsv(mdlpath)
  local outdir
  if g_params.outdir == '' then
    outdir = paths.dirname(mdlpath) .. '/'
  else
    outdir = g_params.outdir
  end
  
  local basename = paths.basename(mdlpath, '.mdl')
  
  local base = outdir .. basename

  local d = torch.load(mdlpath, binary, false)
  g_compatibility(d, g_params.addEpochState)

  local savename = base .. '_log.csv'
  local saveparams = base .. '_params.csv'
  
  local ldp = d.params
  ldp.stateEpoch = d.state.epoch
  ldp.stateChunk = d.state.wikichunk
  
  local params = getParams(ldp, paramNames)
  local log = getLog(d.log)
  
  csvigo.save{data=log, path=savename, column_order=order}
  csvigo.save{data=params, path=saveparams, column_order=paramNames}
  
end



local cmd = torch.CmdLine()
cmd:option('--outdir', '', 'directory to put csv files in name')
cmd:option('--andir', true, 'if outdir is not given then it defaults to the analysis dir specified in the code')
cmd:option('--addEpochState', 1, 'if saved model does not have state.epoch then this value will be used')
g_params = cmd:parse(arg or {})



if g_params.outdir == '' and g_params.andir then
  g_params.outdir = analysisdir
end


for line in io.lines() do
  makecsv(line)
end






