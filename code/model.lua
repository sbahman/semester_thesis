-- Copyright (c) 2015-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree. An additional grant 
-- of patent rights can be found in the PATENTS file in the same directory.

require('nn')
require('sys')
require('nngraph')
local utf8 = require 'lua-utf8'

paths.dofile('LinearNB.lua')


local function build_memory(params, input, context, time, weights)
  -- params = parameters for making model; input = mention (nn.identity); context = nn.identity; time = positional embedding; weights = weights to explicitly return so one can set them later
    

    weights.csoft = {}--torch.Tensor(g_params.nhops, g_params.memsize)    -- soft max weighting of context
    
    local shareList = {}
    shareList[1] = {}   -- share query and context lookup table
    shareList[2] = {}
    
    local hid = {}
    local qu = nn.LookupTable(params.nwords, params.edim)(input)    --query
    table.insert(shareList[1], qu)
    weights.query = qu.data.module.weight
    
    hid[0] = nn.Linear(params.edim, params.cidim)(qu)
    
    local L_c = nn.LookupTable(params.nwords, params.edim)(context)
    weights.context = L_c.data.module.weight
    table.insert(shareList[1], L_c)
    
    local Ain
    local Bin
    
    if params.time == 'joi' then
      -- joint time embedding
      
      local L_t = nn.LookupTable(params.memsize, params.edim)(time)
      local Lin = nn.CAddTable()({L_c, L_t})

      local Lin2dim = nn.View(-1, params.edim)(Lin)

      local Ain2dim = nn.Linear(params.edim, params.cidim)(Lin2dim)
      local Bin2dim = nn.Linear(params.edim, params.cidim)(Lin2dim)

      Ain = nn.View(-1, params.memsize, params.cidim)(Ain2dim)
      Bin = nn.View(-1, params.memsize, params.cidim)(Bin2dim)
      
    elseif params.time == 'sep' then
      -- separate time embedding
      
      local L_c2dim = nn.View(-1, params.edim)(L_c)
      
      local Ain2dim = nn.Linear(params.edim, params.cidim)(L_c2dim)
      local Bin2dim = nn.Linear(params.edim, params.cidim)(L_c2dim)
      
      
      local Ain3 = nn.View(-1, params.memsize, params.cidim)(Ain2dim)
      local Bin3 = nn.View(-1, params.memsize, params.cidim)(Bin2dim)
      
      local L_t1 = nn.LookupTable(params.memsize, params.cidim)(time)
      local L_t2 = nn.LookupTable(params.memsize, params.cidim)(time)
      
      Ain = nn.CAddTable()({Ain3, L_t1})
      Bin = nn.CAddTable()({Bin3, L_t2})
    else
      print('unknown time embedding parameter')
    end
    
      

    nngraph.annotateNodes()

    for h = 1, params.nhop do
        -- inner product of context and state
        local hid3dim = nn.View(1, -1):setNumInputDims(1)(hid[h-1])
        local MMaout = nn.MM(false, true)
        if rgpu then MMaout = MMaout:cuda() end
        local Aout = MMaout({hid3dim, Ain})
        local Aout2dim = nn.View(-1):setNumInputDims(2)(Aout)
        --probabilistic context weighting
        local P = nn.SoftMax()(Aout2dim)
        weights.csoft[h] = P.data.module.output
        --weight the ouput context memory
        local probs3dim = nn.View(1, -1):setNumInputDims(1)(P)
        local MMbout = nn.MM(false, false)
        if rgpu then MMbout = MMbout:cuda() end
        local Bout = MMbout({probs3dim, Bin})
        --state propagation
        local C = nn.LinearNB(params.cidim, params.cidim)(hid[h-1])
        table.insert(shareList[2], C)
        --output
        local D = nn.CAddTable()({C, Bout})
        --apply ReLU
        if params.lindim == params.cidim then
            hid[h] = D
        elseif params.lindim == 0 then
            hid[h] = nn.ReLU()(D)
        else
            local F = nn.Narrow(2,1,params.lindim)(D)
            local G = nn.Narrow(2,1+params.lindim,params.cidim-params.lindim)(D)
            local K = nn.ReLU()(G)
            hid[h] = nn.JoinTable(2)({F,K})
        end
        nngraph.annotateNodes()
    end
    
    

    return hid, shareList
end


local function distance(pred, candidates, params)
    local distanceMeasure
    if params.distance == 'cos' then
      distanceMeasure = nn.CosineDistance
    elseif params.distance == 'pair' then
      distanceMeasure = nn.PairwiseDistance
    elseif params.distance == 'dot' then
      distanceMeasure = nn.DotProduct
    else
      print 'unknown distance measure'
    end
      
      
    local distances = {}
    local cview = nn.View(-1, params.ncand, params.entdim)(candidates)
    for i = 1, params.ncand do
        local cand = nn.Select(2, i)(cview)        
--        local p = nn.Select(1, i)(pred)
--        local rep = nn.Replicate(params.ncand, 1)(p)
        local dist = nil
        if params.distance == 'pair' then
          dist = distanceMeasure(2)({pred, cand})
        else
          dist = distanceMeasure()({pred, cand})
        end
        distances[i] = nn.View(-1, 1)(dist)
    end
    local join = nn.JoinTable(2)(distances)
    return join
--    return nn.View(-1 ,params.ncand)(join)
end
      

function g_build_model(params)
    local weights = {
      query = nil,
      context1 = nil,
      context2 = nil,
      target = nil,
      csoft = nil,
      --esoft = nil
    }
    
    local input = nn.Identity()()
    --local target = nn.Identity()()    --index of correct class
    local context = nn.Identity()()
    local time = nn.Identity()()
    local candidates = nn.Identity()()
    local hid, shareList = build_memory(params, input, context, time, weights)
    --convert to get pred. entity
    local z = nn.Linear(params.cidim, params.entdim)(hid[#hid])
    --embedd candidate entities
    local cand = nn.LookupTable(params.nent, params.entdim)(candidates)
    --get weights of cand entities
    weights.target = cand.data.module.weight
    
    local dist = distance(z, cand, params)     --distance of prediciton z to each candidate entity
    --local costl = nn.MultiMarginCriterion()
    local pred = nn.LogSoftMax()(dist)

    nngraph.annotateNodes()
    --finalize model
    local model = nn.gModule({input, context, time, candidates}, {pred})
    if rgpu then model = model:cuda()end
    
    -- IMPORTANT! do weight sharing after model is in cuda
    for i = 1,#shareList do
        local m1 = shareList[i][1].data.module
        for j = 2,#shareList[i] do
            local m2 = shareList[i][j].data.module
            m2:share(m1,'weight','bias','gradWeight','gradBias')
        end
    end
    return model, weights
end
