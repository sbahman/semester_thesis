#!/usr/bin/env python
# -*- coding: utf-8

import io
import codecs
import re
import sys




source = None
mentionFile = None
docPath = None

print 'Argument List:', str(sys.argv)


if len(sys.argv) == 4:
    print "hellooo"
    source = sys.argv[1]
    mentionFile = sys.argv[2]
    docPath = sys.argv[3]
else:
    dataPath = "/cluster/scratch/sbahman/"
    source = dataPath + "textWithAnchorsFromAllWikipedia2014Feb.txt"
    mentionFile = dataPath + 'mentions'
    docPath = dataPath + "docs/"





mentionf = codecs.open(mentionFile, encoding='utf-8', mode='w')





def process_file(fname):
    global docPath
    pat2 = re.compile("(^.*?)(<a\shref=\")(.*?)(\">)(.*?)(</a>)(.*)", re.S)
    with codecs.open(fname, encoding='utf-8') as f:
        elid = None
        offset = 0
        mlength = 0
        target = None
        for line in f:
            if line[:6] == "</doc>":
                offset = 0
                mlength = 0
                target.close()
                target = None
                elid = None
            elif line[:8] == "<doc id=":
                elid = getId(line)
                target = codecs.open(docPath + str(elid),
                                     encoding='utf-8', mode='w')
                print elid
            else:
                tailstring = line
                tmp = pat2.match(tailstring)
                while tmp is not None:
                    headtext = tmp.group(1)
                    wikiname = tmp.group(3)
                    mention = tmp.group(5)
                    tailstring = tmp.group(7)

                    target.write(headtext + mention)

                    # text += headtext
                    offset += len(headtext)
                    mlength = len(mention)
                    writeMention(mention, wikiname, offset, mlength, elid)
                    # text += mention
                    offset += mlength
                    tmp = pat2.match(tailstring)
                if tailstring is not None:
                    target.write(tailstring)


def getId(line):
    pat1 = re.compile(".*id=\"(.*?)\"")
    elid = pat1.match(line).group(1)
    return int(elid)


def getMention(line, text, elid):
    pat2 = re.compile("(^.*?)(<a\shref=\")(.*?)(\">)(.*?)(</a>)(.*)")
    tmp = pat2.match(line)
    while tmp is not None:
        text += tmp.group(1)
        wikiname = tmp.group(3)
        mention = tmp.group(5)
        tailstring = tmp.group(7)

        offset = len(text)
        length = len(mention)
        writeMention(mention, wikiname, offset, length, elid)
        text += mention
        tmp = pat2.match(tailstring)
    return text


def writeMention(mention, wikiname, offset, length, docid):
    global mentionf
    mentionf.write(mention + '\t' + wikiname + '\t' + str(offset) + '\t' +
                   str(length) + '\t' + str(docid) + '\n')



process_file(source)


mentionf.close()
