#!/usr/bin/env python
# -*- coding: utf-8


import lxml.etree as ET
import io
import codecs

output = False 

dataPath = "data/"

target = None
mentionf = codecs.open(dataPath + 'mentions', encoding='utf-8', mode='w')


def fast_iter(context, func, *args, **kwargs):
    """
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    """
    for event, elem in context:
        func(elem, *args, **kwargs)
        # It's safe to call clear() here because no descendants will be
        # accessed
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context



count = 0
elsig = 0

ignoretags = ['br', 'math', 'syntaxhighlight']

def process_element(elem, dpath):
    global target
    global output
    global count
    global elsig
    global ignoretags
    elid = 0

    if elem.tag == 'doc':
        #close current doc and open new one
        if target is not None:
            target.close()
        elid = elem.get('id')
        fname = dpath + 'docs/' + elid
        target = codecs.open(fname, encoding='utf-8', mode='w')
        text = elem.text if elem.text is not None else ""
    else:
        print elem.tag
        raise ValueError('non Doc element found')
    if int(elid) > elsig + 1:
        elsig = int(elid)
        print elsig
    offset = 0
    length = 0
    handeled = None
    lastelem = None
    for subel in elem.iterdescendants():
        # print subel.text
        if subel.tag == 'a':
            wikiname = subel.get('href')
            lastelem = wikiname
            mentionstring = subel.text if subel.text is not None else ""
            if subel.__len__() != 0:
                assert(subel.__len__() == 1)
                for subsubel in subel.iterdescendants():
                    print elid
                    print subsubel.tag
                    handeled = subsubel.tag
                    mentionstring += '<' + subsubel.tag + '>'
                    if subsubel.tail is not None:
                        mentionstring += subsubel.tail
            else:
                handeled = None
            
            offset = len(text)
            length = len(mentionstring)
            text += mentionstring
            if subel.tail is not None:
                text += subel.tail
            if output:
                writeMention(mentionstring, wikiname, offset, length, elid)
            # print subel.get('href')
            # print subel.text

        # handle all sorts of weird tags that occur and produce usable file
        else:
            if subel.tag == handeled:
                continue
            elif subel.tag in ignoretags:
            	continue

            # elif subel.tag == 'br':
            #     assert(subel.__len__() == 0)
            #     if subel.tail is not None:
            #         text+= subel.tail
            # elif subel.tag == 'math':
            #     # equation is unlikely to contain any usable information
            #     # or mentions therefore do nothing
            #     continue
            # elif subel.tag == 'syntaxhighlight':
            #     # assert(subel.__len__() == 0)
            #     # text += subel.text
            #     continue

            # specific buggs
            elif elid == '1325' and subel.tag == 'permanyer':
                text += '<Permanyer, either 1996 or 2008>\n'
            elif elid == '1634':
                continue
            elif elid == '2251' and subel.tag == 'velten':
                continue
            else:
                print elid
                print subel.tag
                count += 1
                print count
                # print subel.text
                # print subel.tail
                # print lastelem
                # raise ValueError('unknown element tag detected')
            handeled = None

    if output:
        target.write(text)


def writeMention(mention, wikiname, offset, length, docid):
    global mentionf
    mentionf.write(mention + '\t' + wikiname + '\t' + str(offset) + '\t' 
            + str(length) + '\t' + str(docid) +'\n')


source = "data/textWithAnchorsFromAllWikipedia2014Feb.txt"




context = ET.iterparse( source, tag='doc', html='true', encoding='utf-8' )

fast_iter(context,process_element, dataPath)

target.close()
mentionf.close()
