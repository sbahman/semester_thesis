#!/bin/sh

euler=/cluster/scratch/sbahman/
desktop=/home/severin/dev/sem_severin_bahman/code/entity-MemN2N/data/

dataPath=$euler
docPath="$dataPath"docs2/
i=0
for filename in "$dataPath"chunks/*; do
	#echo "$filename"
	mentionFile=$dataPath"mentionChunks/mentions""$i"
	#echo $mentionFile
	bsub -W 4:00 python manualformdata.py $filename $mentionFile $docPath
	i=$((i+1))
done
