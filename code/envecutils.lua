require('paths')
local stringx = require('pl.stringx')
local tds = require('tds')
local file = require('pl.file')



local fnames = {
  envec = 'data/envecutils/gloveEntityVectors_freq3_lr0.001_ep160_sgd_PMI_withZ_withBias.txt',
  emprob = 'data/envecutils/mek-top-freq-crosswikis-plus-wikipedia-lowercase-top64.txt',
  enwikititles = 'data/envecutils/enwiki-titles.txt',
  enwikiredir = 'data/envecutils/enwiki-redirect-normalized.txt',
  
  saveenvectors = 'data/envecutils/envectors.t7',
  saveprob = 'data/envecutils/prob.t7',
  savefile = 'data/envecutils/envec.t7',
}

local function tokenize(line)
  local strippedLine = line:gsub("[,!?:;\\><{}%[%]()|.\"%s]+", " ")   --replace all these characters with whitespace

  strippedLine = stringx.strip(strippedLine)    -- remove trailing or leading white spaces
  return stringx.split(strippedLine)
end

local function load_envec(fnames)
  print('loading entity vectors')
  local vIdx = tds.hash() -- vIdx[wikipedia ID] = index of entity vector in enVectors
  local ivIdx = tds.hash()
  local c = 0
  local embdim = nil
  for line in io.lines(fnames.envec) do
    embdim = embdim or (#(stringx.split(line))-1)
    c = c+1
  end
  c = c + 1 -- add one for padding
  local enVectors = torch.Tensor(c, embdim)
  local k = 1         -- k = 1 because I will now add first vector which is the padding vector used when less than 20 candidates are available in testing (should never match)
  enVectors[k] = torch.randn(embdim)
  vIdx[0] = k   -- coresponding wiki id of padding vector is 0
  ivIdx[k] = 0
  for line in io.lines(fnames.envec) do
    k = k+1
    local d = stringx.split(line)
    local wikiId = tonumber(d[1])
    vIdx[wikiId] = k
    ivIdx[k] = wikiId
    for i = 2, #d do
      enVectors[k][i-1] = tonumber(d[i])
    end
  end
  print('done')
  return enVectors, vIdx, ivIdx
end


local function load_wikiTitles(fnames)
  print('loading wiki titles')
  local wtitles = tds.hash()
  wtitles['<candpad>'] = 0
  for line in io.lines(fnames.enwikititles) do
    t = stringx.split(line, '\t')
    wtitles[t[1]] = tonumber(t[2])
  end
  for k,v in pairs(wtitles) do
    if not wtitles[k:lower()] then
      wtitles[k:lower()] = v
      wtitles[k] = nil    -- only keep lower case titles if not both present
    end
  end
  
  local iwtitles = tds.hash()
  for k,v in pairs(wtitles) do
    iwtitles[v] = k
  end
  print('done')
  return wtitles, iwtitles
end


local function load_enprob(fnames)
  print('loading entity probabilities')
  local c = 0
  for line in io.lines(fnames.emprob) do
    local w = stringx.split(line, '\t')
    c = c + #w - 4
  end
  local mvocab = tds.hash()
  local mivocab = tds.hash()
  local mprob = torch.Tensor(c)
  c = 1
  for line in io.lines(fnames.emprob) do
    local w = stringx.split(line, '\t')
    local men = w[1]
    mivocab[#mvocab+1] = men
    mvocab[men] = tds.Vec({c, #w -4})     -- position of candidate entities and number of candidate entities
    for i = 5, #w do
      local a = stringx.split(w[i], ',')
      mprob[c] = a[1]
      c = c + 1
    end
  end 
  print('done')
  return mvocab, mprob
end


local function getWikiIdFromTitle(wikiTitles, title)
  local id = wikiTitles[title]
  if not id then
    id = wikiTitles[title:lower()]
    return id, title:lower()
  else
    return id, title
  end
end


local function applyTitleRedirects(fnames, wikiTitles, iwikiTitles)
  print('applying title redirects')
  local idRedirects = tds.hash()
  for line in io.lines(fnames.enwikiredir) do
    local w = stringx.split(line, '\t')
    local origID, origTi = getWikiIdFromTitle(wikiTitles, w[1])
    local targetID, targetTi = getWikiIdFromTitle(wikiTitles, w[2])
    if targetID then
      wikiTitles[origTi] = targetID
      if origID then
        iwikiTitles[origID] = targetTi
      end
--    else
--      wikiTitles[origTi] = nil
--      if origID then
--        iwikiTitles[origID] = nil
--      end
    end
    if targetID and origID then
      idRedirects[origID] = targetID
    end
    
  end

  print('done')
  return idRedirects
end

local function applyIDRedirects(fnames, idRedirects, vIdx)
  print('applying Id redirects')
  for k,v in pairs(idRedirects) do
    if vIdx[v] then
      vIdx[k] = vIdx[v]
    end
  end
  print('done')
end
  
  
local function getComp(title, vIdx, wtitles)
  local words = tokenize(title:lower())
  local c = 0
  local comp = tds.Vec()     -- indexes of compound words
  local r = ''  -- build up compound word
  local f = false
  for k, w in ipairs(words) do
    local wikiID, title = getWikiIdFromTitle(wtitles, w)
    if wikiID then
      local i = vIdx[wikiID]
      if i then
        if f then 
          r = r .. " "
        end
        f = true
        r = r .. w
        comp[#comp+1] = i
        c = c + 1
      end
    end
  end
  if c == 0 then 
    return nil, nil
  else
    return comp, r
  end
end
 
local function buildCompVectors(wikiTitles, vIdx, ivIdx, enVectors)
  print 'building Compound Entities'
  local comp = tds.hash()
  local vIdxOrig = tds.hash()   -- so I dont get compounds of words not in original dictionary
  for k,v in pairs(vIdx) do
    vIdxOrig[k] = v
  end
  
  for k,v in pairs(wikiTitles) do
    if not vIdx[v] then
      local co, red = getComp(k, vIdxOrig, wikiTitles)
      if co then
        local rId, rTi = getWikiIdFromTitle(wikiTitles, red)-- check if redirect word already in vocab
        if rId and vIdx[rId] then
          vIdx[v] = vIdx[rId]
        else
          vIdx[v] = #ivIdx+1
          ivIdx[#ivIdx+1] = v
          comp[#comp +1] = co
        end
      else
        -- if could not find vector for id then use rand
        vIdx[v] = #ivIdx+1
        ivIdx[#ivIdx+1] = v
        comp[#comp +1] = 0  -- when building tensor make a random vector
      end
    end
  end
  
  
  local enDim = enVectors:size(2)
  
  local compTensor = torch.Tensor(#comp, enDim):zero()
  
  for i = 1, #comp do
    if comp[i] == 0 then
      compTensor[i] = torch.randn(enDim)
    else
      for j = 1, #comp[i] do
        compTensor[i] = compTensor[i] + enVectors[comp[i][j]]
      end
      compTensor[i] = compTensor[i]/#comp[i]
    end
  end
  
  print 'done'
  return torch.cat({enVectors, compTensor}, 1)
end







local envecutils = {}
if not (paths.filep(fnames.savefile) and paths.filep(fnames.savefile)) then
  local wikiTitles, iwikiTitles = load_wikiTitles(fnames)
  local enVectors, vIdx, ivIdx =  load_envec(fnames)
  local idRedirects = applyTitleRedirects(fnames, wikiTitles, iwikiTitles)
  enVectors = buildCompVectors(wikiTitles, vIdx, ivIdx, enVectors)
  --applyIDRedirects(fnames, idRedirects, vIdx)
  
  local enDim = enVectors:size(2)
  
  envecutils.ivIdx = ivIdx
  envecutils.iwtitles = iwikiTitles
  envecutils.enDim = enDim
  envecutils.vIdx = vIdx
  envecutils.wtitles = wikiTitles
  --envecutils.redir = tds.hash()
  envecutils.comp = tds.hash()
  

  torch.save(fnames.saveenvectors, enVectors, binary, false)
  torch.save(fnames.savefile,envecutils, binary, false)
  print('done loading entity vectors')
  print('Writing t7 File for future usage.')
  enVectors = nil

else
  envecutils = torch.load(fnames.savefile, binary, false)
  print('done loading envecutils base from t7')
end




collectgarbage()


envecutils.size = function(self)
  return self.enDim
end

envecutils.numEnt = function(self)
  return #self.ivIdx
end

envecutils.loadEnvectors = function(self)
  self.enVectors = torch.load(fnames.saveenvectors, binary, false)
end

envecutils.getIDfromWikiID = function(self, wikiID)
  return self.vIdx[wikiID]
end

envecutils.getTitlefromWikiID = function(self, wikiID)
  return self.iwtitles[wikiID]
end

envecutils.getWikiIdfromEnID = function(self, enID)
  return self.ivIdx[enID]
end

envecutils.getTitlefromEnID = function(self, enID)
  local wikiID = self:getWikiIdfromEnID(enID)
  if wikiID then
    return self:getTitlefromWikiID(wikiID)
  else
    return nil
  end
end

envecutils.printWords = function(self, entityTensor)
  entityTensor:apply(function(x)
      print(self:getTitlefromEnID(x))
    end)
end

envecutils.getWords = function(self, entityTensor)
  local words = {}
  entityTensor:apply(function(x)
      words[#words+1] = self:getTitlefromEnID(x)
    end)
  return words
end

envecutils.printWikiID = function(self, entityTensor)
  entityTensor:apply(function(x)
      print(self:getWikiIdfromEnID(x))
    end)
end

envecutils.getIDfromTitle = function(self, wikiTitle)
  local wikiID = getWikiIdFromTitle(self.wtitles, wikiTitle)
  
  if wikiID then 
    return self.vIdx[wikiID]
  else
    return nil
  end
end

envecutils.getEnTensor = function(self)
  if self.enVectors then
    return self.enVectors
  else
    return torch.load(fnames.saveenvectors, binary, false)
  end
end


--envecutils.getIDfromTitle = function(self, wikiTitle)
--  local wikiID, title = getWikiIdFromTitle(self.wtitles, wikiTitle)
--  local id = nil
--  if wikiID then
--    local id = self.vIdx[wikiID]
--    if not id then
--      local comp, red =  self:getComp(title)    -- not really necessary any more!
--      if comp then
--        local rId, rTi = getWikiIdFromTitle(self.wtitles, red)-- check if redirect word already in vocab
--        if rId then
--          id = self.vIdx[rId]
--        end
--        if id then
--          self.vIdx[wikiID] = self.vIdx[rId]
--        else
--          id = #self.vIdx+1
--          self.vIdx[wikiID] = #self.vIdx+1
--          self.ivIdx[#self.vIdx+1] = wikiID
--          self.comp[#self.comp +1] = comp
--        end
--      else
--        -- if could not find vector for id then use rand
--        id = #self.vIdx+1
--        self.vIdx[wikiID] = #self.vIdx+1    -- should it now be ividx!!
--        self.ivIdx[#self.vIdx+1] = wikiID
--        self.comp[#self.comp +1] = 0  -- when building tensor make a random vector
--      end
--    end
--    return id
--  else
--    return nil
--  end
--end


--envecutils.getComp = function(self, title)
--  local words = tokenize(title)
--  local c = 0
--  local comp = tds.Vec()     -- indexes of compound words
--  local r = ''  -- build up compound word
--  local f = false
--  for k, w in ipairs(words) do
--    local wikiID, title = getWikiIdFromTitle(self.wtitles, w)
--    if wikiID then
--      local i = self.vIdx[wikiID]
--      if i then
--        if f then 
--          r = r .. " "
--        end
--        f = true
--        r = r .. w
--        comp[#comp+1] = i
--        c = c + 1
--      end
--    end
--  end
--  if c == 0 then 
--    return nil, nil
--  else
--    return comp, r
--  end
--end


--envecutils.getEnTensor = function(self)
--  local enV = nil
--  if self.enVectors then
--    enV = self.enVectors
--  else
--    enV = torch.load(fnames.saveenvectors, binary, false)
--  end
--  local compTensor = torch.Tensor(#self.comp, self.enDim):zero()
--  for i = 1, #self.comp do
--    if self.comp[i] == 0 then
--      compTensor[i] = torch.randn(self.enDim)
--    else
--      for j = 1, #self.comp[i] do
--        compTensor[i] = compTensor[i] + enV[self.comp[i][j]]
--      end
--      compTensor[i] = compTensor[i]/#self.comp[i]
--    end
--  end
  
--  local enVectors = torch.cat({enV, compTensor}, 1)
--  torch.save(fnames.saveenvectors, enVectors, binary, false)
--  print('Saving expanded entity tensor')
  
--  local enu = {}
--  enu.ivIdx = self.ivIdx
--  enu.iwtitles = self.iwtitles
--  enu.enDim = self.enDim
--  enu.vIdx = self.vIdx
--  enu.wtitles = self.wtitles
--  enu.comp = tds.hash()
--  torch.save(fnames.savefile,enu, binary, false)
--  print('Saving expanded envec base')
--  return enVectors
--end



if not paths.filep(fnames.saveprob) then
  local mvocab, mprob = load_enprob(fnames)
  local c = 0
  local k = 0
  local function enIDfromWikiID(wikiID)
    k = k + 1
    local ID = envecutils.vIdx[wikiID]
    if ID then
      return ID
    else 
      c = c + 1
      return envecutils.vIdx[0]
    end
  end
  
  mprob:apply(enIDfromWikiID)     -- convert mprob from wikiID to enID
  print ("could not find " .. c .. " out of " .. k .. " candidate entities")
--  print(mprob:nElement())
--  for i = 1, mprob:nElement() do
--    mprob[i] = envecutils.vIdx[mprob[i]]
--  end
  
  local prob = {}
  prob.mvocab = mvocab
  prob.mprob = mprob
  
  print('saving candidate probabilities')
  torch.save(fnames.saveprob, prob, binary, false)
  
end


envecutils.loadEnProb = function(self)
  print('loading entity probabilities')
  local prob = torch.load(fnames.saveprob, binary, false)
  self.mprob = prob.mprob
  self.mvocab = prob.mvocab
  collectgarbage()
end



--envecutils.getCandidates = function(self, mention, nCandidates, targetWiki)
--  local targetID = self:getIDfromTitle(targetWiki)
--  local candidates = {}
--  local probID = self.mvocab[mention:lower()]
--  local c = nil
--  if probID then
--    c = self.mprob[probID]
--  end
--  local targetCand = nil
--  if c then
--    local n = #c
--    -- get candidates
--    for i = 1,n do
--      local id = self:getIDfromWikiID(tonumber(c[i]))
--      if id then
--        candidates[#candidates+1] = id
--        if id == targetID then
--          targetCand = #candidates
--        end
--      end
--      if #candidates >= nCandidates then
--        break
--      end
--    end
    
--    -- fill up with random
--    for i = #candidates+1, nCandidates do
--      candidates[i] = math.random(self.nEnt)
--      if candidates[i] == targetID then
--        targetCand = i
--      end
--    end
      
--    return candidates, targetCand
--  else 
--    return nil, nil
--  end
    
--end


envecutils.getCandidates = function(self, mention, targetWiki)
  local targetCandID = nil
  local cand = nil

  
  local cpos = self.mvocab[mention:lower()]
  if cpos then
    cand = self.mprob:narrow(1, cpos[1], cpos[2])
    
    if targetWiki then
      local targetID = self:getIDfromTitle(targetWiki)
      if targetID then
        local targetCand = cand:eq(targetID):nonzero()
        if targetCand:nElement() > 0 then
          targetCandID = targetCand[1][1]
        end
      end
    end
  end
  
  return cand, targetCandID
    
end

envecutils.getCandidatesWikiID = function(self, mention, targetWikiID)
  local targetCandID = nil
  local cand = nil
  local targetID = nil

 
  local cpos = self.mvocab[mention:lower()]
  if targetWikiID then
    targetID = self:getIDfromWikiID(targetWikiID)
  end
  
  if cpos then
    cand = self.mprob:narrow(1, cpos[1], cpos[2])

    if targetID then
      local targetCand = cand:eq(targetID):nonzero()
      if targetCand:nElement() > 0 then
        targetCandID = targetCand[1][1]
      end
    end
  end

  return cand, targetCandID
    
end

envecutils.getVec = function(self, wikiID)
  local index = self.vIdx[wikiID]
  if index then
    return self.enVectors[index]
  else
    return nil
  end
end




envecutils.freeProb = function(self)
  self.mprob = nil
  self.mvocab = nil
  self.mivocab = nil
  collectgarbage()
end


envecutils.freeMemory = function(self)
  self.vIdx = nil
  self.ivIdx = nil
  self.wtitles = nil
  self.iwtitles = nil
  self.enVectors = nil
  self.mvocab = nil
  self.mivocab = nil
  self.mprob = nil
  collectgarbage()
end

collectgarbage()

return envecutils



