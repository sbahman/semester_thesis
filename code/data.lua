-- Copyright (c) 2015-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree. An additional grant 
-- of patent rights can be found in the PATENTS file in the same directory.

require('paths')
require('nn')

SLAXML = require 'SLAXML/slaxml'
local file = require('pl.file')
local stringx = require('pl.stringx')
local utf8 = require 'lua-utf8'



local function mergeTable(tableOfTensors)
  local dim = tableOfTensors[1]:nElement()
  local merge=nn.Sequential()
                :add(nn.JoinTable(1))
                :add(nn.View(-1,dim))
  return merge:forward(tableOfTensors)
end

local function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local function tokenize(line)
  local strippedLine = line:gsub("[,:;\\><{}%[%]()|\"%s]+", " ")   --replace all these characters with whitespace
  strippedLine = strippedLine:gsub("[!?]", ' <eos>')    -- replace question mark and exclamation mark with end of sentence token including whitespace before such that it will be recognized as separate word by split
  strippedLine = stringx.strip(strippedLine)    -- remove trailing or leading white spaces
  return stringx.split(strippedLine)
end

g_tokenize = tokenize -- define a global version



---------------- UALBERTA LOADER ---------------------


function g_read_ualberta(files, dict, envecutils, params)
  
  local contextUNK = 1    -- 1: if context word is unknown then replace with <unk>
  local mentionUNK = 2    -- 2: if mention word is unknown then replace with <unkmen>
  
  
  local docnames = {}
  local docdir = params.dictDir .. 'ualberta/'
  local dir = files.dir
  local xmlname = files.name .. '.xml'
  
  paths.mkdir(docdir)
  
  
  local stats = {
    total = 0,      -- total examples
    pos = 0,        -- total extracted mention examples
    fNeg = 0,       -- total false negatives
    rNegPos = 0,    -- fNeg/pos = average ration of false negatives to positives, used to calculate f1 over subset of docs
    fmen = 0,       -- #mention has no word embedding
    fcand = 0,      -- #no candidates found for mention
    fenID = 0,      -- #true entity is not in candidates
    fenTop = 0,     -- #correct entity was in candidates but not in top N
    noEn = 0,       -- there was a mention but no ground truth entity was given
    unkmen = 0      -- number of times a mention was unknown (and unkmen embedding was used)
  }
  
  stats.contextUNK = contextUNK
  stats.mentionUNK = mentionUNK
  stats.addtoCand  = false
  stats.ncand = params.ncand
  
  
	local mention = {}
	local wiki = {}
  local candidateTable = {}
	local doc = {}
	local doc_idx = {}
	local offset = {}
	local length = {}
  local documents = {}
	local ndoc = 0


	local fname = dir .. xmlname
  
  print(fname)
  
  local unkTok = dict:getId('<unkmen>')
  
  local el, menID, men, wik, off, len, enIdx
  
	local parser = SLAXML:parser{
		startElement = function(name,nsURI)
			el = name
		end,

		closeElement = function(name, nsURI)
			if name == 'annotation' then
				if men and wik and off and len then
          menID = dict:addToVocab(men, mentionUNK)
          
          --- get stats
          stats.total = stats.total + 1
          
          if not menID then
            stats.fmen = stats.fmen +1
          end
          if men == unkTok then
            stats.unkmen = stats.unkmen + 1
          end
          cand, enIdx = envecutils:getCandidates(men, wik)
          
          if not cand then
            stats.fcand = stats.fcand + 1
          end


          if menID and cand then
            if enIdx and enIdx <= params.ncand then
              local nc = math.min(cand:size()[1], params.ncand)
              local candidates = torch.ones(params.ncand)       -- one is the "no candidate" index which is allways wrong, can in training replace with random if desired
              candidates[{{1,nc}}] = cand[{{1,nc}}]
              candidateTable[#candidateTable+1] = candidates
              mention[#mention + 1] = menID
              wiki[#wiki + 1] = enIdx
              offset[#offset + 1] = tonumber(off)
              length[#length + 1] = tonumber(len)
              doc_idx[#doc_idx + 1] = #doc 
              --table.insert(data_t, {vocab[men], off, vocab[wik], #doc})
            else
              local enID = envecutils:getIDfromTitle(wik)
              if enID then
                stats.fenTop = stats.fenTop + 1
                stats.fNeg = stats.fNeg + 1
              else
                stats.fenID = stats.fenID + 1
                stats.fNeg = stats.fNeg + 1
              end
            end
          else
            stats.fNeg = stats.fNeg + 1
          end

        end
        men = nil
        wik = nil
				off = nil
			end
			el = nil
		end,

		attribute = function(name,value,nsURI)
			if el == 'document' and name == 'docName' then
				doc[#doc + 1] = value
			end
		end,

		text = function(text)
			if 		el == 'mention'  then men = text
			elseif 	el == 'wikiName' then wik = text
			elseif	el == 'offset'	 then off = text
			elseif	el == 'length'	 then len = text
			end
		end
	}
	
	local myxml = io.open(fname):read('*all')
	
	parser:parse(myxml)


	local rawDocs = {}
  

	for d = 1,#doc do
		local docname = dir .. "RawTexts/" .. doc[d] 
		local c_document = file.read(docname)
		rawDocs[d] = c_document
		local lines = stringx.splitlines(c_document)
		local c = 0
		for n = 1,#lines do
      --print(lines[n])
      local w = tokenize(lines[n])
      for wk = 1,#w do
        if stringx.endswith(w[wk], '.') then    -- check if period is eos or part of abreviation
          if not dict:addToVocab(w[wk], 0) then
            c = c + 1
          end
        end
      end
        
			c = c + #w + 1		-- count words (+1 for end of line token)
		end
		c = c + params.memsize -- accounting for padding
		local words = torch.Tensor(c, 1):fill(dict:addToVocab('<pad>'))
		c = math.ceil(params.memsize/2)
		for n = 1, #lines do
			local w = tokenize(lines[n])
			for i = 1, #w do    
        if stringx.endswith(w[i], '.') and not dict:addToVocab(w[i], 0) then    -- check if period is eos or part of abreviation
          c = c + 1
          words[c][1] = dict:addToVocab(w[i]:gsub("%.$", ""), contextUNK)
          c = c + 1
          words[c][1] = dict:addToVocab('<eos>')
        else
          c = c + 1
          words[c][1] = dict:addToVocab(w[i], contextUNK)
        end
			end

			c = c + 1
			words[c][1] = dict:addToVocab('<eol>')
		end

		documents[d+ndoc] = words
	end

	local data_t = {}

	for m = 1,#mention do
		local d = rawDocs[doc_idx[m]]
		local pre_string = utf8.sub(d, 1, offset[m])
		local pre_lines = stringx.splitlines(pre_string)
		local c = 1 -- indexes start 1
		for n = 1, #pre_lines do
			if n > 1 then c = c + 1 end
				-- account for eol at line break 
				--(there is none in front of mention) ..... but what if there is?
			local w = tokenize(pre_lines[n])
      for wk = 1,#w do
        if stringx.endswith(w[wk], '.') and not dict:addToVocab(w[wk], 0) then    -- check if period is eos or part of abreviation
          c = c + 1
        end
      end
			c = c + #w -- count words including
		end
		c = c + math.ceil(params.memsize/2) -- take padding in to account

		if utf8.find(dict:getWordFromID(mention[m]), '&') then
			length[m] = length[m] - 4 	-- for some reason & is represented 
										-- as &amp; in the xml file 
										-- and so we have to subtract 4 chars
		end

		local mention_string  = utf8.sub(d, offset[m]+1, offset[m]+length[m])
--		if mention_string ~= dict:getWordFromID(mention[m]) then
--			print('!!!!!!!!!!!!!!!!!!!!')
--			print(mention_string)
--			print(dict:getWordFromID(mention[m]))
--			print(offset[m])
--			print(length[m])
--			print(d)
--		end
		local mention_lines = stringx.splitlines(mention_string)
		local k = 0
		for n = 1,#mention_lines do
			if n > 1 then
				k = k + 1 	-- only account for eol if line break 
							-- (there is none at end of the mention)
			end
			local w = stringx.split(mention_lines[n])
			k = k + #w
		end
    
    -- mention and wiki as vocab index
    -- doc_idx is index the document can be found at in documents
    -- c is the position of the mention in the document
    -- k is the number of words in the mention
		data_t[m] = {mention[m], wiki[m], doc_idx[m] + ndoc, c, k}
  end
  
  stats.pos = stats.total - stats.fNeg
  stats.rNegPos = stats.fNeg/stats.pos
  docnames[1] = docdir .. files.name .. '.t7'
  local datafile = {}
  local candidateTensor = mergeTable(candidateTable)
  local datatens = torch.Tensor(data_t)
  datafile.data = torch.cat(datatens, candidateTensor, 2)
  datafile.docs = documents
  torch.save(docnames[1], datafile)
  print('writing ' .. docnames[1])
  print(stats)

	local out = {}
  out.docnames = docnames
  out.stats = stats
	return out

end



------------------------- WIKI LOADER ---------------------------------




function g_read_wiki(fname, dict, envecutils, params)
  local docdir = params.dictDir .. 'wiki_files/'
  paths.mkdir(docdir)
  local addtoCand = true    -- add entity to candidates if it is not present (for training)
  
  local contextUNK = 1    -- 1: if context word is unknown then replace with <unk>
  local mentionUNK = 3    -- 3: if mention word is unknown then create new random embedding for it
  
--  local testContextUNK = 1
--  local testMentionUNK = 2
  
  
  local stats = {
    total = 0,      -- total examples
    pos = 0,        -- total extracted mention examples
    fNeg = 0,       -- total false negatives
    rNegPos = 0,    -- fNeg/pos = average ration of false negatives to positives, used to calculate f1 over subset of docs
    fmen = 0,       -- #mention has no word embedding
    fcand = 0,      -- #no candidates found for mention
    fenID = 0,      -- #true entity is not in candidates
    fenTop = 0,     -- #correct entity was in candidates but not in top N
    noEn = 0,       -- there was a mention but no ground truth entity was given
    unkmen = 0      -- number of times a mention was unknown (and unkmen embedding was used)
  }
  
  stats.contextUNK = contextUNK
  stats.mentionUNK = mentionUNK
  stats.addtoCand  = addtoCand
  stats.ncand = params.ncand
  
--  local teststats = {
--    total = 0,  -- total examples
--    fNeg = 0,   -- total false negatives
--    fmen = 0,   -- false negative because mention has no word embedding
--    fcand = 0,  -- false negative because no candidates found for mention
--    fenID = 0,  -- false negative because true entity is not in candidates
--    unkmen = 0  -- number of times a mention was unknown (and unkmen embedding was used)
--  }
  
  local unkTok = dict:getId('<unkmen>')
  
  local docnames = {}
  local documents = {}
  local data_t = {}
  local candidateTable = {}
  
  
--  local testdocnames = {}
--  local testdocuments = {}
--  local testdata_t = {}
--  local testcandidateTable = {}
  
  local nMent = 100000    -- mentions per chunk
  local ndoc = 0
  local m = 0 -- mention counter
  local dc = 1
  
  local inc = 10000
  local mpr = 0
  local linec = 0
  local words
  
  for line in io.lines(fname) do
    linec = linec + 1
    
----------- document end ---------------
    if utf8.sub(line, 1, 6) == "</doc>" then 
      -- docid = nil
      for i = 1, math.ceil(params.memsize/2) do -- fill end with padding
        c = c + 1
        words[c] = dict:addToVocab('<pad>')
      end
      documents[ndoc] = torch.Tensor(words)
      words = {}
      c = 0
      
      if m > mpr then
        print(m)
        print(linec)
        mpr = m + inc
      end
      
      if m >= nMent then
        docnames[dc] = docdir .. 'chunk' .. tostring(dc) .. '.t7'
        local datafile = {}
        local candidateTensor = mergeTable(candidateTable)
        datafile.data = torch.cat(torch.Tensor(data_t), candidateTensor, 2)
        datafile.docs = documents
        torch.save(docnames[dc], datafile)
        print('writing ' .. docnames[dc])
        dc = dc +1
        documents = {}
        data_t = {}
        ndoc = 0
        m = 0
        mpr = 0
        candidateTable = {}
        collectgarbage()
      end
      
-------------- new document  ---------------
    elseif utf8.sub(line, 1, 8) == "<doc id=" then 
      -- docid = utf8.match(line, "<doc%sid=\"(%d*)")
      ndoc = ndoc +1
      words = {}
      c = 0
      for i = 1, math.ceil(params.memsize/2) do -- fill start with padding
        c = c + 1
        words[c] = dict:addToVocab('<pad>')
      end
      
-------------- line in document  ---------------
    else
      local endpos = 1
      for headstring, wik, men, e in utf8.gmatch(line, "(.-)<a%shref=\"(.-)\">(.-)</a>()") do
        stats.total = stats.total + 1
        --print(men)
        endpos = e
        -- process the words before the mention
        local wh = tokenize(headstring)
        for i = 1, #wh do   
          if stringx.endswith(wh[i], '.') and not dict:addToVocab(wh[i], 0) then    -- check if period is eos or part of abreviation
            c = c + 1
            words[c] = dict:addToVocab(wh[i]:gsub("%.$", ""), contextUNK)   -- add word but without period at the end
            c = c + 1
            words[c] = dict:addToVocab('<eos>')
          else
            c = c + 1
            words[c] = dict:addToVocab(wh[i], contextUNK)
          end
          --print(wh[i])
        end
        
        -- save offset for later
        local d = c
        -- process the mention
        local menID = dict:addToVocab(men, mentionUNK)
        
        if menID == unkTok then
          stats.unkmen = stats.unkmen + 1
        end
        if not menID then
          stats.fmen = stats.fmen + 1
        end
        
        local cand, enIdx = envecutils:getCandidates(men, wik)
        

        
        local k = 0
        local wm = tokenize(men)    -- I assume no sentence ends inside mention tag
        for i = 1, #wm do
          k = k + 1
          c = c + 1
          words[c] = dict:addToVocab(wm[i], contextUNK)
          --print(wm[i])
        end
        
        
        if not cand then
          stats.fcand = stats.fcand + 1
        end
          
        
        
        -- mention and wiki as vocab index
        -- doc_idx is index the document can be found at in documents
        -- d is the position of the mention in the document
        -- k is the number of words in the mention
        if cand and menID then
          if enIdx and enIdx <= params.ncand then
              m = m +1
              local nc = math.min(cand:size()[1], params.ncand)
              local candidates = torch.ones(params.ncand)       -- one is the "no candidate" index which is allways wrong, can in training replace with random if desired
              candidates[{{1,nc}}] = cand[{{1,nc}}]

              candidateTable[m] = candidates

              data_t[m] = {menID, enIdx, ndoc, d, k}
          else
            local enID = envecutils:getIDfromTitle(wik)
            if enID then
              stats.fenTop = stats.fenTop + 1
              if addtoCand then
                enIdx = params.ncand
                m = m +1
                local nc = math.min(cand:size()[1], params.ncand)
                local candidates = torch.ones(params.ncand)       -- one is the "no candidate" index which is allways wrong, can in training replace with random if desired
                candidates[{{1,nc}}] = cand[{{1,nc}}]

                candidates[enIdx] = enID    -- add true entity at the end
                candidateTable[m] = candidates

                data_t[m] = {menID, enIdx, ndoc, d, k}
              else
                stats.fNeg = stats.fNeg + 1
              end
            else
              stats.fenID = stats.fenID + 1
              stats.fNeg = stats.fNeg + 1
            end
          end
        else
          stats.fNeg = stats.fNeg + 1
        end
      end
      
      -- add the string at the end
      local wt = tokenize(utf8.sub(line, endpos))
      for i = 1, #wt do   
        if stringx.endswith(wt[i], '.') and not dict:addToVocab(wt[i], 0) then    -- check if period is eos or part of abreviation
          c = c + 1
          words[c] = dict:addToVocab(wt[i]:gsub("%.$", ""), contextUNK)   -- add word but without period at the end
          c = c + 1
          words[c] = dict:addToVocab('<eos>')
        else
          c = c + 1
          words[c] = dict:addToVocab(wt[i], contextUNK)
        end
        --print(wt[i])
      end

      
      c = c + 1
      words[c] = dict:addToVocab('<eol>')
    end
  end
  stats.pos = stats.total - stats.fNeg
  stats.rNegPos = stats.fNeg/stats.pos
  docnames[dc] = docdir .. 'chunk' .. tostring(dc) .. '.t7'
  local datafile = {}
  local candidateTensor = mergeTable(candidateTable)
  datafile.data = torch.cat(torch.Tensor(data_t), candidateTensor, 2)
  datafile.docs = documents
  torch.save(docnames[dc], datafile)
  print('writing ' .. docnames[dc])
  print(stats)
  
	local out = {}
  out.docnames = docnames
  out.stats = stats
  
  return out
end

-------------------------- AIDA LOADER --------------------------



function g_read_AIDA(fname, dict, envecutils, params, savename)
  print ("loading " .. fname)
  local docdir = params.dictDir .. 'AIDA/'
  paths.mkdir(docdir)

  local contextUNK = 1    -- 1: if context word is unknown then replace with <unk>
  local mentionUNK = 2    -- 2: if mention word is unknown then replace with <unkmen>


  local stats = {
    total = 0,      -- total examples
    pos = 0,        -- total extracted mention examples
    fNeg = 0,       -- total false negatives
    rNegPos = 0,    -- fNeg/pos = average ration of false negatives to positives, used to calculate f1 over subset of docs
    fmen = 0,       -- #mention has no word embedding
    fcand = 0,      -- #no candidates found for mention
    fenID = 0,      -- #true entity is not in candidates
    fenTop = 0,     -- #correct entity was in candidates but not in top N
    noEn = 0,       -- there was a mention but no ground truth entity was given
    unkmen = 0      -- number of times a mention was unknown (and unkmen embedding was used)
  }
  
  stats.contextUNK = contextUNK
  stats.mentionUNK = mentionUNK
  stats.addtoCand  = false
  stats.ncand = params.ncand


  

  local docnames = {}
  local documents = {}
  local data_t = {}
  local candidateTable = {}

  local unkTok = dict:getId('<unkmen>')


--  local testdocnames = {}
--  local testdocuments = {}
--  local testdata_t = {}
--  local testcandidateTable = {}


  local ndoc = 0
  local m = 0 -- mention counter



--  

--  local inc = 10000
--  local mpr = 0

--  local nwords = 0
--  for line in io.lines(fname) do
--    local w = stringx.split(line, '\t')
--    if w[1]:gmatch("[^,:;\\><{}%[%]()|\"%s]+") then
--      nwords = nwords + 1
--    end
--  end

  local dc = 1
  local linec = 0


  local B = false
  local words
  local firstDoc = true
  local mention

  for line in io.lines(fname) do
    linec = linec + 1
    
    -------------- CLOSE CURRENT DOCUMENT, START NEW DOCUMENT -------------
    if utf8.sub(line, 1, 10) == "-DOCSTART-" then 
      if not firstDoc then
        for i = 1, math.ceil(params.memsize/2) do -- fill end with padding
          c = c + 1
          words[c] = dict:addToVocab('<pad>')
        end
        documents[ndoc] = torch.Tensor(words)
      end
      firstDoc = false
      ndoc = ndoc +1
      words = {}
      c = 0
      for i = 1, math.ceil(params.memsize/2) do -- fill start with padding
        c = c + 1
        words[c] = dict:addToVocab('<pad>')
      end
      
      
    else
      ---------------- INTERPRET CONTENTS OF LINE -----------------
      local w = stringx.split(line, '\t')
      if #w > 0 then 
        if w[1]:match("[^,:;\\><{}%[%]()|\"%s]+") then  -- check that line is not made up of just these characters
          c = c + 1
          if w[1]:match("^[.?!]$") then
            words[c] = dict:addToVocab('<eos>')
          else
            words[c] = dict:addToVocab(w[1])
          end
        end
        
        if #w > 1 and w[2] == 'B' then    -- line contains new mention
          if #w >= 6 then   -- check if wikiID supplied
            stats.total = stats.total + 1
            local men = w[3]
            local mentionLenght = #stringx.split(w[3])
            local wikiID = tonumber(w[6])
            
            -- process the mention
            local menID = dict:addToVocab(men, mentionUNK)
            
            if menID == unkTok then
              stats.unkmen = stats.unkmen + 1
            end
            if not menID then
              stats.fmen = stats.fmen + 1
            end
            
            local cand, enIdx = envecutils:getCandidatesWikiID(men, wikiID)
            if not cand then
              stats.fcand = stats.fcand + 1
            end


            -- mention and wiki as vocab index
            -- doc_idx is index the document can be found at in documents
            -- d is the position of the mention in the document
            -- k is the number of words in the mention
            if cand and menID then
              if enIdx and enIdx <= params.ncand then
                m = m +1
                local nc = math.min(cand:size()[1], params.ncand)
                local candidates = torch.ones(params.ncand)       -- one is the "no candidate" index which is allways wrong, can in training replace with random if desired
                candidates[{{1,nc}}] = cand[{{1,nc}}]

                candidateTable[m] = candidates

                data_t[m] = {menID, enIdx, ndoc, c, mentionLenght}
              else
                local enID = envecutils:getIDfromWikiID(wikiID)
                if enID then
                  stats.fenTop = stats.fenTop + 1
                  stats.fNeg = stats.fNeg + 1
                else
                  stats.fenID = stats.fenID + 1
                  stats.fNeg = stats.fNeg + 1
                end
              end
            else
              stats.fNeg = stats.fNeg + 1
            end
          else
            stats.noEn = stats.noEn + 1
          end
        end
      end
    end
  end

  -- finish off last document
  for i = 1, math.ceil(params.memsize/2) do
    c = c + 1
    words[c] = dict:addToVocab('<pad>')
  end
  documents[ndoc] = torch.Tensor(words)

  stats.pos = stats.total - stats.fNeg
  stats.rNegPos = stats.fNeg/stats.pos
  docnames[1] = docdir .. savename .. '.t7'
  local datafile = {}
  local candidateTensor = mergeTable(candidateTable)
  datafile.data = torch.cat(torch.Tensor(data_t), candidateTensor, 2)
  datafile.docs = documents
  torch.save(docnames[1], datafile)
  print('writing ' .. docnames[1])
  print(stats)
  
	local out = {}
  out.docnames = docnames
  out.stats = stats
  
  return out
    
end
  
      
        
    
    