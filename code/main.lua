-- Copyright (c) 2015-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree. An additional grant 
-- of patent rights can be found in the PATENTS file in the same directory.

-- require('mobdebug').coro()

torch.setdefaulttensortype('torch.FloatTensor')

require('xlua')
require('paths')
signal = require "posix.signal"
local optim = require 'optim'
local tds = require('tds')
local csvigo = require 'csvigo'
paths.dofile('data.lua')
paths.dofile('model.lua')
paths.dofile('utilities.lua')



signal.signal(signal.SIGUSR1, function() 
      print("SIGUSR1 recieved, terminating at the end of this chunk")
      g_terminate = true
    end
  )

function g_print_csoft(sentence, mention, savename)
  local ctab = {}
  ctab[1] = mention
  ctab[2] = sentence
  for i = 1,#g_embWeights.csoft do
    ctab[i+2] = {}
    for j = 1, g_embWeights.csoft[i][1]:nElement() do
      ctab[i+2][j] = g_embWeights.csoft[i][1][j]
    end
  end
  csvigo.save{data=ctab, path=savename}
end

local function printCand(candp)
  for i = 1,#candp[1] do
    print(candp[1][i] .. '\t\t' .. candp[2][i])
  end
end



function g_elink(sentence, mentionIdx, mentionLength, prName)
  local prPath
  if prName == '' then
    prName = g_params.save
  end
  if prName == nil then
    prPath = false
  else
    prPath = '../analysis/soft/' .. prName .. '.csv'
  end
  
  local validInput = true
  local tmpwords = g_tokenize(sentence)
  local words = {}
  for w = 1,#tmpwords do
    if stringx.endswith(tmpwords[w], '.') and not g_dict:addToVocab(tmpwords[w], 0) then    -- check if period is eos or part of abreviation
      words[#words+1] = tmpwords[w]:gsub("%.$", "")
      words[#words+1] = '<eos>'
    else
      words[#words+1] = tmpwords[w]
    end
  end
  
  
  local context = torch.Tensor(1, g_params.memsize):fill(g_dict:getId('<pad>'))
  
  local contextTabFull = {}
  for i = 1, g_params.memsize do
    contextTabFull[i] = '<pad>'
  end
  
  local mem = math.ceil(g_params.memsize/2)
  
  local contextTab = {}
  local headLength = math.min(mem, mentionIdx - 1)    -- #words before mention
  local docstart = mentionIdx - headLength
  
  local tailbegin = mentionIdx + mentionLength
  local nwords = #words
  
  local tailLength = math.min(mem, nwords - tailbegin + 1)
  local docend = tailbegin + tailLength -1
  
  -- copy the context before the mention
  for i = 1, headLength do
    context[{ 1, { mem - headLength + i} }] = 
      g_dict:getId(words[docstart + i -1])
    contextTab[#contextTab+1] = words[docstart + i -1]
    contextTabFull[mem - headLength + i] = words[docstart + i -1]
  end
  
  -- copy the context after the mention
  
  for i = 1, tailLength do
    context[{ 1, { mem + i} }] = 
      g_dict:getId(words[tailbegin + i -1])
    contextTab[#contextTab+1] = words[tailbegin + i -1]
    contextTabFull[mem + i] = words[tailbegin + i -1]
  end
  
--  if mem < mentionIdx then
--    local hstart = mentionIdx - mem
--    for i = 1, mem do
--      context[1][i] = g_dict:getId(words[i + hstart])
--      contextTab[#contextTab+1] = words[i + hstart]
--    end
--  else
--    local hstart = mem - mentionIdx
--    for i = 1, hlength do
--      context[1][hstart +i] = g_dict:getId(words[i])
--      contextTab[#contextTab+1] = words[i]
--    end
--  end
  
--  local tstart = mentionLength + mentionIdx
--  local tlength = math.min(mem, #words - tstart)  -- #words after mention
  
--  for i = 1, tlength do
--    context[1][mem +i] = g_dict:getId(words[tstart + i])
--    contextTab[#contextTab+1] = words[tstart + i]
--  end
  
  local mention = {}
  for i = 1, mentionLength do
    mention[i] = words[mentionIdx + i - 1]
  end
  
  local mentionString = table.concat(mention, " ")
  local contextString = table.concat(contextTab, " ")
  --print(mentionString)
  local men = uTensor(1)
  local tmp = g_dict:getId(mentionString, 2)
  if not tmp then
    print "no mention Id found"
  end
  men[1] = tmp
  local cand, id = g_envec:getCandidates(mentionString)
  if not cand then
    validInput = false
    print "no candidates found"
  end
  
  if validInput then
    local nc = math.min(cand:size()[1], g_params.ncand)
    local candidates = torch.ones(1, g_params.ncand)       -- one is the "no candidate" index which is allways wrong, can in training replace with random if desired
    candidates[{{},{1,nc}}] = cand[{{1,nc}}]
    
    
    local time = uTensor(1, g_params.memsize)
    for t = 1, g_params.memsize do
      time:select(2, t):fill(t)
    end
    
    
    local x = {men, context, time, candidates}
    
    local tprob = g_model:forward(x)  -- softmax output
    local val, predID = torch.max(tprob, 2)
    
    local pred = candidates[1][predID[1][1]]
    
    local predTitle = g_envec:getTitlefromEnID(pred)
    local predWikiID = g_envec:getWikiIdfromEnID(pred)
    
    local candw = {}
    candw[1] = g_envec:getWords(candidates)
    candw[2] = {}
    
    for i = 1,#candw[1] do
      candw[2][i] = math.exp(tprob[1][i])
    end
    
    
    
    print("------------------------------------------------------------")
    print "Results"
    print("------------------------------------------------------------")
    print("mention:\t" .. mentionString)
    print("context:\t" .. contextString)
    print("------------------------------------------------------------")
    print "entity candidates:\n"
    --g_envec:printWords(candidates)
    printCand(candw)
    print("------------------------------------------------------------")
    print "predicted entity:\n"
    print("Title:\t" .. predTitle)
    print("WikiID:\t" .. predWikiID)
    print("------------------------------------------------------------")
    
    if prPath then
      g_print_csoft(contextTabFull, mention, prPath)
    end
    
  end
end

function loadutils()
  if not g_dict then
    if g_params.dict == 'w2v' then
      g_dict = require('w2vutils')
    elseif g_params.dict == 'glove' then
      g_dict = require('gloveutils')
    else
      print "unknownd dictionary type"
    end
  end
  if not g_envec then
    g_envec = require('envecutils')
  end
end

function g_loadandprep()
  loadutils()
  g_envec:loadEnProb()
end






local function run(epochs)
  local startEpoch = g_state.epoch + 1
  for i = startEpoch, epochs do
    print('epoch ' .. i)
    
    local anneal = (i >= g_params.annealep and g_params.annealep ~= 0)
    g_train(g_data.wiki, anneal)
    
    
    if g_terminate then break end
    
    if g_params.dt < 1e-5 then break end
  end
end

function g_continue()
  g_terminate = false
  run(g_params.epochs)
end


function g_save(name)
    paths.mkdir(g_params.modeldir)
    local path = g_params.modeldir .. name .. '.mdl'
    local d = {}
    d.params = g_params
    d.paramx = g_paramx:float()
    d.optimState = g_optimState
    d.log = g_log
    d.state = g_state
    torch.save(path, d, binary, false)
    print("saved model to " .. path)
end



--------------------------------------------------------------------
--------------------------------------------------------------------
-- model params:
local cmd = torch.CmdLine()
cmd:option('--rgpu', false, 'run on GPU')
cmd:option('--gpu', 1, 'GPU id to use')
cmd:option('--nthreads', 1, 'set number of threads')
cmd:option('--edim', 50, 'context embedding dimension')
cmd:option('--entdim', 50, 'entity embedding dimensions')
cmd:option('--cidim', 0, 'context internal dimension default same as edim')
--cmd:option('--eidim', 50, 'entity internal dimension')
cmd:option('--lindim', 30, 'linear part of the state')
cmd:option('--memsize', 100, 'memory size')
cmd:option('--nhop', 3, 'number of hops')
cmd:option('--ncand', 20, 'number of candidate entities to consider')
cmd:option('--distance', 'dot', 'distance measure to use for getting closest candidate entity')
cmd:option('--time', 'joi', 'time embedding type joi or sep ')
cmd:option('--init_std', 0.05, 'weight initialization std')
cmd:option('--init_hid', 0.1, 'initial internal state value')
cmd:option('--sdt', 0.01, 'initial learning rate')
cmd:option('--anneal', 0.9, 'learning rate annealing factor')
cmd:option('--annealep', 2, 'epoch at which to start annealing, never if 0')
cmd:option('--annealbest', false, 'reduce learn rate if better than best istead of better than previous')
cmd:option('--maxgradnorm', 50, 'maximum gradient norm')
cmd:option('--critavg', false, 'average loss over minibatch')
cmd:option('--batchsize', 1024)
cmd:option('--optim', '', 'optim methode to use')
cmd:option('--noise', 0.1, 'randome noise to inject at training')
cmd:option('--unkmen', 0.01, 'unknown mention tokens to inject at training')
cmd:option('--logIncrement', 100, 'steps to take between logs')
cmd:option('--show', true, 'print progress')
cmd:option('--load', '', 'model file to load')
cmd:option('--addEpochState', 1, 'if saved model does not have state.epoch then this value will be used')
cmd:option('--save', '', 'modelname')
cmd:option('--modeldir', 'models/', 'path to save model')
cmd:option('--initsave', '', 'path to save model after initialization')
cmd:option('--initrand', false, 'initialize with random instead of w2v')
cmd:option('--epochs', 1)
cmd:option('--sgdSeq', false, 'select random training examples or select them sequentially')
cmd:option('--test', false, 'enable testing')
cmd:option('--small', false, 'use small dataset for code testing')
cmd:option('--data', 'data/', 'location of the data directory')
cmd:option('--dict', 'glove', 'use glove or w2v as embedding vector dictionary')
cmd:option('--reload', false, 'reparse the dataset')
g_params = cmd:parse(arg or {})

if g_params.cidim == 0 then
  g_params.cidim = g_params.edim
end


g_state = {}
g_state.epoch = 0
g_state.wikichunk = 0
g_state.bestf1 = 0
g_state.counter = 0
g_state.truePos = 0


g_optimState  = nil       -- stores a lua table with the optimization algorithm's settings, and state during iterations
g_optimMethod = nil      -- stores a function corresponding to the optimization routine

-- remember, the defaults below are not necessarily good
if g_params.optim == 'lbfgs' then
  g_optimState = {
    learningRate = g_params.sdt,
    maxIter = 2,
    nCorrection = 10
  }
  g_optimMethod = optim.lbfgs
elseif g_params.optim == 'sgd' then
  g_optimState = {
    learningRate = g_params.sdt,
    weightDecay = 0,
    momentum = 0,
    learningRateDecay = 1e-7
  }
  g_optimMethod = optim.sgd
elseif g_params.optim == 'adagrad' then
  g_optimState = {
    learningRate = g_params.sdt,
  }
  g_optimMethod = optim.adagrad
elseif g_params.optim == 'adadelta' then
  g_optimState = {
    eps = 1e-6,
    roh = 0.95,
  }
  g_optimMethod = optim.adadelta
else
  print('not using optim')
end


g_log = {}
g_log.wiki = {}
g_log.ace = {}
g_log.msnbc = {}
g_log.aquaint = {}
g_log.aidaA = {}
g_log.aidaB = {}



print(g_params)

if g_params.load ~= '' then
  g_ld = torch.load(g_params.load, binary, false)
  print('loading params')
  print(g_ld.params)
  
  g_compatibility(g_ld, g_params.addEpochState)
  
  g_params.edim = g_ld.params.edim
  g_params.nhop = g_ld.params.nhop
  g_params.entdim = g_ld.params.entdim
  g_params.lindim = g_ld.params.lindim
  g_params.cidim = g_ld.params.cidim
  g_params.memsize = g_ld.params.memsize
  g_params.ncand = g_ld.params.ncand
  g_params.time = g_ld.params.time
  g_params.optim = g_ld.params.optim
  g_params.dt = g_ld.params.dt
  g_params.critavg = g_ld.params.critavg
  g_params.sgdSeq = g_ld.params.sgdSeq
  g_params.noise = g_ld.params.noise
  g_params.unkmen = g_ld.params.unkmen
  g_params.logIncrement = g_ld.params.logIncrement
  g_params.dict = g_ld.params.dict
  g_params.dictDir = g_ld.params.dictDir
  

  
  
  g_state = g_ld.state
  

  if g_params.optim == 'lbfgs' then
    g_optimMethod = optim.lbfgs
  elseif g_params.optim == 'sgd' then
    g_optimMethod = optim.sgd
  elseif g_params.optim == 'adagrad' then
    g_optimMethod = optim.adagrad
  elseif g_params.optim == 'adadelta' then
    g_optimMethod = optim.adadelta
  end

  g_optimState = g_ld.optimState or g_optimState
  
  g_log = g_ld.log

  g_params.loaded = true

  print('new params')
  print(g_params)
end


print(g_state)

torch.setnumthreads(g_params.nthreads)


paths.dofile('train.lua')


rgpu = g_params.rgpu

uTensor = torch.Tensor
if rgpu then
	require 'cunn'
  cutorch.setDevice(g_params.gpu)
	uTensor = torch.CudaTensor
end


-- initialize random number generator according to recommendation in doc
math.randomseed( os.time() )
for i = 1,10 do
	math.random()
end



if not g_params.loaded then
  if g_params.dict == 'w2v' then
    if g_params.ncand == 20 then
      g_params.dictDir = g_params.data .. 'datasets_w2v/'
    else
      g_params.dictDir = g_params.data .. 'datasets_w2v_' .. tostring(g_params.ncand) .. '/'
    end

  elseif g_params.dict == 'glove' then
    if g_params.ncand == 20 then
      g_params.dictDir = g_params.data .. 'datasets_glove/'
    else
      g_params.dictDir = g_params.data .. 'datasets_glove_' .. tostring(g_params.ncand) .. '/'
    end
  else
    print "unknown dictionary type"
  end
end







g_files = {


  wiki = 'data/textWithAnchorsFromAllWikipedia2014Feb.txt',


  ace = {
    dir = 'data/deos14_ualberta_experiments/ace2004/',
    name = 'ace2004'
  },
  
  msnbc = {
    dir = 'data/deos14_ualberta_experiments/msnbc/',
    name = 'msnbc'},
  
  aquaint = {
    dir = 'data/deos14_ualberta_experiments/aquaint/',
    name = 'aquaint'
  },
  
  aidaA = 'data/AIDA_A.txt',
  
  aidaB = 'data/AIDA_B.txt'
  
}

if g_params.small then
  g_files.wiki = 'data/small-textWithAnchorsFromAllWikipedia2014Feb.txt'
end


g_data_Files = {
  saveFile = g_params.dictDir .. 'datasets.t7',
  context = g_params.dictDir .. 'contextEmbedding.t7',
  target = g_params.dictDir .. 'targetEmbedding.t7'
}




 g_envec = nil
 g_dict = nil
 


g_data = {}
if g_params.reload or (not paths.filep(g_data_Files.saveFile)) then
  paths.mkdir(g_params.dictDir)
  loadutils()
  
  g_envec:loadEnProb()
  
--  g_data.training = g_read_ualberta(g_files.train, g_dict, g_envec, g_params)
  g_data.wiki = g_read_wiki(g_files.wiki, g_dict, g_envec, g_params)
  g_data.ace = g_read_ualberta(g_files.ace, g_dict, g_envec,  g_params)
  g_data.msnbc = g_read_ualberta(g_files.msnbc, g_dict, g_envec,  g_params)
  g_data.aquaint = g_read_ualberta(g_files.aquaint, g_dict, g_envec,  g_params)
  g_data.aidaA = g_read_AIDA(g_files.aidaA, g_dict, g_envec,  g_params, 'aidaA')
  g_data.aidaB = g_read_AIDA(g_files.aidaB, g_dict, g_envec,  g_params, 'aidaB')

  
  g_envec:freeProb()
  
  collectgarbage()
  
  g_data.nent = g_envec:numEnt()    -- number of entities
  g_data.entityDim = g_envec:size()    -- entity embedding dimensions (normally 50)
  
  g_data.nwords = g_dict:vocabSize()
  g_data.contextDim   = g_dict:dimSize()
  
  
  torch.save(g_data_Files.context, g_dict:getVocabTensor(), binary, false)
  torch.save(g_data_Files.target, g_envec:getEnTensor(), binary, false)
  
  
  torch.save(g_data_Files.saveFile, g_data, binary, false)
  print('Writing g_data t7 File for future usage.')
else
  g_data = torch.load(g_data_Files.saveFile, binary, false)
  print('done loading g_data from t7')
end

collectgarbage()

g_params.nent = g_data.nent
g_params.nwords = g_data.nwords

print('number of entities ' .. g_params.nent)
print('vocabulary size ' .. g_params.nwords)

g_model, g_embWeights = g_build_model(g_params)

collectgarbage()



g_paramx, g_paramdx = g_model:getParameters()
g_paramx:normal(0, g_params.init_std)
if g_params.loaded then
    g_paramx:copy(g_ld.paramx)
elseif not g_params.initrand then
    local cdim = math.min(g_params.edim, g_data.contextDim)
    local entdim = math.min(g_params.entdim, g_data.entityDim)
    
    local contextEmb = torch.load(g_data_Files.context, binary, false)
    g_embWeights.context[{{},{1,cdim}}]:copy(contextEmb[{{},{1,cdim}}])
    
    context = nil   -- free up the memory
    collectgarbage()
    
    local enEmb = torch.load(g_data_Files.target, binary, false)
    
    g_embWeights.target[{{},{1,entdim}}]:copy(enEmb[{{},{1,entdim}}])
    
    enEmb = nil
    collectgarbage()
end

if g_params.loaded then
  g_ld.paramx = nil
  g_ld = nil
end

--g_envec:loadInverse()

collectgarbage()


g_params.dt = g_params.sdt

if g_params.initsave ~= '' then
    g_save(g_params.initsave)
end

print('starting to run....')
run(g_params.epochs)

if g_params.save ~= '' then
    g_save(g_params.save)
end
