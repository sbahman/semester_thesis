local stringx = require('pl.stringx')
local tds = require('tds')


glopt = {
	txtfilename = 'data/gloveutils/glove50d.txt',
  mfile       = 'data/gloveutils/M50.t7',
  vocabfile   = 'data/gloveutils/vocab.t7'
}
local gloveutils = {}
if not paths.filep(glopt.mfile) or not paths.filep(glopt.vocabfile) then
	gloveutils = require('glove_reader')
else
	gloveutils = torch.load(glopt.vocabfile, binary, false)
	print('Done reading glove data.')
end




collectgarbage()





-------------------------------------------------------

gloveutils.loadM = function(self)
  local M = torch.load(glopt.mfile, binary, false)
  self.M = M
end

gloveutils.loadAndReturnM = function(self)
  return torch.load(glopt.mfile, binary, false)
end

gloveutils.getVocabTensor = function(self)
  local M = self:loadAndReturnM()
  local vocabTensor
  if #self.comp ~= 0 then
    local compTensor = torch.Tensor(#self.comp, self.vdim):zero()
    for i = 1, #self.comp do
      if self.comp[i] == 0 then
        compTensor[i] = torch.randn(self.vdim)
      else
        for j = 1, #self.comp[i] do
          compTensor[i] = compTensor[i] + M[self.comp[i][j]]
        end
        compTensor[i] = compTensor[i]/#self.comp[i]
      end
    end

    print("b1")
    vocabTensor = torch.cat({M, compTensor}, 1)
  else
    print("b2")
    vocabTensor = M
  end
  
  
  self.Oword2id = self.word2id
  
  local glo = {}
  glo.word2id = self.word2id
  glo.id2word = self.id2word
  glo.Oword2id = self.Oword2id
  glo.vdim = self.vdim
  glo.redir = self.redir
  glo.comp = tds.hash()

  torch.save(glopt.vocabfile,glo, binary, false)
  print('Writing glove expanded vocab t7 File for future usage.')

  torch.save(glopt.mfile, vocabTensor, binary, false)
  print('Writing expanded glove vectors t7 File for future usage.')
  
  collectgarbage()
  
  return vocabTensor
end

gloveutils.getWordFromID = function(self, ID)
  return self.id2word[ID]
end

gloveutils.dimSize = function(self)
  return self.vdim
end

gloveutils.vocabSize = function(self)
  return #self.id2word
end

gloveutils.printWord = function(self, wordID)
  print(self.id2word[wordID])
end

gloveutils.printWords = function(self, wordTensor)
  wordTensor:apply(function(x)
      print(self.id2word[x])
    end)
end

gloveutils.getId = function(self, word, unkMode)    -- if unkMode == 1 then return <unk>; 2: return <unkMention>; 0: (any other number) return nil
  local unk = unkMode or 1
  
  word = word:lower()
  
  local idx = self.word2id[word]
  if not idx then
    local red = self.redir[word]    -- check if word has redirect
    if red then
      idx = self.word2id[red]
    else
      idx, red = self:getCat(word)   -- try concat word
      if idx then
        self.redir[word] = red
      else
        local comp = nil
        comp, red = self:getComp(word)      -- try to get compound word
        if comp then
          self.redir[word] = red
          idx = self.word2id[red]
        elseif unk == 1 then
          idx = self.word2id['<unk>']
        elseif unk == 2 then
          idx = self.word2id['<unkmen>']
        else
          idx = nil
        end
      end
    end
  end
  return idx
end

        

gloveutils.addToVocab = function(self, word, unkMode)    -- if unkMode == 1 then return <unk>;   2: return <unkMention>;  3: add random tensor to vocab and return index;0: (or any other number) return nil
  local unk = unkMode or 1
  
  word = word:lower()
  
  local idx = self.word2id[word]
  if not idx then
    local red = self.redir[word]    -- check if word has redirect
    if red then
      idx = self.word2id[red]
    else
      idx, red = self:getCat(word)   -- try concat word
      if idx then
        self.redir[word] = red
      else
        local comp = nil
        comp, red = self:getComp(word)      -- try to get compound word
        if comp then
          self.redir[word] = red
          idx = self.word2id[red]    -- check if redirect word already in vocab
          if not idx then
            idx = #self.id2word+1
            self.word2id[red] = #self.id2word+1
            self.id2word[#self.id2word+1] = red
            self.comp[#self.comp +1] = comp
          end
        elseif unk == 1 then
          idx = self.word2id['<unk>']
        elseif unk == 2 then
          idx = self.word2id['<unkmen>']
        elseif unk == 3 then
          idx = #self.id2word+1
          self.word2id[word] = #self.id2word+1
          self.id2word[#self.id2word+1] = word
          self.comp[#self.comp +1] = 0    -- when building tensor make a random vector
        else
          idx = nil
        end
      end
    end
  end
  return idx
end

gloveutils.getCat = function (self, word)   -- try to get wordvec by concatenating the inputstring (try both caps and lowecase)
  local concat = word:gsub("%s", "_")
  local ind = self.word2id[concat]

  return ind, concat
end

gloveutils.getComp = function(self, word)
  local words = stringx.split(word)
  local c = 0
  local comp = tds.Vec()     -- indexes of compound words
  local r = ''  -- build up compound word
  local f = false
  for k = 1, #words do
    local w = words[k]
    local i = self.Oword2id[w]
    
    if not i then
      local red = self.redir[w]
      if red then
        i = self.Oword2id[red]
      end
    end
    
    if i ~= nil then
      if f then 
        r = r .. "_"
      end
      f = true
      r = r .. w
      comp[#comp+1] = i
      c = c + 1
    end
  end
  if c == 0 then
    return nil, nil
  else
    return comp, r
  end
end

------------------------- uppercase versions of functions --------------

--gloveutils.addToVocabCaps = function(self, word, retUNK)    -- if retUNK it will return an unk token if an unknown word is found, else nil
--  local unk = true
--  if retUNK ~= nil then
--    unk = retUNK
--  end
--  local idx = self.word2id[word]
--  local red = nil
--  if not idx then
--    red = self.redir[word]    -- check if word has redirect
--    if red then
--      idx = self.word2id[red]
--    else
--      idx = self.word2id[word:lower()]
--      if idx then
--        self.redir[word] = word:lower()
--      else
--        idx, red = self:getCatCaps(word)   -- try concat word
--        if idx then
--          self.redir[word] = red
--        else
--          local comp = nil
--          comp, red = self:getCompCaps(word)      -- try to get compound word
--          if comp then
--            self.redir[word] = red
--            idx = self.word2id[red]    -- check if redirect word already in vocab
--            if not idx then
--              idx = #self.id2word+1
--              self.word2id[red] = #self.id2word+1
--              self.id2word[#self.id2word+1] = red
--              self.comp[#self.comp +1] = comp
--            end
--          elseif unk then
--            idx = self.word2id['<unk>']
--          else
--            idx = nil
--          end
--        end
--      end
--    end
--  end
--  return idx
--end
            


--gloveutils.getCatCaps = function (self, word)   -- try to get wordvec by concatenating the inputstring (try both caps and lowecase)
--  local concat = word:gsub("%s", "_")
--  local ind = self.word2id[concat]
--  local redirect = nil
--  if ind == nil then
--    ind = self.word2id[concat:lower()]
--    if ind then
--      redirect = concat:lower()
--    end
--  else
--    redirect = concat
--  end

--  return ind, redirect
--end

--gloveutils.getCompCaps = function(self, word)
--  local words = stringx.split(word)
--  local c = 0
--  comp = tds.Vec()     -- indexes of compound words
--  local r = ''  -- build up compound word
--  local f = false
--  for k = 1, #words do
--    local w = words[k]
--    local wr = w
--    --try the word then try it all lower, build up redirect word r accordingly
--    local i = self.Oword2id[w]
    
--    if i == nil then
--      i = self.Oword2id[w:lower()]
--      wr = w:lower()
--    end
    
--    if i ~= nil then
--      if f then 
--        r = r .. "_"
--      end
--      f = true
--      r = r .. wr
--      comp[#comp+1] = i
--      c = c + 1
--    end
--  end
--  if c == 0 then
--    return nil, nil
--  else
--    return comp, r
--  end
--end

--gloveutils.word2vec = function (self,word)
--  -- unused
--  local concat = word:gsub("%s", "_")
--  local ind = self.word2id[concat]
--  local vec = nil
--  local redirect = nil
--  if ind == nil then
--    ind = self.word2id[concat:lower()]
--    if ind == nil then
--      ind = self.w2vvocabl[concat:lower()]
--      if ind == nil then
--        local words = stringx.split(word)
--        local c = 0
--        local v = torch.FloatTensor(300):zero()
--        local r = ''
--        local f = false
--        for k = 1, #words do
--          w = words[k]
--          --try the word then try it all lower, build up redirect word r accordingly
--          i = self.word2id[w]
--          if i == nil then
--            i = self.word2id[w:lower()]
--            if i == nil then
--              i = self.w2vvocabl[w:lower()]
--              if i ~= nil then 
--                if f then 
--                  r = r .. "_"
--                end
--                f = true
--                r = r .. w:lower()
--              end
--            else
--              if f then 
--                r = r .. "_"
--              end
--              f = true
--              r = r .. w:lower()
--            end
--          else
--            if f then
--              r = r .. "_"
--            end
--            f = true
--            r = r .. w
--          end
--          if i ~= nil then
--            v = v + self.M[i]
--            c = c + 1
--          end
--        end
--        if c ~= 0 then
--          vec = v/c
--          redirect = r
--        end
--      else
--        vec = self.M[ind]
--        redirect = concat:lower()
--      end
--    else
--      vec = self.M[ind]
--      redirect = concat:lower()
--    end
--  else
--    vec = self.M[ind]
--  end

--  return vec, redirect
--end

gloveutils.distance = function (self,vec,k)
	local k = k or 1	
	--self.zeros = self.zeros or torch.zeros(self.M:size(1));
	local norm = vec:norm(2)
	vec:div(norm)
	local distances = torch.mv(self.M ,vec)
	distances , oldindex = torch.sort(distances,1,true)
	local returnwords = {}
	local returndistances = {}
	for i = 1,k do
		table.insert(returnwords, gloveutils.id2word[oldindex[i]])
		table.insert(returndistances, distances[i])
	end
	return {returndistances, returnwords}
end

gloveutils.word2vecSimple = function (self,word,throwerror)
   local throwerror = throwerror or false
   local ind = self.word2id[word]
   if throwerror then
		assert(ind ~= nil, 'Word does not exist in the dictionary!')
   end
	--if ind == nil then
		--ind = self.word2id['UNK']
	--end
    if ind == nil then
      return nil
    else
      return self.M[ind]
    end
end

gloveutils.freev2w = function(self)
  self.id2word = nil
end

gloveutils.freeMemory = function(self)
  self.M = nil
  self.word2id = nil
  self.id2word = nil
  self.redir = nil
  self.comp = nil
  self.Oword2id = nil
  collectgarbage()
end

return gloveutils