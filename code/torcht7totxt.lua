local cmd = torch.CmdLine()
cmd:option('--load', '', 'model name')
g_params = cmd:parse(arg or {})


local d = torch.load(g_params.load, binary, false)

local savename = g_params.load .. '.txt'

torch.save(savename, d, ascii, false)