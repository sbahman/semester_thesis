-- Copyright (c) 2015-present, Facebook, Inc.
-- All rights reserved.
--
-- This source code is licensed under the BSD-style license found in the
-- LICENSE file in the root directory of this source tree. An additional grant 
-- of patent rights can be found in the PATENTS file in the same directory.

-- require('mobdebug').coro()
require('xlua')
require('paths')
local tds = require('tds')







--------------------------------------------------------------------
--------------------------------------------------------------------
-- model params:
local cmd = torch.CmdLine()
cmd:option('--rgpu', false, 'run on GPU')
cmd:option('--gpu', 1, 'GPU id to use')
cmd:option('--edim', 300, 'internal state dimension')
cmd:option('--lindim', 75, 'linear part of the state')
cmd:option('--init_std', 0.05, 'weight initialization std')
cmd:option('--init_hid', 0.1, 'initial internal state value')
cmd:option('--sdt', 0.01, 'initial learning rate')
cmd:option('--maxgradnorm', 50, 'maximum gradient norm')
cmd:option('--memsize', 100, 'memory size')
cmd:option('--nhop', 6, 'number of hops')
cmd:option('--ncand', 20, 'number of candidate entities to consider')
cmd:option('--batchsize', 128)
cmd:option('--show', false, 'print progress')
cmd:option('--load', '', 'model file to load')
cmd:option('--save', '', 'path to save model')
cmd:option('--initsave', '', 'path to save model after initialization with w2v')
cmd:option('--epochs', 100)
cmd:option('--test', false, 'enable testing')
cmd:option('--small', false, 'use small dataset for code testing (not mapped)')
cmd:option('--data', 'data/', 'location of the data directory')
g_params = cmd:parse(arg or {})

print(g_params)

rgpu = g_params.rgpu

uTensor = torch.Tensor
if rgpu then
	require 'cunn'
  cutorch.setDevice(g_params.gpu)
	uTensor = torch.CudaTensor
end


-- initialize random number generator according to recommendation in doc
math.randomseed( os.time() )
for i = 1,10 do
	math.random()
end






g_files = {
  train = {
    dir = 'data/deos14_ualberta_experiments/msnbc/',
    name = 'msnbc'},
  

  trainWikiSmall = 'data/small-textWithAnchorsFromAllWikipedia2014Feb.txt',
  trainWiki = 'data/textWithAnchorsFromAllWikipedia2014Feb.txt',

  valid = {
    dir = 'data/deos14_ualberta_experiments/aquaint/',
    name = 'aquaint'
  },
  
  test = {
    dir = 'data/deos14_ualberta_experiments/ace2004/',
    name = 'ace2004'
  },

  --g_training = g_read_wiki(g_train_file, 
  --		g_vocab, g_ivocab, g_documents, g_params)
}


g_data = {}
g_envec = require('envecutils')  
g_w2v = require('w2vutils')

g_envec:loadEnProb()


g_data.nent = g_envec:numEnt()    -- number of entities
g_data.entdim = g_envec:size()    -- entity embedding dimensions (normally 50)

g_data.docnames = {}
-- g_data.training = g_read_ualberta(g_files.train, dict, envec, g_data.documents, g_params)

--g_data.training = g_read_wiki_store(g_files.train, dict, envec, g_data.documents, g_params)


--g_data.nwords = dict:vocabSize()


--g_data.wcontext = dict:getVocabTensor()
--g_data.wtarget = envec:getEnTensor()


--g_params.nent = g_data.nent
--g_params.entdim = g_data.entdim


--print('number of documents ' .. #g_data.documents)

--g_params.nwords = g_data.nwords
--print('vocabulary size ' .. g_params.nwords)





