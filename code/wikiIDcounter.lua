require('paths')
local stringx = require('pl.stringx')
local tds = require('tds')
local file = require('pl.file')



local fnames = {
  envec = 'data/envecutils/gloveEntityVectors_freq3_lr0.001_ep160_sgd_PMI_withZ_withBias.txt',
  emprob = 'data/envecutils/mek-top-freq-crosswikis-plus-wikipedia-lowercase-top64.txt',
  enwikititles = 'data/envecutils/enwiki-titles.txt',
  enwikiredir = 'data/envecutils/enwiki-redirect-normalized.txt',
  
  saveenvectors = 'data/envecutils/envectors.t7',
  saveprob = 'data/envecutils/prob.t7',
  savefile = 'data/envecutils/envec.t7',
}

local function load_wikiTitles(fnames)
  print('loading wiki titles')
  local wtitles = tds.hash()
  wtitles['<candpad>'] = 0
  for line in io.lines(fnames.enwikititles) do
    t = stringx.split(line, '\t')
    wtitles[t[1]] = tonumber(t[2])
  end
  for k,v in pairs(wtitles) do
    if not wtitles[k:lower()] then
      wtitles[k:lower()] = v
      wtitles[k] = nil    -- only keep lower case titles if not both present
    end
  end
  
  local iwtitles = tds.hash()
  for k,v in pairs(wtitles) do
    iwtitles[v] = k
  end
  print('done')
  return wtitles, iwtitles
end

local function load_enprob(fnames)
  print('loading entity probabilities')
  local c = 0
  for line in io.lines(fnames.emprob) do
    local w = stringx.split(line, '\t')
    c = c + #w - 4
  end
  local mvocab = tds.hash()
  local mivocab = tds.hash()
  local mprob = torch.Tensor(c)
  c = 1
  for line in io.lines(fnames.emprob) do
    local w = stringx.split(line, '\t')
    local men = w[1]
    mivocab[#mvocab+1] = men
    mvocab[men] = tds.Vec({c, #w -4})     -- position of candidate entities and number of candidate entities
    for i = 5, #w do
      local a = stringx.split(w[i], ',')
      mprob[c] = a[1]
      c = c + 1
    end
  end 
  print('done')
  return mvocab, mprob
end


wtitles, iwtitles = load_wikiTitles(fnames)

mvocab, mprob = load_enprob(fnames)

wID = tds.hash()

for k,v in pairs(wtitles) do
  wID[v] = 1
end

mID = tds.hash()

for i = 1,mprob:nElement() do
  mID[mprob[i]] = 1
end



cMinW = 0
for k, v in pairs(wID) do
  if mID[k] then
    cMinW = cMinW +1
  end
end


cWinM = 0
for k, v in pairs(mID) do
  if wID[k] then
    cWinM = cWinM +1
  end
end

print("Number of ID's in wtitles: " .. #wID)
print("Number of ID's in prob: " .. #mID)
print("Number of ID's in prob which also occur in wtitles: " .. cMinW)
print("Number of ID's in prob which also occur in wtitles: " .. cWinM)