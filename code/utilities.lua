

function g_compatibility(ld, epoch)    --ld is the loaded file
  epoch = epoch or 0

  
  ld.params.cidim = ld.params.cidim or ld.params.edim
  ld.params.anneal = ld.params.anneal or 0.9
  ld.params.annealep = ld.params.annealep or 2
  ld.params.annealbest = ld.params.annealbest or false
  ld.params.critavg = ld.params.critavg or false
  ld.params.time = ld.params.time or 'joi'
  ld.params.optim = ld.params.optim or ''
  ld.params.logIncrement = ld.params.logIncrement or 100
  ld.params.sgdSeq = ld.params.sgdSeq or false

  
  if ld.params.noise == false or ld.params.noise == nil then
    ld.params.noise = 0
  elseif ld.params.noise == true then
    ld.params.noise = 0.1
  end
  
  if ld.params.unkmen == false or ld.params.unkmen == nil then
    ld.params.unkmen = 0
  elseif ld.params.unkmen == true then
    ld.params.unkmen = 0.001
  end
    
  

  
  
  ld.state = ld.state or {}
  ld.state.epoch = ld.state.epoch or epoch
  ld.state.counter = ld.state.counter or 0
  ld.state.truePos = ld.state.truePos or 0
  ld.state.wikichunk = ld.state.wikichunk or 0
  
  if not ld.state.bestf1 then
    ld.state.bestf1 = 0
    for i = 1,#ld.log.aidaA do
      ld.state.bestf1 = math.max(ld.state.bestf1, ld.log.aidaA[i][1])
    end
  end
  
  if ld.log.cEpoch then
    ld.log.cEpoch = nil
  end

end