require 'nn'
local tds = require('tds')
local w2vutils = require 'w2vutils'
local envecutils = require 'envecutils'


local w2vdim = 300

local dict = {}
-- context vocab, vectors
dict.vocab =  tds.hash()
dict.ivocab =  tds.hash()
dict.redirect = tds.hash()
dict.vecs = {}

dict.vecs[#dict.vocab+1] = torch.randn(w2vdim)
dict.ivocab[#dict.vocab+1] = '<pad>'
dict.vocab['<pad>'] = #dict.vocab+1

dict.vecs[#dict.vocab+1] = torch.randn(w2vdim)
dict.ivocab[#dict.vocab+1] = '<unk>'
dict.vocab['<unk>'] = #dict.vocab+1

dict.vecs[#dict.vocab+1] = torch.randn(w2vdim)
dict.ivocab[#dict.vocab+1] = '<eos>'
dict.vocab['<eos>'] = #dict.vocab+1

dict.addToVocab = function(self, word)
  local idx = self.vocab[word]
  if not idx then
    red = self.redirect[word]
    if not red then
      local vec, red = w2vutils:word2vec(word)
      if not vec then
        self.redirect[word] = '<unk>'
        idx = self.vocab['<unk>']
      else
        w = word
        if red then
          w = red
          self.redirect[word] = red
        end
        idx = #self.vocab+1
        self.vecs[idx] = vec
        self.ivocab[idx] = w
        self.vocab[w] = idx
      end
    else
      idx = self.vocab[red]
    end
  end
  return idx
end

dict.getVocabTensor = function(self)
  local merge=nn.Sequential()
                :add(nn.JoinTable(1))
                :add(nn.View(-1,w2vdim))
  return merge:forward(self.vecs)
end

dict.getWordFromID = function(self, ID)
  return self.ivocab[ID]
end

dict.vocabSize = function(self)
  return #self.vocab
end

dict.freeMemory = function(self)
  w2vutils:freeMemory()
  self.vocab =  nil
  self.ivocab =  nil
  self.redirect = nil
  self.vecs = nil
  collectgarbage()
end


return dict