local cmd = torch.CmdLine()
cmd:option('--load', '', 'model name')
cmd:option('--save', '', 'model name')
g_params = cmd:parse(arg or {})


local d = torch.load(g_params.load, binary, false)
d.state = {}
d.state.epoch = 1
d.state.wikichunk = 0

torch.save(g_params.save, d, binary, false)