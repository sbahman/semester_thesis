require('paths')
local tds = require 'tds'


-- default directory to put logs if andir parameter is enabled
local asciidir = 'modelsASCII/'
local bindir = 'models/'

local binext = '.mdl'
local asciiext = '.mdlA'

local cmd = torch.CmdLine()
cmd:option('--mode', 0, '0 = bin2ascii; 1 = ascii2bin')
cmd:option('--outdir', '', 'output directory')

g_params = cmd:parse(arg or {})



if g_params.mode == 0 then
  g_params.loadformat = 'binary'
  g_params.saveformat = 'ascii'
  g_params.loadextformat = binext
  g_params.saveextformat = asciiext
  if g_params.outdir == '' then
    g_params.outdir = asciidir
  end
elseif g_params.mode == 1 then
  g_params.loadformat = 'ascii'
  g_params.saveformat = 'binary'
  g_params.loadextformat = asciiext
  g_params.saveextformat = binext
  if g_params.outdir == '' then
    g_params.outdir = bindir
  end
else
  error('unknown mode')
end

   
local function convert(mdlpath)
  print(' -------------------------------------------------')
  print ('loading   ' .. mdlpath)  
  local model = torch.load(mdlpath, g_params.loadformat, false)
  local mdlname = paths.basename(mdlpath, g_params.loadextformat)
  local outmdlpath = g_params.outdir .. mdlname .. g_params.saveextformat
  print ('saving   ' .. outmdlpath)
  torch.save(outmdlpath, model, g_params.saveformat, false)
  print(' -------------------------------------------------')
end



paths.mkdir(g_params.outdir)

for line in io.lines() do
  convert(line)
end