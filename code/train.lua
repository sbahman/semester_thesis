local optim = require 'optim'

local cEnd = 5 + g_params.ncand


local function logscores(log, scores)
  local n = #log+1
  log[n] = {}
  log[n][1] = scores.f1
  log[n][2] = scores.precision
  log[n][3] = scores.recall
end


function g_f1score(numEx, truePos, stats, warn)
  local warn = warn or false
  if warn then
    assert(numEx == stats.pos, "number of examples passed does not match number of examples extracted from document")
  end
  local fNeg = stats.rNegPos * numEx
  local precision = truePos/numEx
  local recall = truePos/(fNeg + numEx)
  local f1 = 2*(precision*recall)/(precision+recall)
  local scores = {}
  scores.f1 = f1
  scores.precision = precision
  scores.recall = recall
  return scores
end

function g_testScores(scores)
  scores = scores or {}
  scores.msnbc = g_test(g_data.msnbc)
  scores.ace = g_test(g_data.ace)
  scores.aquaint = g_test(g_data.aquaint)
  scores.aidaA = g_test(g_data.aidaA)
  scores.aidaB = g_test(g_data.aidaB)
  return scores
end

function g_addToLog(scores)
  logscores(g_log.wiki, scores.wikiTrain)
  logscores(g_log.msnbc, scores.msnbc)
  logscores(g_log.ace, scores.ace)
  logscores(g_log.aquaint, scores.aquaint)
  logscores(g_log.aidaA, scores.aidaA)
  logscores(g_log.aidaB, scores.aidaB)
end



----------------------- TEST FUNCTION -------------------------------

function g_test(doc)
  local docnames = doc.docnames
  
  local Ntot = 0
  local cost = 0
  local truePos = 0


  for d = 1,#docnames do
    local datafile = torch.load(docnames[d], binary, false)
    local data = datafile.data
    local documents = datafile.docs
    
    local batchsize = data:size(1)
    local N = math.ceil(data:size(1) / batchsize)   -- =1 (keeping this way of doing it in case I need to switch back)
    
    Ntot = Ntot+N*batchsize
    
    -- local input = uTensor(g_params.batchsize, g_params.edim)
    local input = uTensor(batchsize)
    local candidates = uTensor(batchsize, g_params.ncand)
    local target = uTensor(batchsize)
    local context = uTensor(batchsize, g_params.memsize):fill(1)    --fill with padding
    local time = uTensor(batchsize, g_params.memsize)
    for t = 1, g_params.memsize do
      time:select(2, t):fill(t)
    end
    local k = math.ceil(g_params.memsize/2)


    local crit = nn.ClassNLLCriterion()
    crit.sizeAverage = g_params.critavg

    local m = 1
    input = data[{{},1}]
    target = data[{{},2}]
    candidates = data[{{}, {6, cEnd}}]
    for n = 1, N do
      for b = 1, batchsize do
--        print(docnames)
--        print(data[m][3])
--        print(data[m][4])
--        print(data[m][5])
--        print(k)
--        print(documents)
--        print(documents[data[m][3]])


--        input[b] = data[m][1]
--        target[b] = data[m][2]
--        candidates[b] = data[{m, {6, cEnd}}]


        
        local headLength = math.min(k, data[m][4] - 1)
        local docstart = data[m][4] - headLength
        
        local tailbegin = data[m][4] + data[m][5]
        local nwords = documents[data[m][3]]:nElement()

        
        local tailLength = math.min(k, nwords - tailbegin + 1)
        local docend = tailbegin + tailLength -1

        
        -- copy the context before the mention
        if headLength > 0 then
          context[{ b, { 1 + k - headLength, k} }] = 
            documents[data[m][3]][{{docstart, data[m][4] -1}}]
        end
          

        -- copy the context after the mention
        if tailLength > 0 then
          context[{ b, { k+1, k + tailLength} }] = 
            documents[data[m][3]][{{tailbegin, docend}}]
        end
        
        m = m + 1
--        if m > data:size(1) then
--          m = 1 
--        end
      end
      local x = {input, context, time, candidates}
      local tprob = g_model:forward(x)  -- softmax output
      local costl = crit:forward(tprob, target)
      local val, pred = torch.max(tprob, 2)
      truePos = truePos + torch.sum(torch.eq(target, pred:typeAs(target)))
      cost = cost + costl
    end
  end
  local scores = g_f1score(Ntot, truePos, doc.stats)
  return scores, cost/Ntot
end


----------------------- EVAL FUNCTION FOR OPTIM -------------------------------

local trainData
local trainDoc
local count = 0


function g_eval(params)
  
  if params ~= g_paramx then
    g_paramx:copy(params)
  end
  
  count = count + g_params.batchsize
  local input = uTensor(g_params.batchsize)
  local candidates = uTensor(g_params.batchsize, g_params.ncand)
  local target = uTensor(g_params.batchsize)
  local context = uTensor(g_params.batchsize, g_params.memsize):fill(1)   --fill with padding
  local time = uTensor(g_params.batchsize, g_params.memsize)
  local k = math.ceil(g_params.memsize/2)
  for t = 1, g_params.memsize do
    time:select(2, t):fill(t)
  end

  local crit = nn.ClassNLLCriterion()
  crit.sizeAverage = g_params.critavg

  for b = 1, g_params.batchsize do
    local m = math.random(trainData:size(1))
    input[b] = trainData[m][1]
    candidates[b] = trainData[{m, {6, cEnd}}]
    target[b] = trainData[m][2]

    local headLength = math.min(k, trainData[m][4] - 1)
    local docstart = trainData[m][4] - headLength

    local tailbegin = trainData[m][4] + trainData[m][5]
    local nwords = trainDoc[trainData[m][3]]:nElement()


    local tailLength = math.min(k, nwords - tailbegin + 1)
    local docend = tailbegin + tailLength -1


    -- copy the context before the mention
    if headLength > 0 then
      context[{ b, { 1 + k - headLength, k} }] = 
      trainDoc[trainData[m][3]][{{docstart, trainData[m][4] -1}}]
    end


    -- copy the context after the mention
    if tailLength > 0 then
      context[{ b, { k+1, k + tailLength} }] = 
      trainDoc[trainData[m][3]][{{tailbegin, docend}}]
    end

  end

  if g_params.noise ~= 0 then
    -- inject 10% random words to improve performance
    local pr = g_params.noise
    context:apply(function(y)
        if pr > math.random() then
          return math.random(g_params.nwords)
        end
      end)
  end

  if g_params.unkmen ~= 0 then
    local pr = g_params.unkmen
    input:apply(function(y)
        if pr > math.random() then
          return 3    -- index for unkmen token in both glove and w2v
        end
      end)
  end
      
  local x = {input, context, time, candidates}
  local tprob = g_model:forward(x)  -- softmax output
--      print(tprob)
--      io.read()
  local costl = crit:forward(tprob, target)
  local gradCriterion = crit:backward(tprob, target)
  local val, pred = torch.max(tprob, 2)
  local numCorrect = torch.sum(torch.eq(target, pred:typeAs(target)))

  g_state.truePos = g_state.truePos + numCorrect



  g_paramdx:zero()
  g_model:backward(x, gradCriterion)
  
  local gn = g_paramdx:norm()
  if gn > g_params.maxgradnorm then
    g_paramdx:mul(g_params.maxgradnorm / gn)
  end
  
  return costl, g_paramdx
end

--------------------------- SEQUENTIAL EVAL FUNCTION -----------------
local trainBatchIdx = 1


function g_eval_seq(params)
  
  if params ~= g_paramx then
    g_paramx:copy(params)
  end
  
  local trainBatchEnd = math.min(trainBatchIdx + g_params.batchsize - 1, trainData:size(1))
  local batchsize = trainBatchEnd - trainBatchIdx + 1
  
  count = count + batchsize
  
  local input = uTensor(batchsize)
  local candidates = uTensor(batchsize, g_params.ncand)
  local target = uTensor(batchsize)
  local context = uTensor(batchsize, g_params.memsize):fill(1)   --fill with padding
  local time = uTensor(batchsize, g_params.memsize)
  local k = math.ceil(g_params.memsize/2)
  for t = 1, g_params.memsize do
    time:select(2, t):fill(t)
  end

  local crit = nn.ClassNLLCriterion()
  crit.sizeAverage = g_params.critavg
  
  input = trainData[{{trainBatchIdx, trainBatchEnd},1}]
  target = trainData[{{trainBatchIdx, trainBatchEnd},2}]
  candidates = trainData[{{trainBatchIdx, trainBatchEnd}, {6, cEnd}}]
  

  for b = 1, batchsize do

    local headLength = math.min(k, trainData[b][4] - 1)
    local docstart = trainData[b][4] - headLength

    local tailbegin = trainData[b][4] + trainData[b][5]
    local nwords = trainDoc[trainData[b][3]]:nElement()


    local tailLength = math.min(k, nwords - tailbegin + 1)
    local docend = tailbegin + tailLength -1


    -- copy the context before the mention
    if headLength > 0 then
      context[{ b, { 1 + k - headLength, k} }] = 
        trainDoc[trainData[b][3]][{{docstart, data[b][4] -1}}]
    end


    -- copy the context after the mention
    if tailLength > 0 then
      context[{ b, { k+1, k + tailLength} }] = 
        trainDoc[trainData[b][3]][{{tailbegin, docend}}]
    end
  end
  
  trainBatchIdx = trainBatchEnd + 1

  if g_params.noise ~= 0 then
    -- inject 10% random words to improve performance
    local pr = g_params.noise
    context:apply(function(y)
        if pr > math.random() then
          return math.random(g_params.nwords)
        end
      end)
  end

  if g_params.unkmen ~= 0 then
    local pr = g_params.unkmen
    input:apply(function(y)
        if pr > math.random() then
          return 3    -- index for unkmen token in both glove and w2v
        end
      end)
  end
      
  local x = {input, context, time, candidates}
  local tprob = g_model:forward(x)  -- softmax output
--      print(tprob)
--      io.read()
  local costl = crit:forward(tprob, target)
  local gradCriterion = crit:backward(tprob, target)
  local val, pred = torch.max(tprob, 2)
  local numCorrect = torch.sum(torch.eq(target, pred:typeAs(target)))

  g_state.truePos = g_state.truePos + numCorrect



  g_paramdx:zero()
  g_model:backward(x, gradCriterion)
  
  local gn = g_paramdx:norm()
  if gn > g_params.maxgradnorm then
    g_paramdx:mul(g_params.maxgradnorm / gn)
  end
  
  return costl, g_paramdx
end



----------------------- TRAIN FUNCTION -------------------------------

function g_train(doc, anneal, fillRand)
  local frand = true
  if fillRand == false then
    frand = fillRand
  end
  
  local docnames = doc.docnames
  local Ntot = 0
  local cost = 0
  

  
  local logIncrement = g_params.logIncrement
  local logThresh = logIncrement
  local logCount = #g_log.aidaA
  
  local startChunk = g_state.wikichunk + 1
  
  local cEnd = 5 + g_params.ncand     -- index in training file of last candidate
  
  for d = startChunk,#docnames do
    g_state.wikichunk = d
    if g_params.show then print('Processing chunk ' .. d .. ' / ' .. #docnames) end
    local datafile = torch.load(docnames[d], binary, false)
    trainData = datafile.data
    trainDoc = datafile.docs


    if frand then
      -- replace 90% of candidate padding with random
      local cand = trainData[{{},{6, cEnd}}]
      cand:apply(
        function(x)
          if x == 1 then
            local a = math.random()
            if a < 0.9 then
              return math.random(g_params.nent)
            end
          end
        end)
    end

    local N = math.ceil(trainData:size(1) / g_params.batchsize)

    Ntot = Ntot+N

    for n = 1, N do
      g_state.counter = g_state.counter + 1
      if g_params.show then xlua.progress(n, N) end

      if g_params.optim == '' then
        g_eval(g_paramx)
        g_paramx:add(g_paramdx:mul(-g_params.dt))
      else
        g_optimMethod(g_eval, g_paramx, g_optimState)
      end


      if g_state.counter >= logThresh then

        local scores = g_testScores()
        scores.wikiTrain = g_f1score(count, g_state.truePos, doc.stats)
        count = 0
        g_state.counter = 0
        logCount = logCount + 1
        g_state.truePos = 0

        scores.LR = g_params.dt
        scores.best_AidaA_f1 = g_state.bestf1

        print(scores)
        g_addToLog(scores)

        g_state.bestf1 = math.max(g_state.bestf1, g_log.aidaA[logCount][1])

        -- Learning rate annealing
        if anneal and g_params.optim == '' then
          if g_params.annealbest and g_log.aidaA[logCount][1] > g_state.bestf1 * 0.9999 then
            g_params.dt = g_params.dt * g_params.anneal
          elseif (not g_params.annealbest) and g_log.aidaA[logCount][1] > g_log.aidaA[logCount-1][1] * 0.9999 then
            g_params.dt = g_params.dt * g_params.anneal
          end
        end
        if g_params.dt < 1e-5 then return end
      end
    end
    if(g_terminate == true) then break end
  end

  local scores = g_testScores()
  scores.wikiTrain = g_f1score(count, g_state.truePos, doc.stats)
  count = 0
  g_state.truePos = 0

  scores.LR = g_params.dt
  scores.best_AidaA_f1 = g_state.bestf1

  print(scores)
  g_addToLog(scores)
  logCount = logCount + 1
  g_state.bestf1 = math.max(g_state.bestf1, g_log.aidaA[logCount][1])
  
  g_state.wikichunk = 0
  g_state.counter = 0
  g_state.epoch = g_state.epoch + 1

end







--    -- local input = uTensor(g_params.batchsize, g_params.edim)
--    -- local input = uTensor(g_params.batchsize, 1)
--    local input = uTensor(g_params.batchsize)
--    local candidates = uTensor(g_params.batchsize, g_params.ncand)
--    local target = uTensor(g_params.batchsize)
--    local context = uTensor(g_params.batchsize, g_params.memsize):fill(1)   --fill with padding
--    local time = uTensor(g_params.batchsize, g_params.memsize)
--    local k = math.ceil(g_params.memsize/2)
--    for t = 1, g_params.memsize do
--      time:select(2, t):fill(t)
--    end

--    local crit = nn.ClassNLLCriterion()
--    crit.sizeAverage = false


    
--    for n = 1, N do
--      stepCounter = stepCounter + 1
--      if g_params.show then xlua.progress(n, N) end
--      for b = 1, g_params.batchsize do
--        local m = math.random(data:size(1))
--        input[b] = data[m][1]
--        candidates[b] = data[{m, {6, cEnd}}]
--        target[b] = data[m][2]
        
--        local headLength = math.min(k, data[m][4] - 1)
--        local docstart = data[m][4] - headLength
        
--        local tailbegin = data[m][4] + data[m][5]
--        local nwords = documents[data[m][3]]:nElement()

        
--        local tailLength = math.min(k, nwords - tailbegin + 1)
--        local docend = tailbegin + tailLength -1

        
--        -- copy the context before the mention
--        if headLength > 0 then
--          context[{ b, { 1 + k - headLength, k} }] = 
--            documents[data[m][3]][{{docstart, data[m][4] -1}}]
--        end
          

--        -- copy the context after the mention
--        if tailLength > 0 then
--          context[{ b, { k+1, k + tailLength} }] = 
--            documents[data[m][3]][{{tailbegin, docend}}]
--        end
        
--      end
      
--      if g_params.noise then
--        -- inject 10% random words to improve performance
--        local pr = 0.1
--        context:apply(function(y)
--            if pr > math.random() then
--              return math.random(g_params.nwords)
--            end
--          end)
--      end
      
--      if g_params.unkmen then
--        local pr = 0.001
--        input:apply(function(y)
--            if pr > math.random() then
--              return 3    -- index for unkmen token in both glove and w2v
--            end
--          end)
--      end

      
--      local x = {input, context, time, candidates}
      
----      local tprob = g_model:forward(x)  -- softmax output
----      local costl = crit:forward(tprob, target)
----      local gradCriterion = crit:backward(tprob, target)
----      local val, pred = torch.max(tprob, 2)
----      local numCorrect = torch.sum(torch.eq(target, pred:typeAs(target)))
      
----      truePos = truePos + numCorrect

----      cost = cost + costl
      
----      g_paramdx:zero()
----      g_model:backward(x, gradCriterion)