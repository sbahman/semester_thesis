local stringx = require('pl.stringx')
local tds = require('tds')


w2vopt = {
	binfilename = 'data/w2vutils/GoogleNews-vectors-negative300.bin',
  mfile       = 'data/w2vutils/M.t7',
  vocabfile   = 'data/w2vutils/vocab.t7'
}
local w2vutils = {}
if not paths.filep(w2vopt.mfile) then
	w2vutils = require('bintot7')
else
	w2vutils = torch.load(w2vopt.vocabfile, binary, false)
	print('Done reading word2vec data.')
end




collectgarbage()


-------------------------------------------------------

w2vutils.loadM = function(self)
  local M = torch.load(w2vopt.mfile, binary, false)
  self.M = M
end

w2vutils.loadAndReturnM = function(self)
  return torch.load(w2vopt.mfile, binary, false)
end

w2vutils.getVocabTensor = function(self)
  local M = self:loadAndReturnM()
  local vocabTensor
  if #self.comp ~= 0 then
    local compTensor = torch.Tensor(#self.comp, self.vdim):zero()
    for i = 1, #self.comp do
      if self.comp[i] == 0 then
        compTensor[i] = torch.randn(self.vdim)
      else
        for j = 1, #self.comp[i] do
          compTensor[i] = compTensor[i] + M[self.comp[i][j]]
        end
        compTensor[i] = compTensor[i]/#self.comp[i]
      end
    end
    
    print("b1")
    vocabTensor = torch.cat({M, compTensor}, 1)
  else
    print("b2")
    vocabTensor = M
  end

  
  self.Ow2vvocab = self.w2vvocab
  
  local w2v = {}
  w2v.w2vvocab = self.w2vvocab
  w2v.v2wvocab = self.v2wvocab
  w2v.Ow2vvocab = self.Ow2vvocab
  w2v.vdim = self.vdim
  w2v.redir = self.redir
  w2v.comp = tds.hash()

  torch.save(w2vopt.vocabfile,w2v, binary, false)
  print('Writing w2v expanded vocab t7 File for future usage.')

  torch.save(w2vopt.mfile, vocabTensor, binary, false)
  print('Writing expanded w2v vectors t7 File for future usage.')
  
  collectgarbage()
  
  return vocabTensor
end

w2vutils.getWordFromID = function(self, ID)
  return self.v2wvocab[ID]
end

w2vutils.dimSize = function(self)
  return self.vdim
end

w2vutils.vocabSize = function(self)
  return #self.v2wvocab
end

w2vutils.printWord = function(self, wordID)
  print(self.v2wvocab[wordID])
end

w2vutils.printWords = function(self, wordTensor)
  wordTensor:apply(function(x)
      print(self.v2wvocab[x])
    end)
end

w2vutils.getId = function(self, word, unkMode)    -- if unkMode == 1 then return <unk>; 2: return <unkMention>; 0: (any other number) return nil
  local unk = unkMode or 1
  
  word = word:lower()
  
  local idx = self.w2vvocab[word]
  if not idx then
    local red = self.redir[word]    -- check if word has redirect
    if red then
      idx = self.w2vvocab[red]
    else
      idx, red = self:getCat(word)   -- try concat word
      if idx then
        self.redir[word] = red
      else
        local comp = nil
        comp, red = self:getComp(word)      -- try to get compound word
        if comp then
          self.redir[word] = red
          idx = self.w2vvocab[red]
        elseif unk == 1 then
          idx = self.w2vvocab['<unk>']
        elseif unk == 2 then
          idx = self.w2vvocab['<unkmen>']
        else
          idx = nil
        end
      end
    end
  end
  return idx
end

        

w2vutils.addToVocab = function(self, word, unkMode)    -- if unkMode == 1 then return <unk>;   2: return <unkMention>;  3: add random tensor to vocab and return index;0: (or any other number) return nil
  local unk = unkMode or 1
  
  word = word:lower()
  
  local idx = self.w2vvocab[word]
  if not idx then
    local red = self.redir[word]    -- check if word has redirect
    if red then
      idx = self.w2vvocab[red]
    else
      idx, red = self:getCat(word)   -- try concat word
      if idx then
        self.redir[word] = red
      else
        local comp = nil
        comp, red = self:getComp(word)      -- try to get compound word
        if comp then
          self.redir[word] = red
          idx = self.w2vvocab[red]    -- check if redirect word already in vocab
          if not idx then
            idx = #self.v2wvocab+1
            self.w2vvocab[red] = #self.v2wvocab+1
            self.v2wvocab[#self.v2wvocab+1] = red
            self.comp[#self.comp +1] = comp
          end
        elseif unk == 1 then
          idx = self.w2vvocab['<unk>']
        elseif unk == 2 then
          idx = self.w2vvocab['<unkmen>']
        elseif unk == 3 then
          idx = #self.v2wvocab+1
          self.w2vvocab[word] = #self.v2wvocab+1
          self.v2wvocab[#self.v2wvocab+1] = word
          self.comp[#self.comp +1] = 0    -- when building tensor make a random vector
        else
          idx = nil
        end
      end
    end
  end
  return idx
end

w2vutils.getCat = function (self, word)   -- try to get wordvec by concatenating the inputstring (try both caps and lowecase)
  local concat = word:gsub("%s", "_")
  local ind = self.w2vvocab[concat]

  return ind, concat
end

w2vutils.getComp = function(self, word)
  local words = stringx.split(word)
  local c = 0
  local comp = tds.Vec()     -- indexes of compound words
  local r = ''  -- build up compound word
  local f = false
  for k = 1, #words do
    local w = words[k]
    local i = self.Ow2vvocab[w]
    
    if not i then
      local red = self.redir[w]
      if red then
        i = self.Ow2vvocab[red]
      end
    end
    
    if i ~= nil then
      if f then 
        r = r .. "_"
      end
      f = true
      r = r .. w
      comp[#comp+1] = i
      c = c + 1
    end
  end
  if c == 0 then
    return nil, nil
  else
    return comp, r
  end
end

------------------------- uppercase versions of functions --------------

--w2vutils.addToVocabCaps = function(self, word, retUNK)    -- if retUNK it will return an unk token if an unknown word is found, else nil
--  local unk = true
--  if retUNK ~= nil then
--    unk = retUNK
--  end
--  local idx = self.w2vvocab[word]
--  local red = nil
--  if not idx then
--    red = self.redir[word]    -- check if word has redirect
--    if red then
--      idx = self.w2vvocab[red]
--    else
--      idx = self.w2vvocab[word:lower()]
--      if idx then
--        self.redir[word] = word:lower()
--      else
--        idx, red = self:getCatCaps(word)   -- try concat word
--        if idx then
--          self.redir[word] = red
--        else
--          local comp = nil
--          comp, red = self:getCompCaps(word)      -- try to get compound word
--          if comp then
--            self.redir[word] = red
--            idx = self.w2vvocab[red]    -- check if redirect word already in vocab
--            if not idx then
--              idx = #self.v2wvocab+1
--              self.w2vvocab[red] = #self.v2wvocab+1
--              self.v2wvocab[#self.v2wvocab+1] = red
--              self.comp[#self.comp +1] = comp
--            end
--          elseif unk then
--            idx = self.w2vvocab['<unk>']
--          else
--            idx = nil
--          end
--        end
--      end
--    end
--  end
--  return idx
--end
            


--w2vutils.getCatCaps = function (self, word)   -- try to get wordvec by concatenating the inputstring (try both caps and lowecase)
--  local concat = word:gsub("%s", "_")
--  local ind = self.w2vvocab[concat]
--  local redirect = nil
--  if ind == nil then
--    ind = self.w2vvocab[concat:lower()]
--    if ind then
--      redirect = concat:lower()
--    end
--  else
--    redirect = concat
--  end

--  return ind, redirect
--end

--w2vutils.getCompCaps = function(self, word)
--  local words = stringx.split(word)
--  local c = 0
--  comp = tds.Vec()     -- indexes of compound words
--  local r = ''  -- build up compound word
--  local f = false
--  for k = 1, #words do
--    local w = words[k]
--    local wr = w
--    --try the word then try it all lower, build up redirect word r accordingly
--    local i = self.Ow2vvocab[w]
    
--    if i == nil then
--      i = self.Ow2vvocab[w:lower()]
--      wr = w:lower()
--    end
    
--    if i ~= nil then
--      if f then 
--        r = r .. "_"
--      end
--      f = true
--      r = r .. wr
--      comp[#comp+1] = i
--      c = c + 1
--    end
--  end
--  if c == 0 then
--    return nil, nil
--  else
--    return comp, r
--  end
--end

--w2vutils.word2vec = function (self,word)
--  -- unused
--  local concat = word:gsub("%s", "_")
--  local ind = self.w2vvocab[concat]
--  local vec = nil
--  local redirect = nil
--  if ind == nil then
--    ind = self.w2vvocab[concat:lower()]
--    if ind == nil then
--      ind = self.w2vvocabl[concat:lower()]
--      if ind == nil then
--        local words = stringx.split(word)
--        local c = 0
--        local v = torch.FloatTensor(300):zero()
--        local r = ''
--        local f = false
--        for k = 1, #words do
--          w = words[k]
--          --try the word then try it all lower, build up redirect word r accordingly
--          i = self.w2vvocab[w]
--          if i == nil then
--            i = self.w2vvocab[w:lower()]
--            if i == nil then
--              i = self.w2vvocabl[w:lower()]
--              if i ~= nil then 
--                if f then 
--                  r = r .. "_"
--                end
--                f = true
--                r = r .. w:lower()
--              end
--            else
--              if f then 
--                r = r .. "_"
--              end
--              f = true
--              r = r .. w:lower()
--            end
--          else
--            if f then
--              r = r .. "_"
--            end
--            f = true
--            r = r .. w
--          end
--          if i ~= nil then
--            v = v + self.M[i]
--            c = c + 1
--          end
--        end
--        if c ~= 0 then
--          vec = v/c
--          redirect = r
--        end
--      else
--        vec = self.M[ind]
--        redirect = concat:lower()
--      end
--    else
--      vec = self.M[ind]
--      redirect = concat:lower()
--    end
--  else
--    vec = self.M[ind]
--  end

--  return vec, redirect
--end

w2vutils.distance = function (self,vec,k)
	local k = k or 1	
	--self.zeros = self.zeros or torch.zeros(self.M:size(1));
	local norm = vec:norm(2)
	vec:div(norm)
	local distances = torch.mv(self.M ,vec)
	distances , oldindex = torch.sort(distances,1,true)
	local returnwords = {}
	local returndistances = {}
	for i = 1,k do
		table.insert(returnwords, w2vutils.v2wvocab[oldindex[i]])
		table.insert(returndistances, distances[i])
	end
	return {returndistances, returnwords}
end

w2vutils.word2vecSimple = function (self,word,throwerror)
   local throwerror = throwerror or false
   local ind = self.w2vvocab[word]
   if throwerror then
		assert(ind ~= nil, 'Word does not exist in the dictionary!')
   end
	--if ind == nil then
		--ind = self.w2vvocab['UNK']
	--end
    if ind == nil then
      return nil
    else
      return self.M[ind]
    end
end

w2vutils.freev2w = function(self)
  self.v2wvocab = nil
end

w2vutils.freeMemory = function(self)
  self.M = nil
  self.w2vvocab = nil
  self.v2wvocab = nil
  self.redir = nil
  self.comp = nil
  self.Ow2vvocab = nil
  collectgarbage()
end

return w2vutils
