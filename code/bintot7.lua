local tds = require('tds')
file = torch.DiskFile(w2vopt.binfilename,'r')
local max_w = 50
local nSpecial = 5  -- number od special tokens

print("reading w2v from file")

function readStringv2(file)  
	local str = {}
	for i = 1,max_w do
		local char = file:readChar()
		
		if char == 32 or char == 10 or char == 0 then
			break
		else
			str[#str+1] = char
		end
	end
	str = torch.CharStorage(str)
	return str:string()
end





--Reading Header
file:ascii()
words = file:readInt()
size = file:readInt()


local w2vvocab = tds.hash()
local v2wvocab = tds.hash()
local M = torch.Tensor(words + nSpecial,size)

w2vvocab['<pad>'] = #v2wvocab+1
v2wvocab[#v2wvocab+1] = '<pad>'

w2vvocab['<unk>'] = #v2wvocab+1
v2wvocab[#v2wvocab+1] = '<unk>'
  
w2vvocab['<unkmen>'] = #v2wvocab+1
v2wvocab[#v2wvocab+1] = '<unkmen>'

w2vvocab['<eos>'] = #v2wvocab+1
v2wvocab[#v2wvocab+1] = '<eos>'

w2vvocab['<eol>'] = #v2wvocab+1
v2wvocab[#v2wvocab+1] = '<eol>'

M[{{1,nSpecial},{}}] = torch.randn(nSpecial, size)

local id = nSpecial

--Reading Contents
file:binary()
for i = 1,words do
  id = id + 1
	local str = readStringv2(file)
	local vecrep = file:readFloat(300)
	vecrep = torch.Tensor(vecrep)
	local norm = torch.norm(vecrep,2)
	if norm ~= 0 then vecrep:div(norm) end
	w2vvocab[str] = id
	v2wvocab[id] = str
	M[{{id},{}}] = vecrep
end

print('adding lowercase words to w2vvocab')
for k,v in pairs(w2vvocab) do    -- add all the lowercase versions of words to vocab
  if not w2vvocab[k:lower()] then
    w2vvocab[k:lower()] = v
    w2vvocab[k] = nil
  end
end

local Ow2vvocab = tds.hash()         -- original vocab used to get comp words
for k,v in pairs(w2vvocab) do
  Ow2vvocab[k] = v
end

--Writing Files
word2vec = {}
word2vec.w2vvocab = w2vvocab
word2vec.v2wvocab = v2wvocab
word2vec.Ow2vvocab = Ow2vvocab
word2vec.comp = tds.hash()
word2vec.redir = tds.hash()
word2vec.vdim = M:size(2)
torch.save(w2vopt.vocabfile,word2vec, binary, false)
print('Writing w2v vocab t7 File for future usage.')

torch.save(w2vopt.mfile, M, binary, false)
print('Writing w2v vectors t7 File for future usage.')


return word2vec


