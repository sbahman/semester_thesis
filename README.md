# Semester Thesis: Entity Linking with Memory Networks
This Semester Thesis uses Memory Networks, a type of Neural Network with an attention mechanism to perform Entity Linking. The code is written in lua using torch and can be found in the code folder. A copy of the thesis can be found in the base folder.


## Author
Severin Bahman
